require 'rubygems'
load 'bestseller_class.rb'

@categories = Category.where(:watch => true)

@categories.each do |c|
	bestsellers_files = Dir::glob("bestsellers/#{c.id}_*.html")

	bestseller = Bestseller.new
	bestseller.category_id = c.id
	bestseller.count = 100
	bestseller.watch = c.watch
	bestseller.save
	#Dir.foreach('bestsellers') do |item|
	Dir.foreach('bestsellers') do |item|
		next if item == "." || item == ".." || item == ".DS_Store"
		rank = (item.split("_")[2]).split(".")[0].to_i
		book = Book.new("bestsellers/#{item}", rank)
		book.save(bestseller.id)
	end
end