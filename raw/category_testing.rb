require 'rubygems'
require 'rails'
require 'active_record'
require 'ancestry'

if Rails.env == "production"
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		:host => "localhost",
		#:pool => 5,
		:username => "kindle_trend",
		:password => "THzmVLcvaNojnbi"
	)
else
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		#:pool => 5,
		:username => "kindletrend",
		:password => "kindletrend",
	)
end

class Category < ActiveRecord::Base
	has_ancestry
end

ancestry = Category.all.pluck(:ancestry)

ancestor_ids = Array.new

ancestry.each do |a|
	ancestor_ids << a.to_s.split("/").flatten
end

non_parents = Category.where("id IN (?)", ancestor_ids.flatten.uniq)
puts non_parents.count