require 'rss'
require 'open-uri'
require 'nokogiri'
require 'active_record'
require 'twitter'
require 'long_url'
#require 'ralexa'

if Rails.env == "production"
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		:host => "localhost",
		#:pool => 5,
		:username => "kindle_trend",
		:password => "THzmVLcvaNojnbi"
	)
else
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		#:pool => 5,
		:username => "kindletrend",
		:password => "kindletrend",
	)
end

class Category < ActiveRecord::Base
	has_many :bestsellers
end

class Bestseller < ActiveRecord::Base
	belongs_to :category
	has_many :ebooks
end

class Ebook < ActiveRecord::Base
	belongs_to :bestseller
	has_many :authors
	has_many :ranks
	has_many :reviewers
end

class Author < ActiveRecord::Base
	belongs_to :ebook
end

class Rank < ActiveRecord::Base
	belongs_to :ebook
end

class Reviewer < ActiveRecord::Base
	belongs_to :ebook
end

class Writer
	def initialize
		@twitter = Twitter::REST::Client.new do |config|
		  config.consumer_key        = "8N1sWP52OpY5KoheqgtVhA"
		  config.consumer_secret     = "4nSdihetSV9jnkIdBPNIM9bpx6AgZ0bFMk2ydxA4"
		  config.access_token        = "20130961-opOxq5HKSGUhi5GkBMLlfUKncSTg95mJn32EowSiM"
		  config.access_token_secret = "rEuWDe9Sm14nFHiBZLsduUpohvWqyAfkQM1OEjiV1bvfn"
		end
	end

	def www(id)
		author = Author.find(id)

		if author.twitter?
			user = @twitter.user("#{author.twitter}")
			website = LongURL.find("#{user.website.to_s}")
		end

		author.www = "#{website}"
		author.save
	end

	def alexa()
		session = Ralexa.session("1K95SFDGJ3YEPP0BFQR2", "17R6L/c/AKjNm5Km8k+xP+fdmVk/azwqvEbL384E")
		puts "Url info of FourHourWorkweek.com"
		puts session.url_info.get("http://fourhourworkweek.com")
	end
end