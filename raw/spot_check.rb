require 'rubygems'
require 'rails'
require 'open-uri'
require 'nokogiri'
require 'active_record'

if Rails.env == "production"
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		:host => "localhost",
		#:pool => 5,
		:username => "kindle_trend",
		:password => "THzmVLcvaNojnbi"
	)
else
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		#:pool => 5,
		:username => "kindletrend",
		:password => "kindletrend",
	)
end

class Category < ActiveRecord::Base

end

class Bestseller < ActiveRecord::Base

end

class Ebook < ActiveRecord::Base

end

class Author < ActiveRecord::Base

end

class RankCategory < ActiveRecord::Base

end

# Next page of results
# class="zg_pagination"

# Define the root list url.
url = "http://www.amazon.com/Best-Sellers-Kindle-Store-eBooks/zgbs/digital-text/154606011/ref=zg_bs_unv_kstore_2_154692011_2"

# Get the root page.
page = Nokogiri::HTML(open(url))

# Get the categories navigation section of the page.
nav = page.xpath('//ul[@id="zg_browseRoot"]/ul/ul')
nav = nav.children

#nav = Nokogiri::HTML(nav)
# Get the category names and links for this page.
nav.each do |n|
	categories = n.xpath("./li")
	categories.each do |category|
		puts "=============================="
		puts "Category: #{category.text}"
		puts "Link: #{category.xpath('a/@href')}"
	end
end

# Result page links.
links = page.css("li.zg_page")

puts ""
puts "+++++++++++++++++++++++++++++++"
puts "+++++++++++++++++++++++++++++++"
puts ""

links.each do |l|
	puts "=============================="
	puts "Range: #{l.text.strip!}"
	puts "Link: #{l.xpath('a/@href')}"
end

# Find a specific item link.
#file = File.open("test.html","w")
item = page.xpath("//span[@class='zg_rankNumber'][contains(text(),'18.')]/../../div/div[2]/a")
# /div[@class='zg_itemImageImmersion']

#file.write(item)
#file.close

puts ""
puts "+++++++++++++++++++++++++++++++"
puts "+++++++++++++++++++++++++++++++"
puts ""
puts "Anchor tag: #{item}"
puts "Item: #{item.text}"
puts "Link: #{item.xpath('@href').text.strip!}"