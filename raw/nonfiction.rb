require 'rubygems'
require 'active_record'
require 'ancestry'

if Rails.env == "production"
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		:host => "localhost",
		#:pool => 5,
		:username => "kindle_trend",
		:password => "THzmVLcvaNojnbi"
	)
else
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		#:pool => 5,
		:username => "kindletrend",
		:password => "kindletrend",
		#:adapter => "postgresql",
		#:encoding => "unicode",
		#:database => "kindleStoreAnalyzer_development",
		#:pool => 5,
		#:username => "kindleStoreAnalyzer",
		#:password => "k1ndl35t0r3@n@lyz3r",
	)
end

class Category < ActiveRecord::Base
	has_ancestry
	has_many :bestsellers
end

class Bestseller < ActiveRecord::Base
	belongs_to :category
	has_many :ebooks
end

class Ebook < ActiveRecord::Base
	belongs_to :bestseller
	has_many :authors
	has_many :ranks
	has_many :reviewers
end

class Author < ActiveRecord::Base
	belongs_to :ebook
end

class Rank < ActiveRecord::Base
	belongs_to :ebook
end

class Reviewer < ActiveRecord::Base
	belongs_to :ebook
end

nf = Category.where(:ancestry => "1")

nf.each do |n|
	puts n.children[0].name
end