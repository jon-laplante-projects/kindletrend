#####################################################
# Categories with counts (from main, not bestsellers)
# Links: //ul[@id="ref_154606011"]/li/a/span[@class="refinementLink"]/..
# Counts: //ul[@id="ref_154607011"]/li/a/span[@class="narrowValue"]
#####################################################

require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'active_record'
require 'ancestry'

#@id_ladder = [1,8,789,831]
@page = ""

ActiveRecord::Base.establish_connection(
	:adapter => "sqlite3",
	:database => "../db/data.sqlite3"
)

#ActiveRecord::Base.establish_connection(
#	:adapter => "postgresql",
#	:encoding => "unicode",
#	:database => "kindlemetrics_development",
#	:pool => 5,
#	:username => "kindleAnalytics",
#	:password => "k1ndl3@n@lyt1c5",
#)

class Category < ActiveRecord::Base
	has_ancestry
end

def crawl(id)
	node = Category.find(id)

	unless node.is_root?
		# We are not on the root...
		if node.has_children?
			node = node.children[0]
		else
			node = Category.find(siblings(node.id))
		end
	else
		# Start getting categories if this is the root...
		if node.has_children?
			#if node.children.count > 30
			#	# We have traversed the tree and returned to the root.
			#	abort "Congratulations! You have a map of all Kindle categories!"
			#else
				node = node.children[0]
			#end
		else
			node = node.parent
		end
	end
	#get_categories(node.id)
	parse_categories(node.id)
end
# End crawl.

def siblings(id)
	node = Category.find(id)
	sibs = node.sibling_ids
	if sibs[sibs.index(node.id).to_i] < sibs.last
		sibs[sibs.index(node.id).to_i + 1]
	# Are we on the last sibling?
	else
		# Test for last sibling and root...[p]
		if node.is_root?
			abort "Congratulations! You have a map of all Kindle categories!"
			exec "open Notification.app"
		else
			siblings(node.parent_id)
		end
	end
end

def parse_categories(id)
	node = Category.find(id)
	url = node.url

	# Get the root page.
	@page = Nokogiri::HTML(open(url))

	# Get the categories navigation section of the page.
	if node.name == "Kindle eBooks"
		nav = @page.xpath('//ul[@id="ref_154606011"]')
	else
		@xpath_var = "//ul[@id='ref_#{node.asin}']"
		
		puts "Nav. xpath: #{@xpath_var}"
		nav = @page.xpath(@xpath_var)
	end

	nav = nav.children
	puts "Nav. count: #{nav.count}"

	nav_children = false

	# Get the category names and links for this page.
	if nav.count > 0
		nav.each do |n|
			if n.text.to_s.strip != ""
				name = n.xpath('a/span[@class="refinementLink"]').text
				puts "Name: #{name}"
				# 3 lines to distill this is really ugly!
				puts "Nav link = " << n.xpath('a/span[@class="refinementLink"]/../@href').to_s[0,21]
				unless n.xpath('a/span[@class="refinementLink"]/../@href').to_s[0,21] == "http://www.amazon.com"
					url = "http://www.amazon.com" + n.xpath('a/span[@class="refinementLink"]/../@href').to_s
					puts "URL = #{url}"
					asin = n.xpath('a/span[@class="refinementLink"]/../@href').to_s
					asin = CGI::parse("#{asin}")
					asin = asin['/s?rh'].to_s
				else
					url = n.xpath('a/span[@class="refinementLink"]/../@href').to_s
					puts "ASIN Split = #{url.split("/")[3]}"
					asin = url.split("/")[3]
					asin = CGI::parse("#{asin}")
					asin = asin['rh'].to_s
					puts "Test data: #{asin}"
				end
				puts "ASIN: #{asin[4,asin.length-6]}"
				count = n.xpath('a/span[@class="narrowValue"]').text.to_s
				count = count[2, count.length - 3].to_s.delete(",").to_i
				puts "Count: #{count}"

				if name.strip != ""
					item = Category.new(:parent_id => node.id)	
						item.name = "#{name}"
						item.url = "#{url}"
						item.asin = "#{asin[4,asin.length-6]}"
						item.count = count
					item.save
				end
			end
		end
	end

	puts "Current node: #{node.id}"
	crawl(node.id)
end

# No longer used...
def get_categories(id)
	# At the end of the function, if recs have been added, append the first in the group @id_ladder.
	# Otherwise, increment @id_ladder.last.
	node = Category.find(id)
	#url = node.url
	url = "http://www.amazon.com/Kindle-eBooks/b?ie=UTF8&node=154606011"

	# Get the root page.
	@page = Nokogiri::HTML(open(url))

	# Get the categories navigation section of the page.
	if node.name == "Kindle eBooks"
		nav = @page.xpath('//ul[@id="zg_browseRoot"]/ul/ul')
	else
		@xpath_var = '//span[@class="zg_selected"]/../../ul'
		
		puts "Nav. xpath: #{@xpath_var}"
		nav = @page.xpath(@xpath_var)
	end

	nav = nav.children
	puts "Nav. count: #{nav.count}"

	# Get the category names and links for this page.
	if nav.count > 0
		nav.each do |n|
			if node.name == "Kindle eBooks"
				categories = n.xpath("./li")

				categories.each do |c|
					puts "Category: #{c}"
					item = Category.new(:parent_id => node.id)
					item.name = "#{c.text.strip}"
					if node.name == "Kindle eBooks"
						item.url = "#{c.xpath('a/@href')}"
					else
						item.url = "#{c.xpath("./a").xpath("@href").text}"
					end
					item.asin = CGI::parse("#{item.url}")
					item.count = 
					item.save
				end
			else
				if n.text.strip != ""
					item = Category.new(:parent_id => node.id)
						item.name = "#{n.text.strip}"
						item.url = "#{n.xpath('a/@href').text}"
						item.asin = 
						item.count = 
					item.save
				end
			end
		end
	end

	puts "Current node: #{node.id}"

	crawl(node.id)
end

# No longer used...
def level(category, level=0)
	unless category.name == "Kindle eBooks"
		parent = Category.find(category.parent_id)
		
		unless parent.name == "Kindle eBooks"	
			level += 1
			level(parent, level)
		else
			return level + 1 
		end
	else
		return 0
	end
end

def init()
	top = Category.find_by(:name => "Kindle eBooks")

	# If we don't have any categories, let's see the first tier.
	if !top
		category = Category.new
			category.name = "Kindle eBooks"
			#category.url = "http://www.amazon.com/Best-Sellers-Kindle-Store-eBooks/zgbs/digital-text/154606011/ref=zg_bs_unv_kstore_2_154692011_2"
			category.url = "http://www.amazon.com/s/ref=lp_154607011_ex_n_1?rh=n%3A133140011%2Cn%3A!133141011%2Cn%3A154606011&bbn=154606011&ie=UTF8&qid=1392049593"
			category.asin = "154606011"
			category.count = 2482522
		category.save
		
		#get_categories(category)
	end
end

node = Category.last

begin
	parse_categories(node.id)
rescue
	exec 'open Notification.app'
end