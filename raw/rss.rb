require 'rubygems'
require 'rails'
require 'rss'
require 'open-uri'
require 'nokogiri'
require 'mechanize'
require 'active_record'
require 'ancestry'

if Rails.env == "production"
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		:host => "localhost",
		#:pool => 5,
		:username => "kindle_trend",
		:password => "THzmVLcvaNojnbi"
	)
else
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		#:pool => 5,
		:username => "kindletrend",
		:password => "kindletrend",
	)
end

load '../app/models/author.rb'
load '../app/models/bestseller.rb'
load '../app/models/category.rb'
load '../app/models/category_group.rb'
load '../app/models/ebook.rb'
load '../app/models/rank.rb'
load '../app/models/reviewer.rb'
load '../app/models/rollup.rb'

url = 'http://www.amazon.com/gp/rss/bestsellers/digital-text/154606011/ref=zg_bs_154606011_rsslink'
#url = "http://www.amazon.com/Best-Sellers-Kindle-Store-Diets-Weight-Loss/zgbs/digital-text/156451011/ref=zg_bs_nav_kstore_3_156430011"
open(url) do |rss|
  feed = RSS::Parser.parse(rss)
  puts "Title: #{feed.channel.title}"
  
  # Iterate through each item in the feed and parse it.
  feed.items.each do |item|
    puts "Item: #{item.title}"
    #file = File.open("#{item.title}.html", "w")
    url = Nokogiri::HTML("#{item.description}")
    url.css(".url").map do |el|
    	page = open(el[:href])
    	puts "URL: #{el[:href]}"
    	#page = page.read.delete("\n")
    	page = Nokogiri::HTML(page)
    	snippet = page.css("li#SalesRank")
    	#snippet = snippet.gsub /^$\n/, ''
    	#file.write("#{snippet}")
    	code = Nokogiri::HTML("#{snippet}")
    	primary_rank = code.xpath("//b").first.next_sibling
		primary_rank = primary_rank.to_s.strip()

		digit = 1

		# Get the rank for the general Kindle store.
		for i in 0...primary_rank.length.to_i
			# Regex POSIX...
			if primary_rank[digit,1] =~ /[[:digit:]]/
				digit = digit + 1
			else
				puts "The rank is: #{primary_rank[1,digit.to_i - 1]}."
				break
			end
		end

		# Get the rank for the niche stores.
		items = code.css("li.zg_hrsr_item")

		items.each do |item|
			i = Nokogiri::HTML("#{item}")

			# Kindle Store rank.
			i_rank = i.css("span.zg_hrsr_rank").text
			i_ladder = ""
			
			# Niches...
			ranks = code.css("ul.zg_hrsr")
			ladders = ranks[0].css("span.zg_hrsr_ladder")

			# Get other ranking niches.
			i_ladder = items.xpath("//*[contains(concat(' ', @class, ' '), ' zg_hrsr_ladder ')]").text
			
			# Overall Rating
			rating = page.xpath("//*[contains(text(),'out of')]").first.text
			rating = rating[0, rating.size - 15]

			# Number of reviews
			review_count = page.xpath("//*[contains(text(),'customer reviews')]").first.text
			review_count = review_count[0, review_count.size - 17]

			# Price
			price = page.xpath("//*[contains(concat(' ', @class, ' '), ' priceLarge ')]").text
			price = price.strip!

			# Page count
			page_count = page.xpath("//*[contains(text(),'Print Length:')]/..").text
			page_count.slice! "Print Length: "
			page_count.slice! " pages"

			puts "Rank: #{i_rank}"
			puts "Ladder: #{i_ladder}"
			puts "++++++++++++++++++++"
			puts "Rating: #{rating}"
			puts "Review count: #{review_count}"
			puts "Price: #{price}"
			puts "Pages: #{page_count}"
			puts "===================="

			# Book description
			# //div/div[@class='content']/noscript
			# Array.group_by{|x| x}.values
			desc = page.xpath("//div/div[@class='content']/noscript")
			keywords = "#{desc.text}".downcase.gsub(/[^a-z\s]/, '').split(" ")
			# Build stop word list...
			stopwords = ["i","a","about","an","are","as","at","be","by","com","for","from","how","in","is","it","of","on","or","that","the","this","to","was","what","when","where","who","will","with","the","www"]
			keywords.delete(stopwords)
			#keywords = keywords.group_by{|x| x}.values
			counts = Hash.new(0)
			keywords.each { |keyword| counts[keyword] += 1 }
			puts ""
			puts ""
			puts "++++++++++++++++++++"
			puts "++++++++++++++++++++"
			puts "Keywords: #{keywords}"

			# Author(s)
			# Single author - //span/span[contains(text(),'(Author)')]/preceding-sibling::a
			# Multiple authors - //span/span[@class="contributorNameTrigger"]/a
			authors = page.xpath("//span/span[contains(text(),'(Author)')]/preceding-sibling::a") || page.xpath("//span/span[@class='contributorNameTrigger']/a")

			authors.each do |a|
				puts "++++++++++++++++++++"
				puts "++++++++++++++++++++"
				puts "Author(s): #{a.text}"
				author = a.text.split(" ")
				gender = JSON.parse(open("http://api.genderize.io?name=#{author[0]}").read)
				puts "Gender: #{gender['gender']}"
				puts "===================="
			end
		end
    end
    #file.close
  end
end