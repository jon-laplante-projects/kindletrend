require 'rubygems'
require 'rails'
require 'active_record'
require 'ancestry'

if ARGV[0] == "production"
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		:host => "localhost",
		#:pool => 5,
		:username => "kindle_trend",
		:password => "THzmVLcvaNojnbi"
	)
else
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		#:pool => 5,
		:username => "kindletrend",
		:password => "kindletrend",
	)
end

load '../app/models/author.rb'
load '../app/models/bestseller.rb'
load '../app/models/category.rb'
load '../app/models/category_group.rb'
load '../app/models/ebook.rb'
load '../app/models/rank.rb'
load '../app/models/reviewer.rb'
load '../app/models/rollup.rb'

#class Generate_rollup
	def go
		cat_group = CategoryGroup.where(complete: true).last
		@bestseller = Bestseller.where(category_group_id: cat_group.id, complete: true)

		@bestseller.each do |b|
			@category = Category.find(b.category_id)
			generate(b.id)
		end
	end

	def generate(bestseller_id)
		unless bestseller_id.nil?
			bestseller = Bestseller.find(bestseller_id)
			category = Category.find(bestseller.category_id)
			est = estimates(bestseller_id)
			rollup = Rollup.new
			rollup.bestseller_id = bestseller_id
			rollup.category_child_count = category.children.count
			rollup.category_book_count = category.count
			rollup.estimated_mean_sales = est["estimated_mean_sales"]
			rollup.estimated_mean_income = est["estimated_mean_earnings"]
			rollup.estimated_ceiling_sales = est["estimated_ceiling_sales"]
			rollup.estimated_ceiling_income = est["estimated_ceiling_earnings"]
			rollup.estimated_floor_sales = est["estimated_floor_sales"]
			rollup.estimated_floor_income = est["estimated_floor_earnings"]
			rollup.save
		end
	end

	def estimates(bestseller_id)
		unless bestseller_id.nil?
			bestseller = Bestseller.find(bestseller_id)
			cat_root = Category.where(category_group_id: bestseller.category_group_id).first

			estimates_hash = Hash.new

			# Ranks and price arrays.
			ranks_arr = Array.new
			price_arr = Array.new
			reviews_arr = Array.new
			pages_arr = Array.new
			filesizes_arr = Array.new

			bestseller.ebooks.each do |e|
				e.ranks.where(category_id: cat_root.id).each do |r|
					ranks_arr << r.rank
				end
			end

			bestseller.ebooks.each do |p|
				price_arr << p.price if p.price
				reviews_arr << p.reviews if p.reviews
				pages_arr << p.pages if p.pages
				filesizes_arr << p.filesize[0, p.filesize.length - 3].to_i if p.filesize
			end

			# Calculate means.
			mean_rank = (ranks_arr.inject{|sum,x| sum + x }/ranks_arr.size).to_i unless ranks_arr.empty?
			mean_price = (price_arr.inject{|sum,x| sum + x }/price_arr.size).to_f unless price_arr.empty?
			mean_reviews = (reviews_arr.inject{|sum,x| sum + x }/reviews_arr.size).to_i unless reviews_arr.empty?
			mean_pages = (pages_arr.inject{|sum,x| sum + x }/pages_arr.size).to_i unless pages_arr.empty?
			mean_filesize = (filesizes_arr.inject{|sum,x| sum + x }/filesizes_arr.size).to_i unless filesizes_arr.empty?

			# Calculate floors.
			# Floor rank is actually the HIGHTEST rank.
			floor_rank = ranks_arr.sort.last.to_i
			floor_price = price_arr.sort.first.to_f
			floor_reviews = reviews_arr.sort.first.to_i
			floor_pages = pages_arr.sort.first.to_i
			floor_filesize = filesizes_arr.sort.first.to_i

			# Calculate ceilings.
			# Ceiling rank is actually the LOWEST rank.
			ceiling_rank = ranks_arr.sort.first.to_i
			ceiling_price = price_arr.sort.last.to_f
			ceiling_reviews = reviews_arr.sort.last.to_i
			ceiling_pages = pages_arr.sort.last.to_i
			ceiling_filesize = filesizes_arr.sort.last.to_i

			mean_est_earnings = earningsEstimator(mean_rank, mean_price, mean_filesize, "mean")
			floor_est_earnings = earningsEstimator(floor_rank, floor_price, floor_filesize, "floor")
			ceiling_est_earnings = earningsEstimator(ceiling_rank, ceiling_price, ceiling_filesize, "ceiling")

			estimates_hash["estimated_mean_earnings"] = mean_est_earnings
			estimates_hash["estimated_ceiling_earnings"] = ceiling_est_earnings
			estimates_hash["estimated_floor_earnings"] = floor_est_earnings

			mean_est_sales = salesEstimator(mean_rank, "mean")
			floor_est_sales = salesEstimator(floor_rank, "floor")
			ceiling_est_sales = salesEstimator(ceiling_rank, "ceiling")

			estimates_hash["estimated_mean_sales"] = mean_est_sales
			estimates_hash["estimated_ceiling_sales"] = ceiling_est_sales
			estimates_hash["estimated_floor_sales"] = floor_est_sales

			return estimates_hash
		end
	end

	def earningsEstimator(rank, price, filesize=1, operation)
		daily_sales = salesEstimator(rank, operation)

		# Client.average("orders_count")
		# Client.minimum("age")
		# Client.maximum("age")

		# Based on price, determine commissions.
		# If royalty is 70%, delivery rate for Amazon.com US is $0.15/MB (no charge for 35% royalty)
		#
		# USD List Price Requirements for Amazon.com  				Minimum 		Maximum 
		# 35 % Royalty Option 										List Price 		List Price
   		# • Less than or equal to 3 megabytes 						$ 0.99 			$ 200.00
   		# • Greater than 3 megabytes and less than 10 megabytes 	$ 1.99 			$ 200.00
   		# • 10 megabytes or greater 								$ 2.99 			$ 200.00
		# 70 % Royalty Option 										$ 2.99 			$ 9.99

		if price > 2.99 && price < 9.99
			royalty = 0.7
		else
			# Royalty will be 35%
			royalty = 0.35
		end

		delivery = 0
		delivery = 0.15 * filesize/1024 if royalty == 0.7

		return ((daily_sales * 30) * (royalty * price)) - (delivery * daily_sales)
	end

	def salesEstimator(rank, operation)
		# Estimate the number of sales.
		# From http://www.theresaragan.com/p/sale-ranking-chart.html:::
		#
		# Amazon Best Seller Rank 50,000 to 100,000 - selling close to 1 book a day.
		# Amazon Best Seller Rank 10,000 to 50,000 - selling 5 to 15 books a day.
		# Amazon Best Seller Rank 5,500 to 10,000 - selling 15 to 25 books a day.
		# Amazon Best Seller Rank 3,000 to 5,500 - selling 25 to 70 books a day.
		# Amazon Best Seller Rank 1,500 to 3,000 - selling 70 to 100 books a day.
		# Amazon Best Seller Rank 750 to 1,500 - selling 100 to 120 books a day.
		# Amazon Best Seller Rank 500 to 750 - selling 120 to 175 books a day.
		# Amazon Best Seller Rank 350 to 500 - selling 175 to 250 books a day.
		# Amazon Best Seller Rank 200 to 350 - selling 250 to 500 books a day.
		# Amazon Best Seller Rank 35 to 200 - selling 500 to 2,000 books a day.
		# Amazon Best Seller Rank 20 to 35 - selling 2,000 to 3,000 books a day.
		# Amazon Best Seller Rank of 5 to 20 - selling 3,000 to 4,000 books a day.
		# Amazon Best Seller Rank of 1 to 5 - selling 4,000+ books a day.
		if rank <= 200000
			case rank
			when 100000...200000
				daily_sales = 0.034
			when 50000...100000
				daily_sales = 1
			when 10000...50000
				case operation
					when "mean"
						daily_sales = 12
					when "median"
						daily_sales = 10
					when "ceiling"
						daily_sales = 15
					when "floor"
						daily_sales = 5
				end
			when 5500...10000
				case operation
					when "mean"
						daily_sales = 22
					when "median"
						daily_sales = 20
					when "ceiling"
						daily_sales = 25
					when "floor"
						daily_sales = 15
				end
			when 3000...5500
				case operation
					when "mean"
						daily_sales = 62
					when "median"
						daily_sales = 57
					when "ceiling"
						daily_sales = 70
					when "floor"
						daily_sales = 25
				end
			when 1500...3000
				case operation
					when "mean"
						daily_sales = 88
					when "median"
						daily_sales = 85
					when "ceiling"
						daily_sales = 100
					when "floor"
						daily_sales = 70
				end
			when 750...1500
				case operation
					when "mean"
						daily_sales = 113
					when "median"
						daily_sales = 110
					when "ceiling"
						daily_sales = 120
					when "floor"
						daily_sales = 100
				end
			when 500...750
				case operation
					when "mean"
						daily_sales = 150
					when "median"
						daily_sales = 145
					when "ceiling"
						daily_sales = 175
					when "floor"
						daily_sales = 120
				end
			when 350...500
				case operation
					when "mean"
						daily_sales = 210
					when "median"
						daily_sales = 200
					when "ceiling"
						daily_sales = 250
					when "floor"
						daily_sales = 175
				end
			when 200...350
				case operation
					when "mean"
						daily_sales = 400
					when "median"
						daily_sales = 375
					when "ceiling"
						daily_sales = 500
					when "floor"
						daily_sales = 250
				end
			when 35...200
				case operation
					when "mean"
						daily_sales = 1300
					when "median"
						daily_sales = 1250
					when "ceiling"
						daily_sales = 2000
					when "floor"
						daily_sales = 500
				end
			when 20...35
				case operation
					when "mean"
						daily_sales = 2600
					when "median"
						daily_sales = 2500
					when "ceiling"
						daily_sales = 3000
					when "floor"
						daily_sales = 2000
				end
			when 5...20
				case operation
					when "mean"
						daily_sales = 3650
					when "median"
						daily_sales = 3500
					when "ceiling"
						daily_sales = 4000
					when "floor"
						daily_sales = 3000
				end
			when 1...5
				case operation
					when "mean"
						daily_sales = 8000
					when "median"
						daily_sales = 7500
					when "ceiling"
						daily_sales = 10000
					when "floor"
						daily_sales = 4000
				end
			end
		else
			daily_sales = 0
		end

		return daily_sales
	end

	def median(array)
	  sorted = array.sort
	  len = sorted.length
	  return (sorted[(len - 1) / 2] + sorted[len / 2]) / 2.0
	end
#end

#rollups = Generate_rollup.new
#rollups.initialize
go()
