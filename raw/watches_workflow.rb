require 'rubygems'
require 'rails'
require 'rss'
require 'open-uri'
require 'nokogiri'
require 'active_record'
require 'ancestry'

if ARGV[1] == "production"
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		:host => "localhost",
		#:pool => 5,
		:username => "kindle_trend",
		:password => "THzmVLcvaNojnbi"
	)
else
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		#:pool => 5,
		:username => "kindletrend",
		:password => "kindletrend",
	)
end

load '../app/models/author.rb'
load '../app/models/bestseller.rb'
load '../app/models/category.rb'
load '../app/models/category_group.rb'
load '../app/models/ebook.rb'
load '../app/models/rank.rb'
load '../app/models/reviewer.rb'
load '../app/models/rollup.rb'
load 'process_watches.rb'

def run
	puts "Starting run..."
	@cat_group = CategoryGroup.where(complete: true).last
	puts "Category_group_id: #{@cat_group.id}"
	puts "Watch: #{ARGV[2]}"
	@watches = Category.where("category_group_id = #{@cat_group.id} AND watch = #{ARGV[2]}")

	puts "Processing:"
	@watches.each do |w|
		puts "#{w.name}"

		# Create the bestseller rec.
		bestseller = Bestseller.new
		bestseller.category_id = w.id
		bestseller.count = @bestseller_count
		bestseller.category_group_id = @cat_group.id
		bestseller.watch = w.watch
		bestseller.save

		url = "http://www.amazon.com/gp/rss/bestsellers/digital-text/#{w.asin}/ref=zg_bs_#{w.asin}_rsslink"
		puts "URL: #{url}"

		user_agent = Mechanize.new {|agent| agent.user_agent_alias = "Mac Firefox"}

		rand_browser = 1 + rand(7)
		case(rand_browser)
			when 1 then agent_alias = 'Mac Firefox'
			when 2 then agent_alias = 'Mac Mozilla'
			when 3 then agent_alias = 'Windows Chrome'
			when 4 then agent_alias = 'Windows IE 8'
			when 5 then agent_alias = 'Windows IE 9'
			when 6 then agent_alias = 'Windows Mozilla'
			when 7 then agent_alias = 'Linux Firefox'
		end

		feed = RSS::Parser.parse(user_agent.get(url).content)
		feed.items.each do |item|
			book = Watch.new("#{item.link.strip}", "#{bestseller.id}", "#{agent_alias}", "#{item.title}")
			book.save(bestseller.id)

			sleep(rand(8..10).seconds)
		end

		# system bundle exec ruby bestseller_workflow.rb run DB_environment bestseller_id watch_number
		system "bundle exec ruby bestseller_workflow.rb run #{ARGV[1]} #{bestseller.id} #{ARGV[2]}"

		bestseller.complete = true
		bestseller.save
	end

	system "bundle exec ruby generate_rollups.rb #{ARGV[1]}"

	puts "========================================="
	puts "=    Done!                              ="
	puts "========================================="
end

# ARGV[0] = Run the program
# ARGV[1] = Which database should I connect to?
# ARGV[2] = Which watch should be run?
case ARGV[0]
	when "run"
	run()
end
