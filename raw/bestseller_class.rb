# encoding: UTF-8

require 'rails'
require 'rss'
require 'open-uri'
require 'nokogiri'
require 'mechanize'
require 'active_record'
require 'rails'
require 'ancestry'
load 'error_logger.rb'

if ARGV[0] == "production"
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		:host => "localhost",
		#:pool => 5,
		:username => "kindle_trend",
		:password => "THzmVLcvaNojnbi"
	)
else
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		#:pool => 5,
		:username => "kindletrend",
		:password => "kindletrend",
	)
end

class CategoryGroup < ActiveRecord::Base
	has_many :categories
end

class Category < ActiveRecord::Base
	has_ancestry
	belongs_to :category_group
	has_many :bestsellers
end

class Bestseller < ActiveRecord::Base
	belongs_to :category
	has_many :ebooks
end

class Ebook < ActiveRecord::Base
	belongs_to :bestseller
	has_many :authors
	has_many :ranks
	has_many :reviewers
end

class Author < ActiveRecord::Base
	belongs_to :ebook
end

class Rank < ActiveRecord::Base
	belongs_to :ebook
end

class Reviewer < ActiveRecord::Base
	belongs_to :ebook
end

class Book
attr_accessor :url
attr_reader :title, :ranks, :categories, :rating, :reviews, :reviewers, :authors, :publication_date, :publisher, :price, :pages, :description, :filesize

	#def initialize(url='http://www.amazon.com/gp/rss/bestsellers/digital-text/154606011/ref=zg_bs_154606011_rsslink', rank=nil)
	def initialize(ebook_id)
		@ebook = Ebook.find(ebook_id)
		@url = "#{@ebook.url}"
		@page = page

		# Get the rank for the niche stores.
		items = code.xpath("li[@class='zg_hrsr_item']")

		items.each do |item|
			i = Nokogiri::HTML("#{item}")

			# Kindle Store rank.
			i_rank = i.xpath("span[@class='zg_hrsr_rank']").text
			i_ladder = ""

			# Niches...
			ranks = code.xpath("ul[@class='zg_hrsr']")
			ladders = ranks[0].xpath("span[@class='zg_hrsr_ladder']")

			# Get other ranking niches.
			i_ladder = items.xpath("//*[contains(concat(' ', @class, ' '), ' zg_hrsr_ladder ')]").text

			@rank = i_rank
			#ranks(i_ladder) # Should 'Ranks' be an array?
		end
	end

	def url
		@url
	end

	def title
		begin
			@page.xpath("//span[@id='btAsinTitle']/text()").to_s.strip.encode("ISO-8859-1").force_encoding("UTF-8").scrub
		rescue
			@page.xpath("//span[@id='btAsinTitle']/text()").to_s.strip
		ensure
			unless @page.xpath("//span[@id='btAsinTitle']/text()").blank?
				@page.xpath("//span[@id='btAsinTitle']/text()").to_s.strip
			else
				puts "................................."
				puts "................................."
				puts "................................."
				puts @page.to_s
				puts "................................."
				puts "................................."
				puts "................................."
				nil
			end
		end
	end

	def page
		rand_browser = 1 + rand(7)
		case(rand_browser)
			when 1 then agent_alias = 'Mac Firefox'
			when 2 then agent_alias = 'Mac Mozilla'
			when 3 then agent_alias = 'Windows Chrome'
			when 4 then agent_alias = 'Windows IE 8'
			when 5 then agent_alias = 'Windows IE 9'
			when 6 then agent_alias = 'Windows Mozilla'
			when 7 then agent_alias = 'Linux Firefox'
		end

		user_agent = Mechanize.new {|agent| agent.user_agent_alias = "#{agent_alias}"}
		
		page_data = ""

		user_agent.get(@url) { |d| page_data = d.content }
	    #Nokogiri::HTML(user_agent.get(@url).content)
	    Nokogiri::HTML(page_data)
	end

	def snippet
		@page.xpath("li[@id='SalesRank']")
	end

	def code
		Nokogiri::HTML("#{snippet}")
	end

	def ranks
		ranks_arr = Array.new
		bestseller = Bestseller.find(@ebook.bestseller_id)

		primary_rank = @page.xpath('//li[@id="SalesRank"]/text()').to_s.strip

		unless primary_rank.blank?
			primary_rank = primary_rank[0, primary_rank.length - 24]
			primary_rank = primary_rank[1, primary_rank.length - 1]
			primary_rank = primary_rank.delete(",").to_i

			primary_hash = Hash.new
			primary_hash["rank"] = "#{primary_rank}"
			# Get the root of the current category group.
			cat_root = Category.where(category_group_id: bestseller.category_group_id).first
			primary_hash["category"] = cat_root.id
			ranks_arr << primary_hash
		end

		ranks = @page.xpath('//li[@id="SalesRank"]/ul/li')

		ranks.each do |r|
			other_ranks = Hash.new

			rank = r.xpath("span")
			rank_ladder = rank[1].text.to_s.split(" > ")
			if "#{rank_ladder[0]}" == "in Kindle Store"
				rank_str = rank[0].text
				other_ranks["rank"] = rank_str[1, rank_str.length - 1]

				rank_ladder.shift

				cat_group = CategoryGroup.where(complete: true).last
				current_root = Category.where(category_group_id: cat_group.id).first
				ancestry = current_root.id.to_s
				#ancestry = "1"
				for x in 0..rank_ladder.length - 1
					if rank_ladder[x] != "Kindle eBooks"
						rung = Category.where(:ancestry => "#{ancestry}", :name => "#{rank_ladder[x]}").first
						unless rung.blank?
							if x != rank_ladder.length - 1
								ancestry << "/#{rung.id.to_s}"
							else
								#puts "Ancestry: #{ancestry}"
								#puts "Name: #{rank_ladder[x]}"
								if rung
									other_ranks["category"] = rung.id
								else
									c = Category.new
									c.parent = ancestry.split("/").pop
									c.name = "#{rank_ladder[x]}"
									other_ranks["category"] = rung.id
								end
							end
						end
					end
				end

				ranks_arr << other_ranks
			end
		end

		return ranks_arr
	end

	def rating
		rating = @page.xpath("//*[contains(text(),'out of')]/text()").first.to_s

		unless rating.blank?
			rating[0, rating.length - 15].to_f
		else
			nil
		end
	end

	def reviews
		review_count = @page.xpath("//*[contains(text(),'customer review')]/text()").first.to_s
		unless review_count.blank?
			review_count = review_count.to_s
			review_count = review_count[0, review_count.length - 17]
			review_count = review_count.delete(",")
		else
			review_count = 0
		end

		review_count.to_i
	end

	def authors
		writers = Array.new

		if @page.xpath("//span[@class='contributorNameTrigger']/a").count > 0
			author_list = @page.xpath("//span[@class='contributorNameTrigger']/a")

			author_list.each do |a|
				auth_hash = Hash.new
				unless a.text.blank?
					begin
						auth_hash["name"] = a.text.encode("ISO-8859-1").force_encoding("UTF-8").scrub
					rescue
						auth_hash["name"] = a.text
					end
					author = a.text.split(" ")
					auth_hash["gender"] = gender(author[0])
					auth_hash["authorcentral"] = authorcentral(auth_hash["name"]) #?  : nil
					writers << auth_hash
				end
			end
		else
			auth_arr = @page.xpath("//h1[contains(concat(' ',@class,' '), 'parseasinTitle')]/following-sibling::span/a")
			if auth_arr.count > 0
				auth_arr.each do |a|
					auth_hash = Hash.new
					#auth_hash["name"] = @page.xpath("//h1[contains(concat(' ',@class,' '), 'parseasinTitle')]/following-sibling::span/a").text
					auth_hash["name"] = "#{a.text}"
					author = auth_hash["name"].split(" ")
					titles = ["Dr.","Mr.","Mrs.","Miss","Ms.","Representative","Senator","Speaker","President","Councillor","Alderman","Delegate","Mayor","Governor","Prefect","Prelate","Premier","Burgess","Ambassador","Envoy","Secretary","Attaché","Provost","Prince","Princess","Archduke","Archduchess","Earl","Viscount","Lord","Emperor","Empress","Tsar","Tsarina","Tsaritsa","King","Pope","Bishop","Father","Pastor","Cardinal","Colonel","General","Commodore","Corporal","Mate","Sergeant","Admiral","Brigadier","Captain","Commander","Officer","Lieutenant","Major","Private","Doctor"]
					unless titles.include?(author[0])
						auth_hash["gender"] = gender(author[0])
					else
						auth_hash["gender"] = gender(author[1])
					end
					auth_hash["authorcentral"] = authorcentral(auth_hash["name"])
					writers << auth_hash
				end
			else
				writers = nil
			end
		end

		return writers
	end

	def price
		price = @page.xpath("//*[contains(concat(' ', @class, ' '), ' priceLarge ')]/text()").to_s.strip

		unless price.blank?
			price[1,price.length].to_f
		else
			price = 0.to_f
		end
	end

	def pages
		page_count = @page.xpath("//*[contains(text(),'Print Length:')]/..").text
		page_count.slice! "Print Length: "
		page_count.slice! " pages"

		unless page_count.blank?
			page_count.to_i
		else
			nil
		end
	end

	def publication_date
		unless @page.xpath("//input[@id='pubdate']/@value").to_s.to_date.blank?
			@page.xpath("//input[@id='pubdate']/@value").to_s.to_date
		else
			nil
		end
	end

	def publisher
		publisher = @page.xpath("//b[contains(text(), 'Publisher')]/..").text.strip

		unless publisher.blank?
			publisher[11, publisher.length - 1]
		else
			nil
		end
	end

	def filesize
		filesize = @page.xpath("//b[contains(text(), 'File Size:')]/..").text

		unless filesize.blank?
			filesize[11, filesize.length - 1]
		else
			nil
		end
	end

	def description
		#@page.xpath("//div/div[@class='content']/noscript").text
		unless @page.xpath("//div[@id='postBodyPS']").text.blank?
			@page.xpath("//div[@id='postBodyPS']").text
		else
			nil
		end
	end

	def keywords
		keywords = "#{@description.text}".downcase.gsub(/[^a-z\s]/, '').split(" ")
		# Build stop word list...
		stopwords = ["i","a","about","an","are","as","at","be","by","com","for","from","how","in","is","it","of","on","or","that","the","this","to","was","what","when","where","who","will","with","the","www"]
		keywords.delete(stopwords)
		counts = Hash.new(0)
		keywords.each { |keyword| counts[keyword] += 1 }
	end

	def gender(name)
		gender = JSON.parse(open("http://api.genderize.io?name=#{name.to_s}").read) unless name.blank?

		unless gender.blank?
			return "#{gender['gender']}"
		else
			nil
		end
	end

	def authorcentral(author)
		unless author.blank?
			@page.xpath(%Q[//a[contains(text(),"#{author} Page")]/@href/text()]).to_s.strip
		else
			nil
		end
	end

	def authorcentral_twitter(url)
		unless url.blank?
			begin
				ac_page = Nokogiri::HTML(open(url))

				return ac_page.xpath("//p[@class='tweetScreenName']/a.text()").to_s.strip
			rescue
				return nil
			end
		end
	end

	def save(bestseller_id)
		#begin
			unless title.blank?
	  			#ActiveRecord::Base.transaction do
					#bestseller = Bestseller.find(bestseller_id)

					#ebook = bestseller.ebooks.new
					@ebook.name = "#{title}"
					@ebook.rating = rating #unless rating.blank?
					@ebook.reviews = reviews #unless reviews.blank?
					@ebook.price = price #unless price.blank?
					@ebook.pages = pages #unless pages.blank?
					#ebook.rank = @base_rank
					@ebook.desc = "#{description}" #unless description.blank?
					#@ebook.url = "#{url}"
					@ebook.publisher = "#{publisher}" #unless publisher.blank?
					@ebook.publication_date = "#{publication_date}" #unless publication_date.blank?
					@ebook.filesize = "#{filesize}" #unless filesize.blank?
					@ebook.save

					authors().each do |a|
						author = @ebook.authors.new
						author.name = "#{a['name']}" #unless a['name'].blank?
						author.gender = "#{a['gender']}" #unless a['gender'].blank?
						author.authorcentral = "#{a['authorcentral']}" #unless a['authorcentral'].blank?
						author.twitter = "#{authorcentral_twitter(a['authorcentral'])}" #unless a['authorcentral'].blank?
						author.save
					end

					ranks().each do |r|
						rank = @ebook.ranks.new
						rank.category_id = r["category"] unless r["category"].blank?
						rank.rank = r["rank"] unless r["rank"].blank?
						rank.save
					end
				#end
			end
		#rescue Exception => e
		##	raise ActiveRecord::Rollback
		#	log_err(e)
		#end
	end

def output
		#begin
  		#	ActiveRecord::Base.transaction do
				category = Category.find(1)
				puts "Bestseller.category_id = #{category.id}"
				puts "name = #{title}"
				puts "rating = #{rating}"
				puts "reviews = #{reviews}"
				puts "price = #{price}"
				puts "pages = #{pages}"
				#puts "rank = ranks[0]['rank']"
				puts "desc = #{description()}"
				puts "url = #{url}"
				puts "publisher = #{publisher}"
				puts "publication_date = #{publication_date}"
				puts "filesize = #{filesize}"
				#ebook.save

				authors().each do |a|
					#author = ebook.authors.new
					puts "author.name = #{a['name']}"
					puts "author.gender = #{a['gender']}"
					puts "author.authorcentral = #{a['authorcentral']}" unless a['authorcentral'].blank?
					puts "author.twitter = #{authorcentral_twitter(a['authorcentral'])}" unless a['authorcentral'].blank?
				#	author.save
				end
#
#				ranks().each do |r|
#					#rank = ebook.ranks.new
#					puts "rank.category_id = #{r['category']}"
#					puts "rank.rank = #{r['rank']}"
#					#rank.save
#				end
		#	end
		#rescue Exception => e
		#	raise ActiveRecord::Rollback
		#	log_err(e)
		#end
	end

	def recovery

	end
end
