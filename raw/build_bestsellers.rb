require 'rubygems'
require 'rails'
require 'rss'
require 'open-uri'
require 'nokogiri'
require 'active_record'
require 'ancestry'

if ARGV[1] == "production"
  ActiveRecord::Base.establish_connection(
    :adapter => "mysql2",
    :encoding => "utf8",
    :database => "kindletrend_production",
    :host => "localhost",
    #:pool => 5,
    :username => "kindle_trend",
    :password => "THzmVLcvaNojnbi"
  )
else
  ActiveRecord::Base.establish_connection(
    :adapter => "mysql2",
    :encoding => "utf8",
    :database => "kindletrend_production",
    #:pool => 5,
    :username => "kindletrend",
    :password => "kindletrend",
  )
end

load '../app/models/author.rb'
load '../app/models/bestseller.rb'
load '../app/models/category.rb'
load '../app/models/category_group.rb'
load '../app/models/ebook.rb'
load '../app/models/rank.rb'
load '../app/models/reviewer.rb'
load '../app/models/rollup.rb'

def run
  # Get the current category...
  @cat_group = CategoryGroup.where(complete: true).last

  watches = Category.where("category_group_id = #{@cat_group.id} AND watch = #{ARGV[2]}")

  watches.each do |w|
    bestseller = Bestseller.new
    bestseller.category_id = w.id
    bestseller.count = @bestseller_count
    bestseller.category_group_id = @cat_group.id
    bestseller.watch = w.watch
    bestseller.save
  end
end

# ARGV[0] = run
# ARGV[1] = environment (production/development/test)
# ARGV[2] = watch to process
case ARGV[0]
  when "run"
  run()
end