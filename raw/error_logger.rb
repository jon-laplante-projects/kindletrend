require 'rubygems'

def log_err(e)
	File.open("../log/write_failure.log", "a") do |f|
		f.write("Datetime: #{Time.now}\n\n")
		f.write("#{e.message}\n")
		f.write("====================================================================\n\n")
	end
end