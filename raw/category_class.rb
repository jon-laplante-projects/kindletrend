require 'rubygems'
require 'rails'
require 'active_record'
#require 'acts_as_tree'
require 'ancestry'
require 'jbuilder'

if Rails.env == "production"
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		:host => "localhost",
		#:pool => 5,
		:username => "kindle_trend",
		:password => "THzmVLcvaNojnbi"
	)
else
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		#:pool => 5,
		:username => "kindletrend",
		:password => "kindletrend",
	)
end

ActiveRecord::Base.include_root_in_json = true

class Category < ActiveRecord::Base
	has_ancestry

	def self.json_tree(nodes)
    	nodes.map do |node, sub_nodes|
      		{:name => node.name, :id => node.id, :children => json_tree(sub_nodes).compact}
    	end
end
end

class Department
	#attr_reader :hello
	attr_accessor :category_id, :category_name

	def initialize(category_id=nil, category_name=nil)
		@category_id = category_id
		@category_name = category_name
	end

	def children
		Category.where(:parent => @category_id)
	end

	def parent
		rec = Category.find(@category_id)
		Category.find(rec.parent)
	end

	def decendants
		root = Category.find(@category_id)
		root.children
	end

	def ancestors
		root = Category.find(@category_id)
		root.ancestors
	end

	def hierarchy
		file = File.open("tree.htm", "w")
		root = Category.descendants_of(@category_id)
		#root = Category.descendants_of(@category_id).pluck(:name).arrange_serializable
		html = '<!DOCTYPE html><meta charset="utf-8">'
		html << '<link rel="stylesheet" href="jOrgChart/example/css/jquery.jOrgChart.css" />'
		html << '<link rel="stylesheet" href="jOrgChart/example/css/custom.css" />'
		html << '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>'
		html << '<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>'
		html << '<script src="jOrgChart/jquery.jOrgChart.js"></script>'
		html << '<script>'
		html << '$(document).ready(function(){'
		html << '$("#menu").jOrgChart();'
		html << '});'
		html << '</script><body>'
		html << '<ul id="menu" style="display:none;"><li>Kindle eBooks<ul>'
		root.each do |category|
			html << '<li>' + category.name
			# depending on the depth of your tree it is better to rely on an helper
			# to drill into the level of the tree
			unless category.children.empty?
				html << '<ul id="sub-menu">'
				category.children.each do |subcategory|
					html << '<li>' + subcategory.name + '</li>'
				end
			html << '</li></ul>'
			end
		end
		html << '</ul></li>'
		html << '</ul>'
		html << '</body></html>'
		#root = Category.find(@category_id).subtree.arrange
		#tree = root.arrange_serializable.to_json
		file.write(html)
		file.close
	end

		def generate_json
		file = File.open("tree.json", "w")
		node = Category.find(1).descendants
		built_json = Jbuilder.encode do |json|
			json.code "Kindle eBooks"
			
		end
		#json = '{"name": "Kindle eBooks", "children": ['
		#json << node.descendants.arrange_serializable.to_json
		#json << ']}'
		#root = Category.descendants_of(@category_id)
		##root = Category.descendants_of(@category_id).pluck(:name).arrange_serializable
		#json = '{ "name": "Kindle eBooks", "children" : [{'
		#root.each do |category|
		#	if category.id < 3
		#		json << '"name": "' + category.name + '"'
		#	else
		#		json << ',"name": "' + category.name + '"'
		#	end
		#
		#	# depending on the depth of your tree it is better to rely on an helper
		#	# to drill into the level of the tree
		#	unless category.children.empty?
		#		#json << ','
		#		json << '"children": ['
		#		category.children.each do |subcategory|
		#			if subcategory.id == category.children[0].id 
		#				json << '{ "name": "' + subcategory.name + '" }'
		#			else
		#				json << ', { "name": "' + subcategory.name + '" }'
		#			end
		#		end
		#
		#	#if category.id == category.children.last.id
		#		json << ']'
		#	#else
		#	#	json << '],'
		#	#end
		#	end
		#	json << '}'
		#end
		#json << '}'
		##root = Category.find(@category_id).subtree.arrange
		##tree = root.arrange_serializable.to_json
		file.write(built_json)
		file.close
	end

	private
	def get_ancestors()

	end
end