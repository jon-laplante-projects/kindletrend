##################################################
# NOTE: Crawling a category that has less than
# 100 books will FAIL OUT!!!
##################################################
require 'rubygems'
require 'rails'
require 'open-uri'
require 'nokogiri'
require 'active_record'
load 'bestseller_class.rb'
load 'error_logger.rb'

if ARGV[1] == "production"
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		:host => "localhost",
		#:pool => 5,
		:username => "kindle_trend",
		:password => "THzmVLcvaNojnbi"
	)
else
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		#:pool => 5,
		:username => "kindletrend",
		:password => "kindletrend",
	)
end

class CategoryGroup < ActiveRecord::Base
	has_many :categories
end

class Category < ActiveRecord::Base
	has_ancestry
	belongs_to :category_group
	has_many :bestsellers
end

class Bestseller < ActiveRecord::Base
	belongs_to :category
	has_many :ebooks
end

class Ebook < ActiveRecord::Base
	belongs_to :bestseller
	has_many :authors
	has_many :ranks
	has_many :reviewers
end

class Author < ActiveRecord::Base
	belongs_to :ebook
end

class Rank < ActiveRecord::Base
	belongs_to :ebook
end

class Reviewer < ActiveRecord::Base
	belongs_to :ebook
end

def get_pages(url_id)
	#puts "At for...loop"
	link_arr = Array.new
	for x in (5).downto(1)
		puts "X: #{x}"
		list_link = "http://www.amazon.com/Best-Sellers-Kindle-Store/zgbs/digital-text/#{url_id}?_encoding=UTF8&pg=#{x}"
		page = Nokogiri::HTML(open(list_link).read)

		@bestseller_count = if page.xpath("//a[@page='5']") == ""
			if page.xpath("//a[@page='4']") == ""
				if page.xpath("//a[@page='3']") == ""
					if page.xpath("//a[@page='2']") == ""
						page.xpath("//a[@page='1']").text.to_s.split("-")[1]
					else
						page.xpath("//a[@page='2']").text.to_s.split("-")[1]
					end
				else
					page.xpath("//a[@page='3']").text.to_s.split("-")[1]
				end
			else
				page.xpath("//a[@page='4']").text.to_s.split("-")[1]
			end
		else
			page.xpath("//a[@page='5']").text.to_s.split("-")[1]
		end

		case x
			when 5
				item_num = 100
			when 4
				item_num = 80
			when 3
				item_num = 60
			when 2
				item_num = 40
			when 1
				item_num = 20
		end

		unless x == 1
			begin
			link_hash = Hash.new
			item = page.xpath("//span[@class='zg_rankNumber'][contains(text(),'#{item_num}.')]/../../div/div[2]/a")
			i = item.xpath('@href').text.strip!
			i = i.gsub("\n", "")
			link_hash["url"] = "#{i}"
			#link_hash["url"] = "bestsellers/#{@category.id}_#{@category.name}_#{item_num}.html"
			link_hash["rank"] = "#{item_num}"

			link_arr << link_hash

			#html = Nokogiri::HTML(open("#{i}"))
			#File.open("bestsellers/#{@category.id}_#{@category.name}_#{item_num}.html", "w") do |file|
			#	file.write(html.to_s)
			#end

			rescue Exception => e
				log_err(e)
				link_hash["url"] = nil
				link_hash["rank"] = "#{item_num}"
			end
		else
			for y in 11..20
				puts "Y: #{y}"
				link_hash = Hash.new
				#item = page.xpath("//span[@class='zg_rankNumber'][contains(text(),'#{y}.')]/../../div/div[2]/a")
				item = page.xpath("//span[@class='zg_rankNumber'][.='#{y}.']/../../div/div[2]/a")
				##link_arr << "#{item.xpath('@href').text.strip!}"
				i = item.xpath('@href').text.strip!
				i = i.gsub("\n", "")
				link_hash["url"] = "#{i}"
				#link_hash["url"] = "bestsellers/#{@category.id}_#{@category.name}_#{y}.html"
				link_hash["rank"] = "#{y}"

				link_arr << link_hash
				#html = Nokogiri::HTML(open("#{i}"))
				#File.open("bestsellers/#{@category.id}_#{@category.name}_#{y}.html", "w") do |file|
				#	file.write(html.to_s)
				#end
			end
		end
	end

	#File.open("link_array_#{@category_id}.txt", "w") do |file|
	#	file.write(link_arr.to_s)
	#end

	puts "Links: #{link_arr}"
	if ARGV[2].blank?
		bestseller = Bestseller.new
		bestseller.category_id = @category_id
		bestseller.count = @bestseller_count
		bestseller.category_group_id = @cat_group.id
		bestseller.watch = @categories.watch
		bestseller.save
	else
		bestseller = Bestseller.find(ARGV[2])
	end

	link_arr.each do |a|
		ebook = bestseller.ebooks.new
		ebook.bestseller_id = bestseller.id
		ebook.url = "#{a['url']}"
		ebook.rank = a["rank"]
		ebook.save
	end

	bestseller.ebooks.each do |b|
		#begin
			sleep(rand(4..8).seconds)
			book = Book.new(b.id)
			book.save(bestseller.id)
		#rescue
		#	File.open("../log/write_failure.log", "a") do |f|
		#		f.write("Datetime: #{Time.now}\n\n")
		#		f.write("ID: #{b.id}\n")
		#		f.write("====================================================================\n\n")
		#	end
		#end
	end
end

def run
	#categories = Category.where(:ancestry => "1")
	##count = 1
	#
	#categories.each do |c|
	#	puts "#{c.id}. #{c.name}"
	#	#count += 1
	#end
	#
	#@category_id = gets.to_i
	if ARGV[2].blank?
		@cat_group = CategoryGroup.where(complete: true).last
		@categories = @cat_group.categories.where(:watch => ARGV[3])
	else
		###############################
		## Duplicates functionality. ##
		###############################
		bestseller = Bestseller.find(ARGV[2])
		@cat_group = CategoryGroup.find(bestseller.category_group_id)
		@categories = @cat_group.categories.where(id: bestseller.category_id)
	end

	puts "Processing:"
	@categories.each do |c|
		@category_id = c.id

		@category = c
		puts "#{c.name}"

		category = @cat_group.categories.find(@category_id)
		url_id = category.asin

		get_pages(url_id)
	end

	puts "========================================="
	puts "=    Done!                              ="
	puts "========================================="
	#category = Category.find(@category_id)
	#url_id = category.url[7,category.url.to_s.length - 6].split("/").pop
	#
	#get_pages(url_id)
end

# ARGV[0] = "run"
# ARGV[1] = production/development database server
# ARGV[2] = Bestseller to build out.
case ARGV[0]
	when "run"
	run()
end
