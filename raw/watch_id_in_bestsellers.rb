require 'rubygems'
require 'rails'
require 'rss'
require 'open-uri'
require 'nokogiri'
require 'active_record'
require 'ancestry'

if ARGV[0] == "production"
  ActiveRecord::Base.establish_connection(
    :adapter => "mysql2",
    :encoding => "utf8",
    :database => "kindletrend_production",
    :host => "localhost",
    #:pool => 5,
    :username => "kindle_trend",
    :password => "THzmVLcvaNojnbi"
  )
else
  ActiveRecord::Base.establish_connection(
    :adapter => "mysql2",
    :encoding => "utf8",
    :database => "kindletrend_production",
    #:pool => 5,
    :username => "kindletrend",
    :password => "kindletrend",
  )
end

load '../app/models/author.rb'
load '../app/models/bestseller.rb'
load '../app/models/category.rb'
load '../app/models/category_group.rb'
load '../app/models/ebook.rb'
load '../app/models/rank.rb'
load '../app/models/reviewer.rb'
load '../app/models/rollup.rb'

bestsellers = Bestseller.all

bestsellers.each do |b|
	cat = Category.find(b.category_id)

	b.watch = cat.watch
	b.save
end

puts "Done!"