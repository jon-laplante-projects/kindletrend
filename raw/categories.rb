require 'rubygems'
require 'rails'
require 'nokogiri'
require 'mechanize'
require 'open-uri'
require 'active_record'
require 'ancestry'
load 'error_logger.rb'

#@id_ladder = [1,8,789,831]
@page = ""

#ActiveRecord::Base.establish_connection(
#	:adapter => "sqlite3",
#	:database => "../db/data.sqlite3"
#)

if ARGV[1] == "production"
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		:host => "localhost",
		#:pool => 5,
		:username => "kindle_trend",
		:password => "THzmVLcvaNojnbi"
	)
else
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		#:pool => 5,
		:username => "kindletrend",
		:password => "kindletrend",
	)
end

class CategoryGroup < ActiveRecord::Base
	has_many :categories
end

class Category < ActiveRecord::Base
	has_ancestry
	belongs_to :category_group
end

def crawl(id)
	node = Category.find(id)

	if node.has_children?
		node = node.children.first
	else
		node = Category.find(siblings(node.id))
	end
	
	#get_categories(node.id)
	parse_categories(node.id)
end
# End crawl.

def siblings(id)
	node = Category.find(id)
	group_root_id = CategoryGroup.where(id: node.category_group_id).pluck("group_root_id").first
	
	if node.id == group_root_id
		cat_group = CategoryGroup.find(node.category_group_id)
		cat_group.complete = true
		cat_group.save

		abort "Congratulations! You have a map of all Kindle categories!"
		exec "open Notification.app"
	elsif node.has_siblings?
		sibs = node.sibling_ids.sort
		if sibs[sibs.index(node.id).to_i] < sibs.last
			sibs[sibs.index(node.id).to_i + 1]
		else
			siblings(node.parent_id)
		end
	else
		siblings(node.parent_id)
	end
end

def update_watches
	last_watch = Category.where("watch IS NOT NULL").last
	count = Category.where("category_group_id = #{cat_group.id} AND watch > 0").count

	unless count > 0
		prior_watches = Category.where("category_group_id = #{cat_group.id} AND watch > 0")
		current_root = Category.where(category_group_id: cat_group.id).first
	end
end

def parse_categories(id)
	node = Category.find(id)
	puts "Current node: #{node.id}"

	# Get the root page.
	#@page = Nokogiri::HTML(open(node.url))

	rand_browser = 1 + rand(7)
	case(rand_browser)
		when 1 then agent_alias = 'Mac Firefox'
		when 2 then agent_alias = 'Mac Mozilla'
		when 3 then agent_alias = 'Windows Chrome'
		when 4 then agent_alias = 'Windows IE 8'
		when 5 then agent_alias = 'Windows IE 9'
		when 6 then agent_alias = 'Windows Mozilla'
		when 7 then agent_alias = 'Linux Firefox'
	end

	user_agent = Mechanize.new {|agent| agent.user_agent_alias = "#{agent_alias}"}

	user_agent.get(node.url) { |d| @page = d.content }

	@page = Nokogiri::HTML(@page)

	# Get the categories navigation section of the page.
	if node.name == "Kindle eBooks"
		nav = @page.xpath('//ul[@id="ref_154606011"]')
	else
		@xpath_var = "//ul[@id='ref_#{node.asin}']"
		
		puts "Nav. xpath: #{@xpath_var}"
		nav = @page.xpath(@xpath_var)
	end

	nav = nav.children
	puts "Nav. count: #{nav.count}"

	# Get the category names and links for this page.
	if nav.count > 0
		nav.each do |n|
			if n.xpath('a/span[@class="refinementLink"]').to_s.strip != ""
				name = n.xpath('a/span[@class="refinementLink"]').text
				puts "Name: #{name}"
				# 3 lines to distill this is really ugly!
				puts "Nav link = " << n.xpath('a/span[@class="refinementLink"]/../@href').to_s[0,21]
				unless n.xpath('a/span[@class="refinementLink"]/../@href').to_s[0,21] == "http://www.amazon.com"
					url = "http://www.amazon.com" + n.xpath('a/span[@class="refinementLink"]/../@href').to_s
				#	puts "URL: #{url}"
					#asin = n.xpath('a/span[@class="refinementLink"]/../@href').to_s
					#asin = CGI::parse("#{asin}")
					#asin = asin['/s?rh'].to_s
				#	asin = url.split("n%3A")[4].split("&bbn")[0]
				else
					# Old URL structure: http://www.amazon.com/s?rh=n%3A157305011
					# New structure as of 15-May-2014: http://www.amazon.com/s/ref=lp_154606011_nr_n_23/186-8214925-0066221?rh=n%3A133140011%2Cn%3A%21133141011%2Cn%3A154606011%2Cn%3A668010011&bbn=154606011&ie=UTF8&qid=1400164622&rnid=154606011
					url = n.xpath('a/span[@class="refinementLink"]/../@href').to_s
				end
					puts "URL: #{url}"
					#puts "ASIN Split = #{url.split("/")[3]}"
					#asin = url.split("/")[3]
					#asin = CGI::parse("#{asin}")
					#asin = asin['rh'].to_s
					#asin = url.split("n%3A")[4].split("&bbn")[0]
					asin = url.split("&bbn")[0].split("Cn%3A").pop
					puts "ASIN: #{asin}"
				#end
				#puts "ASIN: #{asin[4,asin.length-6]}"
				count = n.xpath('a/span[@class="narrowValue"]').text.to_s
				count = count[2, count.length - 3].to_s.delete(",").to_i
				puts "Count: #{count}"

				if name.strip != ""
					item = Category.new(:parent_id => node.id)	
						item.name = "#{name}"
						item.url = "#{url}"
						#item.asin = "#{asin[4,asin.length-6]}"
						item.asin = "#{asin}"
						item.count = count
						item.category_group_id = node.category_group_id
					item.save
				end
			end
		end
	end

	sleep(rand(4..10).seconds)

	puts "==============================================================="
	puts ""
	crawl(node.id)
end

def init()
	cat_group = CategoryGroup.where(complete: true).last
	top = Category.find_by(:name => "Kindle eBooks")

	# If we don't have any categories, let's see the first tier.
	if !top
		cat_group = CategoryGroup.new
		cat_group.save

		category = cat_group.categories.new
			category.name = "Kindle eBooks"
			#category.url = "http://www.amazon.com/Best-Sellers-Kindle-Store-eBooks/zgbs/digital-text/154606011/ref=zg_bs_unv_kstore_2_154692011_2"
			category.url = "http://www.amazon.com/s/ref=lp_154607011_ex_n_1?rh=n%3A133140011%2Cn%3A!133141011%2Cn%3A154606011&bbn=154606011&ie=UTF8&qid=1392049593"
			category.asin = "154606011"
			category.count = 2482522
		category.save
		
		#get_categories(category)
	end
end

def run
	node = Category.last
	
	begin
		File.open("../log/status.log", "w") do |f|
			f.write("Datetime: #{Time.now}\n\n")
			f.write("Beginning to crawl Kindle categories...\n")
			f.write("====================================================================\n\n")
		end

		parse_categories(node.id)

		File.open(" ../log/status.log", "a") do |f|
			f.write("Datetime: #{Time.now}\n\n")
			f.write("Crawl completed.\n")
			f.write("====================================================================\n\n")
		end
	rescue Exception => e
		puts "Error:\n"
		puts "===========================\n"
		puts "#{e.to_s}"
		puts "\n\n\n"
		
		File.open("../log/status.log", "w") do |f|
			f.write("New error at #{Time.now}\n")
			f.write("======================================\n")
			f.write(e.to_string)
		end
	#	log_err(e)
	#	
	#	File.open("failed_page.html", "w") do |f|
	#		f.write("#{@page.to_s}")
	#	end
	#
	#	exec 'open Notification.app'
	end
end

def new
	cat_group = CategoryGroup.new
	cat_group.save

	category = cat_group.categories.new
		category.name = "Kindle eBooks"
		#category.url = "http://www.amazon.com/Best-Sellers-Kindle-Store-eBooks/zgbs/digital-text/154606011/ref=zg_bs_unv_kstore_2_154692011_2"
		category.url = "http://www.amazon.com/s/ref=lp_154607011_ex_n_1?rh=n%3A133140011%2Cn%3A!133141011%2Cn%3A154606011&bbn=154606011&ie=UTF8&qid=1392049593"
		category.asin = "154606011"
		category.count = 2482522
	category.save

	cat_group.group_root_id = category.id
	cat_group.save
end

# ARGV[0] = init/run/new
# ARGV[1] = Database environment to use.
case ARGV[0]
	when "init"
		init()
	when "run"
		run()
	when "new"
		new()
end
