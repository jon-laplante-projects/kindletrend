# TOP ID = 3790
require 'rubygems'
require 'rails'
require 'nokogiri'
require 'open-uri'
require 'active_record'
require 'ancestry'

#@id_ladder = [1,8,789,831]
@page = ""

if Rails.env == "production"
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		:host => "localhost",
		#:pool => 5,
		:username => "kindle_trend",
		:password => "THzmVLcvaNojnbi"
	)
else
	ActiveRecord::Base.establish_connection(
		:adapter => "mysql2",
		:encoding => "utf8",
		:database => "kindletrend_production",
		#:pool => 5,
		:username => "kindletrend",
		:password => "kindletrend",
	)
end

class Category < ActiveRecord::Base
	has_ancestry
end

tree = Category.where("id > 512").select("ancestry").distinct
tree_arr = Array.new

tree.each do |t|
  tree_arr << t.ancestry.to_s.split("/")  
end

# Make array of arrays into a single array.
tree_arr = tree_arr.flatten

# Remove duplicate values from array.
tree_arr = tree_arr.uniq

# Get recs that are not parents.
ends = Category.where("ID NOT IN (?)", tree_arr)


ends.each do |e|
	# Get the root page.
	@page = Nokogiri::HTML(open(e.url))

	@xpath_var = '//span[@class="zg_selected"]/../../ul'
			
	#puts "Nav. xpath: #{@xpath_var}"
	nav = @page.xpath(@xpath_var)

	# Get the categories navigation section of the page.
	if e.name == "Kindle eBooks"
		nav = @page.xpath('//ul[@id="zg_browseRoot"]/ul/ul')
	else
		@xpath_var = '//span[@class="zg_selected"]/../../ul'
		
		nav = @page.xpath(@xpath_var)
	end

	nav = nav.children
	puts "Nav. count: #{nav.count}"

	# Get the category names and links for this page.
	if nav.count > 0
		nav.each do |n|
			if e.name == "Kindle eBooks"
				categories = n.xpath("./li")

				categories.each do |c|
					puts "Category: #{c}"
					item = Category.new(:parent_id => e.id)
					item.name = "#{c.text.strip}"
					if e.name == "Kindle eBooks"
						item.url = "#{c.xpath('a/@href')}"
					else
						item.url = "#{c.xpath("./a").xpath("@href").text}"
					end
					item.save
				end
			else
				#puts "Not 'Kindle eBooks'"
				puts "Text: #{n.text.strip}"
				if n.text.strip != ""
					puts "Text is not null."
					puts "URL: #{n.xpath('a/@href').text}"
					item = Category.new(:parent_id => e.id)
						item.name = "#{n.text.strip}"
						item.url = "#{n.xpath('a/@href').text}"
						puts "New rec written..."
					item.save
				end
			end
		end
	end
end