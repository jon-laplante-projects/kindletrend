## Technologies used
* Ruby on Rails
* jQuery
* Bootstrap
* MySql

![Welcome to Kindle Trend!](https://brydgq.am.files.1drv.com/y4m2ZPgwjQpcgJOHQ0YAZFlk_hj9MvPEywI8wNDQLmB9IPX47aL9t6Yvozk3zSrLUZXoRkBL19dO6aQsaTSpVjLNncCvLToXsWXU99fNS1D74ZAV8ZoryjLfp-JtHghbiUosm0qzVtdVTT-bVYYE1kLr-CAUkCx3mXfIC2W_YHFlp9awcrJNxT6nh2njrWmJg7ggFnYK5WIq9YHIKZwW3WVEA?width=1024&height=464&cropmode=none)

Kindle Trend provides detailed analytics about top value niches in Amazon’s Kindle ebook store. Find low competition niches that open the door for easy bestseller status, drill down into exactly how profitable a niche is, and how difficult or easy to break into it might be, and view charts and graphs of many aspects to Kindle bestsellers that are rarely considered (like author gender or the day of the week books are published).

## Who is Kindle Trend for?
*  People who want to market their books in the Kindle ebook store
*  People who want to position themselves as experts in their field, influencers, or thought leaders
*  Professional writings who need to maximize profit potential for every book they write

## Why Kindle Trend?
*  Figuring out what to write is really hard.
*  The current tools still leave you with a lot of guess work.
*  Aggregating data is a long and tedious task.

### Tools
#### *Category Tree*
Visibility into the structure of categories (i.e. niches) in the Kindle store
![Kindle Trend Category Tree](https://bbyjgq.am.files.1drv.com/y4mZ6Ud0a1FPcmvr7KiRZlSIcuY__3mGe-zKdqykQEBTwLPRF9xnyLhRQo2uOnhU4W4Ehl5esLa3Bx89dizKxA6jG38opkH0HYlz1kaI9vjvLR7pMyyIDOWv-DtpsQ8I5Hsot9EwJqP9ctwtbQfdm-0Fu9bPtOpHMFssPSa1AHlXWLodAvEr9TKFboh5o7jcnmSRNNyzm8EC8QLfWIEOIYi0g?width=1024&height=594&cropmode=none)


#### *Easy Targets*
Find categories with less than the maximum number of books for the bestseller list, allowing you to break in incredibly easily
![Kindle Trend Easy Targets](https://bbyggq.am.files.1drv.com/y4mNhtxTZ1Jt0ydXz26zu0R0fYKJxpcXdOoihkB3dWwLBs0Gyd1VNGXY6A9ZGTUBP_bRJ3jLVW_4MCHmgffqDBvLAEICfG0au65hVWZLtVnlxxxRjhSLW6r-bQ7uVR5eWVnq6-bo4STQTTi80fDkifC3i_N_s9V49ZkPdmjb0C-H70DNfD7EJRHetCyaO9JOcrIOUGVgfrQ1peT1Yl_WcO_iQ?width=1024&height=590&cropmode=none)


#### *Category Summaries*
High-level data that lets you see demand, competition, and potential profitability at a glance
![Kindle Trend Category Summaries](https://bbyhgq.am.files.1drv.com/y4mx1PtNTxYHjF2fu4ZLDL0sPL9iIXWS4_iYbmTF51QcWliIwKDqE2xu0_xJELMXlX0CsqJt1QEMoOIRk_uQFxkN5MPO_LNcHS6xG5qXNH-NazpxSnPqXjBM3BIxZYzjdYq6KLMek4kVKI2X14mWk2pOEcPLTxJtPEGdq4B1ZfixxEe92dBxwn6_RJ41NyQu0Mj1BH4bJbuynMWEQ4OQyCf3g?width=1024&height=640&cropmode=none)


#### *Take Offs*
Details what it will take to break in as a bestseller in this category, and what you can expect when you do.
![Kindle Trend Take Offs](https://bryegq.am.files.1drv.com/y4mEHV1hqrQlBOQDMlRmvbZz7bXCOfP8qjmbXH8qblO5IDu6vckg9Va3hiRoHelmOgN0CsW6nWHipQCYXHuOoRz9Racfp7K6OwnbdZ18RBOtmyHpC7U-dlXtNUCrntwU240ZPDvP9mjusqE0146oyGDVO2QzsL26tLOdxoUYCgl1F2hIKuBVJf14i7QXcoqCSN37KmwFfRVv_qkEqv3P_Xfng?width=1024&height=590&cropmode=none)


#### *Analytics*
Detailed data about all possible nuances of a given category. Exactly the information needed to tailor your book into a bestseller.
![Kindle Trend Analytics](https://bbb9ga.am.files.1drv.com/y4mNzAjSdrbSHEQPubLoijZXc19qkDyEnv8RKgQVEDQYHcgpMBqDERZW1RkxefDP58EKDYNEzYL_aLck6IqLRnJc3Zh5Sqac8pV0urBpbRsog3iWWy9PD4i1x6QvSVOao7IMwLujzN0he2kXzQe3Y7N4cckAPkug9Ecq2z8vuOKPtQN-GOTNxBYCFHxFB3IESOX_Oo5rXHEe10mlKZyS7TFPw?width=1024&height=591&cropmode=none)