# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141020161636) do

  create_table "accounts", force: true do |t|
    t.integer  "user_id"
    t.string   "customer_name"
    t.string   "status"
    t.string   "payment_processor"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "token"
    t.string   "processor_id"
    t.string   "subscription_id"
    t.integer  "partner_id"
    t.boolean  "exempt"
    t.boolean  "admin"
  end

  create_table "authors", force: true do |t|
    t.string   "name"
    t.string   "gender"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ebook_id"
    t.string   "www"
    t.string   "facebook"
    t.string   "twitter"
    t.string   "authorcentral"
  end

  create_table "bestsellers", force: true do |t|
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "count"
    t.boolean  "complete",          default: false
    t.integer  "category_group_id"
    t.integer  "watch"
    t.string   "ancestry"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.text     "url"
    t.string   "ancestry"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "asin"
    t.integer  "count"
    t.integer  "watch"
    t.integer  "category_group_id"
  end

  add_index "categories", ["ancestry"], name: "index_categories_on_ancestry", using: :btree

  create_table "category_automations", force: true do |t|
    t.integer "prev_top_id"
    t.integer "last_top_id"
    t.integer "prev_id"
    t.boolean "prev_top_complete"
    t.integer "parent_watch"
    t.integer "curr_watch"
    t.integer "watch_iteration"
  end

  create_table "category_forms", force: true do |t|
    t.integer  "form_id"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "category_groups", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "complete"
    t.integer  "group_root_id", limit: 8
  end

  create_table "category_rollups", id: false, force: true do |t|
    t.integer "id",       default: 0, null: false
    t.string  "name"
    t.string  "ancestry"
  end

  create_table "customers", id: false, force: true do |t|
    t.integer "id",                limit: 8, default: 0,  null: false
    t.string  "customer_name"
    t.string  "email",                       default: "", null: false
    t.string  "status"
    t.string  "payment_processor"
    t.integer "sign_in_count",               default: 0,  null: false
    t.string  "partner_name"
    t.string  "partner_type"
    t.boolean "exempt"
    t.boolean "admin"
  end

  create_table "ebooks", force: true do |t|
    t.string   "name"
    t.decimal  "rating",           precision: 10, scale: 2
    t.integer  "reviews"
    t.decimal  "price",            precision: 10, scale: 2
    t.integer  "pages"
    t.decimal  "rank",             precision: 10, scale: 2
    t.text     "desc"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "bestseller_id"
    t.string   "url"
    t.date     "publication_date"
    t.string   "publisher"
    t.string   "filesize"
    t.boolean  "assigned"
  end

  create_table "field_values", force: true do |t|
    t.integer  "field_id"
    t.string   "preset_field_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fields", force: true do |t|
    t.string   "name"
    t.integer  "form_id"
    t.integer  "friendly_name_id"
    t.string   "field_type"
    t.boolean  "require_field"
    t.boolean  "ignore_field"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "forms", force: true do |t|
    t.string   "name"
    t.text     "form_url"
    t.text     "submission_url"
    t.string   "submission_type"
    t.boolean  "captcha"
    t.boolean  "free_ebooks"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "friendly_names", force: true do |t|
    t.string   "name"
    t.string   "field_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "field_name"
  end

  create_table "friendly_radios", force: true do |t|
    t.integer "friendly_name_id"
    t.string  "label"
    t.string  "field_name"
  end

  create_table "friendly_vals", force: true do |t|
    t.integer "friendly_name_id"
    t.string  "field_value"
  end

  create_table "partner_types", force: true do |t|
    t.string  "name"
    t.text    "description"
    t.decimal "monthly_price", precision: 10, scale: 0
  end

  create_table "partners", force: true do |t|
    t.string   "name"
    t.string   "name_as_processor"
    t.string   "token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "partner_type_id"
    t.string   "subdomain"
    t.string   "product_name"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "support_email"
    t.string   "app_style"
    t.boolean  "kt_supported"
  end

  create_table "ranks", force: true do |t|
    t.integer  "category_id"
    t.decimal  "rank",        precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ebook_id"
  end

  create_table "rollups", force: true do |t|
    t.integer  "bestseller_id"
    t.integer  "category_child_count"
    t.integer  "category_book_count"
    t.integer  "estimated_mean_sales"
    t.decimal  "estimated_mean_income",    precision: 10, scale: 2
    t.integer  "estimated_ceiling_sales"
    t.decimal  "estimated_ceiling_income", precision: 10, scale: 2
    t.integer  "estimated_floor_sales"
    t.decimal  "estimated_floor_income",   precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "special_offers", force: true do |t|
    t.string   "name"
    t.string   "title"
    t.text     "desc"
    t.string   "url"
    t.string   "img"
    t.string   "button_text"
    t.boolean  "active"
    t.datetime "starts_at"
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "submission_logs", force: true do |t|
    t.integer  "submitted_book_id"
    t.integer  "form_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "submitted_book_data", force: true do |t|
    t.integer  "submitted_book_id"
    t.integer  "friendly_field_id"
    t.text     "field_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "submitted_books", force: true do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "role"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
