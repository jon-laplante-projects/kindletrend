category_group = CategoryGroup.where("complete IS NOT true").last

prev_top_id = Category.where(category_group_id: category_group.id).second
last_top_id = Category.where(ancestry: "#{category_group.group_root_id}").last
prev_id = Category.last

CategoryAutomation.create(
	prev_top_id: prev_top_id.id, 
	last_top_id: last_top_id.id, 
	prev_id: prev_id.id,
	prev_top_complete: false)