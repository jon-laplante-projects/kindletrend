class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.integer :user_id
      t.string :customer_name
      t.string :status
      t.string :payment_processor

      t.timestamps
    end
  end
end
