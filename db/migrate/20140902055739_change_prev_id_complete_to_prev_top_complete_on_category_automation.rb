class ChangePrevIdCompleteToPrevTopCompleteOnCategoryAutomation < ActiveRecord::Migration
  def change
  	rename_column :category_automations, :prev_id_complete, :prev_top_complete
  end
end
