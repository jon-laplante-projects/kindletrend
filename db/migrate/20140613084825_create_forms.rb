class CreateForms < ActiveRecord::Migration
  def change
    create_table :forms do |t|
      t.string :name
      t.text :form_url
      t.text :submission_url
      t.string :submission_type
      t.boolean :captcha
      t.boolean :free_ebooks

      t.timestamps
    end
  end
end
