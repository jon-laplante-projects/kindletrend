class CreateSubmittedBooks < ActiveRecord::Migration
  def change
    create_table :submitted_books do |t|
      t.integer :user_id

      t.timestamps
    end
  end
end
