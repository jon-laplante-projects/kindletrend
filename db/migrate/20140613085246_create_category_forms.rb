class CreateCategoryForms < ActiveRecord::Migration
  def change
    create_table :category_forms do |t|
      t.integer :form_id
      t.integer :category_id

      t.timestamps
    end
  end
end
