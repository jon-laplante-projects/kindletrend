class AddSubdomainToPartner < ActiveRecord::Migration
  def change
    add_column :partners, :subdomain, :string
  end
end
