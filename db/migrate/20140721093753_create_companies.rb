class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :sys_name
      t.integer :company_type_id
      t.integer :company_contact_id

      t.timestamps
    end
  end
end
