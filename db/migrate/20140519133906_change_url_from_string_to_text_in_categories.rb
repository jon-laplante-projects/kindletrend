class ChangeUrlFromStringToTextInCategories < ActiveRecord::Migration
  def change
  	change_column :categories, :url, :text
  end
end
