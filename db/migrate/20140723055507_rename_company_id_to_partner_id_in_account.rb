class RenameCompanyIdToPartnerIdInAccount < ActiveRecord::Migration
  def change
  	rename_column :accounts, :company_id, :partner_id
  end
end
