class AddExemptAndAdminToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :exempt, :boolean
    add_column :accounts, :admin, :boolean
  end
end
