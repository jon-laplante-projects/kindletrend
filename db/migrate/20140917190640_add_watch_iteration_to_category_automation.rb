class AddWatchIterationToCategoryAutomation < ActiveRecord::Migration
  def change
    add_column :category_automations, :watch_iteration, :integer
  end
end
