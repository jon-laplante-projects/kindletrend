class CreatePartnerType < ActiveRecord::Migration
  def change
    create_table :partner_types do |t|
      t.string :name
      t.text :description
      t.decimal :monthly_price
    end
  end
end
