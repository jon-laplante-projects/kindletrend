class AddCompleteToCategoryGroups < ActiveRecord::Migration
  def change
    add_column :category_groups, :complete, :boolean
  end
end
