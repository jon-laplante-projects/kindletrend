class CreateFields < ActiveRecord::Migration
  def change
    create_table :fields do |t|
      t.string :name
      t.integer :form_id
      t.integer :friendly_name_id
      t.string :field_type
      t.boolean :require_field
      t.boolean :ignore_field

      t.timestamps
    end
  end
end
