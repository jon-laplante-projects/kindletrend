class CreateSubmittedBookData < ActiveRecord::Migration
  def change
    create_table :submitted_book_data do |t|
      t.integer :submitted_book_id
      t.integer :friendly_field_id
      t.text :field_value

      t.timestamps
    end
  end
end
