class CreateFriendlyRadio < ActiveRecord::Migration
  def change
    create_table :friendly_radios do |t|
      t.integer :friendly_name_id
      t.string :label
      t.string :field_name
    end
  end
end
