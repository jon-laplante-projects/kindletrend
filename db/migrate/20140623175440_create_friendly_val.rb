class CreateFriendlyVal < ActiveRecord::Migration
  def change
    create_table :friendly_vals do |t|
      t.integer :friendly_name_id
      t.string :field_value
    end
  end
end
