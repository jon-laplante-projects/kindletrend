class ChangeProcessorIdTypeInAccount < ActiveRecord::Migration
  def change
    change_column :accounts, :processor_id, :string
  end
end
