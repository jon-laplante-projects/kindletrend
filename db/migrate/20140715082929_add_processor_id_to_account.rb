class AddProcessorIdToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :processor_id, :integer
  end
end
