class AddProductNameToPartner < ActiveRecord::Migration
  def change
    add_column :partners, :product_name, :string
  end
end
