class CreateCategoryAutomation < ActiveRecord::Migration
  def change
    create_table :category_automations do |t|
      t.integer :prev_top_id
      t.integer :last_top_id
      t.integer :prev_id
    end
  end
end
