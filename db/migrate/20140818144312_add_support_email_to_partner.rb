class AddSupportEmailToPartner < ActiveRecord::Migration
  def change
    add_column :partners, :support_email, :string
  end
end
