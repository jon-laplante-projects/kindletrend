class AddAppStyleToPartner < ActiveRecord::Migration
  def change
    add_column :partners, :app_style, :string
  end
end
