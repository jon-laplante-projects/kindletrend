class AddPrevIdCompleteToCategoryAutomation < ActiveRecord::Migration
  def change
    add_column :category_automations, :prev_id_complete, :boolean
  end
end
