class AddKtSupportedToPartner < ActiveRecord::Migration
  def change
    add_column :partners, :kt_supported, :boolean
  end
end
