class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :name
      t.string :name_as_processor
      t.string :token

      t.timestamps
    end
  end
end
