class AddFieldNameToFriendlyName < ActiveRecord::Migration
  def change
    add_column :friendly_names, :field_name, :string
  end
end
