class CreateFriendlyNames < ActiveRecord::Migration
  def change
    create_table :friendly_names do |t|
      t.string :name
      t.string :field_type

      t.timestamps
    end
  end
end
