class CreateFieldValues < ActiveRecord::Migration
  def change
    create_table :field_values do |t|
      t.integer :field_id
      t.string :preset_field_value

      t.timestamps
    end
  end
end
