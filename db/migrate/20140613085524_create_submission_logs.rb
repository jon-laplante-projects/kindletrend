class CreateSubmissionLogs < ActiveRecord::Migration
  def change
    create_table :submission_logs do |t|
      t.integer :submitted_book_id
      t.integer :form_id

      t.timestamps
    end
  end
end
