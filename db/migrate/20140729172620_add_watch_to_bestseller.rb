class AddWatchToBestseller < ActiveRecord::Migration
  def change
    add_column :bestsellers, :watch, :integer
  end
end
