class AddParentWatchAndCurrWatchToCategoryAutomation < ActiveRecord::Migration
  def change
    add_column :category_automations, :parent_watch, :integer
    add_column :category_automations, :curr_watch, :integer
  end
end
