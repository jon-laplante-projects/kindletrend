class AddAssignedToEbook < ActiveRecord::Migration
  def change
    add_column :ebooks, :assigned, :boolean
  end
end
