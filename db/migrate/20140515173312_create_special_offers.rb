class CreateSpecialOffers < ActiveRecord::Migration
  def change
    create_table :special_offers do |t|
      t.string :name
      t.string :title
      t.text :desc
      t.string :url
      t.string :img
      t.string :button_text
      t.boolean :active
      t.datetime :starts_at
      t.datetime :expires_at

      t.timestamps
    end
  end
end
