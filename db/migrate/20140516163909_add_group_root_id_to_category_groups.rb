class AddGroupRootIdToCategoryGroups < ActiveRecord::Migration
  def change
    add_column :category_groups, :group_root_id, :bigint
  end
end
