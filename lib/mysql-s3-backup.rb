require 'date'
require 'aws-sdk'

class Settings
	attr_accessor :username, :password, :database, :mysql_location, :access_key, :secret_key, :bucket_name
end

settings = Settings.new
#settings.username = "kindletrend"
settings.username = "kindle_trend"
#settings.password = "kindletrend"
settings.password = "THzmVLcvaNojnbi"
settings.database = "kindletrend_production"
#settings.mysql_location = "/Applications/XAMPP/xamppfiles/bin/mysqldump"
settings.mysql_location = "/usr/bin/mysqldump"

settings.access_key = "AKIAIKBASKBG2EIZ3BRA"
settings.secret_key = "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"
settings.bucket_name = "kindle-trend-db-backups"

def mysql_to_s3(settings)

	puts "Starting mysql_to_s3 for username: \"" + settings.username + "\"..."

	dumpfile = DateTime.now.to_s + ".sql"

	command_string = settings.mysql_location + " -u " + settings.username + " -p" + settings.password + " --databases " + settings.database + " > " + dumpfile
	status = system command_string
	if status
		puts "Database backed up."
	else
		puts "Error in backing up database."
		return
	end

	# US Standard *                           s3.amazonaws.com(default)
	# US West (Oregon) Region                 s3-us-west-2.amazonaws.com
	# US West (Northern California) Region    s3-us-west-1.amazonaws.com
	# EU (Ireland) Region                     s3-eu-west-1.amazonaws.com
	# Asia Pacific (Singapore) Region         s3-ap-southeast-1.amazonaws.com
	# Asia Pacific (Tokyo) Region             s3-ap-northeast-1.amazonaws.com
	# South America (Sao Paulo) Region        s3-sa-east-1.amazonaws.com
	s3 = AWS::S3.new(
  		:access_key_id     => "#{settings.access_key}",
  		:secret_access_key => "#{settings.secret_key}",
  		:s3_host_name => 's3.amazonaws.com'
  	)

	begin
		puts "Looking for bucket..."
		bucket = s3.buckets[settings.bucket_name]
		s3Obj = bucket.objects[DateTime.now.to_s + '.sql']
		puts "Bucket found."
	rescue Exception => e
		puts e.message
		puts "Error: Bucket " + settings.bucket_name + " might not be present in your Amazon S3 account."
		puts "Deleting backup file from local disk..."
		File.delete(dumpfile)
		return
	end

	begin
		puts "Sending data to Amazon S3..."
		s3Obj.write(Pathname.new(dumpfile))
	rescue Exception => e
		puts "Error: " + e.message
	end

	puts "Deleting backup file from local disk..."
	File.delete(dumpfile)
	return

	puts "Backup Done."
	puts ""

end

mysql_to_s3(settings)