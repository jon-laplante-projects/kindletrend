namespace :automation do
  desc "This is a test for cron."
  task test_me: :environment do
    ActiveRecord::Base.logger.info "Rake fired by cron."
  end

  desc "Pulls the next batch of ebooks for watched categories (including generating bestseller recs)."
  task bestsellers: :environment do
    automation_data = CategoryAutomation.first

    cat_group = CategoryGroup.where(complete: true).last
    cat_id = Bestseller.select("category_id").where(category_group_id: cat_group.id)
    prior_watch = automation_data.curr_watch || 0
    
    for x in 1..4
      # Get the next watch.
      next_watch_id = Category.where("watch > #{prior_watch}").first
      next_watch = Category.where(watch: next_watch_id).count

      if next_watch > 0
        watch = next_watch_id
      else
        watch = 1
      end

      automation_data.curr_watch = watch
      #automation_data.watch_iteration = x
      automation_data.save

      #system "bundle exec ruby /webapps/app.git/raw/build_bestsellers.rb 'run' 'production' #{watch}"
      # Get the current category...
      watches = cat_group.categories.where(watch: watch)

      watches.each do |w|
        bestseller = Bestseller.new
        bestseller.category_id = w.id
        bestseller.count = @bestseller_count
        bestseller.category_group_id = cat_group.id
        bestseller.watch = w.watch
        bestseller.save
      end
    end
  end

  desc "Generate the rollups so users can see category-level data."
  task generate_rollups: :environment do
    #system "bundle exec ruby /web/app.git/raw/generate_rollups.rb 'production'"
    cat_group = CategoryGroup.where(complete: true).last
    bestseller = Bestseller.where("category_group_id = #{cat_group.id} AND complete = TRUE AND id NOT IN (SELECT bestseller_id FROM rollups)")
    #bestseller = Bestseller.where(category_group_id: cat_group.id, complete: true)
    if bestseller.count > 0
      bestseller.each do |b|
        # Ensure there are in fact ebooks for this bestseller.
        ebook_count = Ebook.where(bestseller_id: b.id).count
        # End ebook check.

        if ebook_count > 0
          category = Category.find(b.category_id)
          generate(b.id)
        else
          b.complete = false
          b.save
        end
      end
    end
  end

  desc "Spawn remote servers to pull Kindle store data and return it as JSON."
  task categories: :environment do
    @automation_data = CategoryAutomation.last
    @watch = Category.where(watch: !nil).last
    watch_count = Category.where(watch: @watch.watch, category_group_id: @watch.category_group_id).count
    # increment @watch.watch
    @watch.watch = @watch.watch + 1

    if (@automation_data.prev_top_id == @automation_data.last_top_id) && @automation_data.prev_top_complete == true
      @automation_data.prev_top_complete = false
      @automation_data.save

      # Mark the previously finished category as complete.
      cat_group = CategoryGroup.last
      cat_group.complete = true
      cat_group.save

      init()
    elsif @automation_data.prev_top_complete != true
      cat = Category.find(@automation_data.prev_id)
      @category_id = cat.ancestry.split("/")[0]

      AutomationMailer.category_crawl_fail(@category_id).deliver

      run()
    else
      curr_top_id = Category.where("id > #{@automation_data.prev_top_id}").first
      @category_id = curr_top_id.id

      @automation_data.prev_top_id = curr_top_id.id
      @automation_data.prev_top_complete = false
      @automation_data.save

      new()
    end
  end

  desc "Clean up the pull process for parent-level categories."
  task parent_bestsellers_cleanup: :environment do
    barge = Barge::Client.new(access_token: 'bff673330e72bffb70e6199f8d3869b76a2898f42ad4b039f26e8158bd5d8dab')
    barge.droplet.power_off(1714390)
    barge.droplet.power_off(1714394)
    barge.droplet.power_off(1714411)
    barge.droplet.power_off(1714417)
  end

  desc "Create parent-level bestsellers based on watches."
  task parent_bestsellers: :environment do
    automation_data = CategoryAutomation.first

    cat_group = CategoryGroup.where(complete: true).last
    cat_id = Bestseller.select("category_id").where(category_group_id: cat_group.id)
    prior_watch = automation_data.parent_watch
    
    for x in 1..4
      # Get the next watch.
      next_watch_id = Category.where("watch > #{prior_watch}").first
      next_watch = Category.where(watch: next_watch_id).count

      if next_watch > 99
        watch = next_watch_id
      else
        watch = 100
      end

      automation_data.parent_watch = watch
      #automation_data.watch_iteration = x
      automation_data.save

      #system "bundle exec ruby /webapps/app.git/raw/build_bestsellers.rb 'run' 'production' #{watch}"
      # Get the current category...
      watches = Category.where("category_group_id = #{cat_group.id} AND watch = #{watch}")

      watches.each do |w|
        bestseller = Bestseller.new
        bestseller.category_id = w.id
        bestseller.count = w.count
        bestseller.category_group_id = cat_group.id
        bestseller.watch = w.watch
        bestseller.save
      end
    end
  end

  desc "Run the parent-category ebook pulls."
  task parent_categories: :environment do
    barge = Barge::Client.new(access_token: 'bff673330e72bffb70e6199f8d3869b76a2898f42ad4b039f26e8158bd5d8dab')
    barge.droplet.power_on(1714390)
    barge.droplet.power_on(1714394)
    barge.droplet.power_on(1714411)
    barge.droplet.power_on(1714417)
  end

  desc "Run the non-parent category ebook pulls."
  task subcategories: :environment do
    barge = Barge::Client.new(access_token: 'bff673330e72bffb70e6199f8d3869b76a2898f42ad4b039f26e8158bd5d8dab')
    barge.droplet.power_on(1714390)
    barge.droplet.power_on(1714394)
    barge.droplet.power_on(1714411)
    barge.droplet.power_on(1714417)
  end

  desc "Resets ebooks that have been assigned 12hrs ago but not parsed."
  task reset_aged_assigned_ebooks: :environment do
    Ebook.update_all("assigned = false", "name IS NULL AND updated_at < DATE_SUB(NOW(), INTERVAL 12 HOUR) AND assigned IS true")

  end

  #################################################
  ## Category methods                            ##
  #################################################
  def init
    Category.init
  end

  def crawl(id)
    node = Category.find(id)

    if node.has_children?
      node = node.children.first
    else
      node = Category.find(siblings(node.id))
    end
    
    #get_categories(node.id)
    parse_categories(node.id)
  end
  # End crawl.

  def siblings(id)
    unless id == nil
      node = Category.find(id)
      # group_root_id replaced with the parent category id.
      
      if node.id == @category_id.to_i
        cat_group = CategoryGroup.find(node.category_group_id)
        cat_group.complete = true
        cat_group.save

        @automation_data.prev_top_complete = true
        #@automation_data.prev_top_id = id
        @automation_data.save
        abort "Congratulations! You have a map of all Kindle categories!"

      elsif node.has_siblings?
        sibs = node.sibling_ids.sort
        if sibs[sibs.index(node.id).to_i] < sibs.last
          sibs[sibs.index(node.id).to_i + 1]
        else
          unless node.parent_id == @category_id.to_i
            siblings(node.parent_id)
          else
            @automation_data.prev_top_complete = true
            #@automation_data.prev_top_id = id
            @automation_data.save
            abort "Congratulations! You have a map of all Kindle categories!"
          end
        end
      else
        unless node.parent_id == @category_id.to_i
          siblings(node.parent_id)
        else
          @automation_data.prev_top_complete = true
          @automation_data.prev_top_id = id
          @automation_data.save
          abort "Congratulations! You have a map of all Kindle categories!"
        end
      end
    else
      puts "No category_id. All titles mapped?"
    end
  end

  def parse_categories(id)
    parse_single_category(id)
    node = Category.find(id)
    
    sleep(rand(20..30).seconds)

    puts "==============================================================="
    puts ""
    crawl(node.id)
  end

#  def spawn_category_servers
#    # After spawing the servers, create an array of enabled and disabled servers.
#    # Should these be globals, saved in the DB, or passed back and forth?
#    @enabled_servers = Array.new
#    @disabled_servers = Array.new
#
#    @enabled_servers << "http://localhost:9292/categories.json"
#  end
#
#  def get_categories
#    automation_data = CategoryAutomation.last
#    results = Hash.new
#
#    cat = Category.select(:id, :url).where("id > ?", automation_data.prev_id).last
#    # Use the array of enabled servers. When one needs a timeout (sleep), move it to the disabled list.
#    # Should each pull be in its own thread (along with the sleep)?
#    # POST including id, url, and status.
#
#    post_page = agent.post("#{server}", {
#                    "id" => "#{cat.id}", 
#                    "url" => "#{cat.url}", 
#                    "status" => "active"
#                  })
#  end

  def parse_single_category(id)
    node = Category.find(id)
    puts "Current node: #{node.id}"

    # Get the root page.
    rand_browser = 1 + rand(7)
    case(rand_browser)
      when 1 then agent_alias = 'Mac Firefox'
      when 2 then agent_alias = 'Mac Mozilla'
      when 3 then agent_alias = 'Windows Chrome'
      when 4 then agent_alias = 'Windows IE 8'
      when 5 then agent_alias = 'Windows IE 9'
      when 6 then agent_alias = 'Windows Mozilla'
      when 7 then agent_alias = 'Linux Firefox'
    end

    user_agent = Mechanize.new {|agent| agent.user_agent_alias = "#{agent_alias}"}

    user_agent.get(node.url) { |d| @page = d.content }

    @page = Nokogiri::HTML(@page)

    ## Get the categories navigation section of the page.
    #if node.name == "Kindle eBooks"
    # #nav = @page.xpath('//ul[@id="ref_154606011"]')
      nav = @page.xpath('//ul[@data-typeid="n"]')
    #else
    # @xpath_var = "//ul[@id='ref_#{node.asin}']"
    # @xpath_var = "//ul[@id='ref_#{node.asin}']/li/strong"
    # 
    # puts "Nav. xpath: #{@xpath_var}"
    # nav = @page.xpath(@xpath_var).children
    #end

    #nav = nav.children
    puts "Nav. count: #{@page.xpath(@xpath_var).children.count}"

    # Get the category names and links for this page.
    if nav.children.count > 0
      puts "Node has children..."
      nav.children.each do |n|
        if n.xpath('a/span[@class="refinementLink"]').to_s.strip != ""
          name = n.xpath('a/span[@class="refinementLink"]').text
          puts "Name: #{name}"
          # 3 lines to distill this is really ugly!
          puts "Nav link = " << n.xpath('a/span[@class="refinementLink"]/../@href').to_s[0,21]
          unless n.xpath('a/span[@class="refinementLink"]/../@href').to_s[0,21] == "http://www.amazon.com"
            url = "http://www.amazon.com" + n.xpath('a/span[@class="refinementLink"]/../@href').to_s
          else
            # Old URL structure: http://www.amazon.com/s?rh=n%3A157305011
            # New structure as of 15-May-2014: http://www.amazon.com/s/ref=lp_154606011_nr_n_23/186-8214925-0066221?rh=n%3A133140011%2Cn%3A%21133141011%2Cn%3A154606011%2Cn%3A668010011&bbn=154606011&ie=UTF8&qid=1400164622&rnid=154606011
            url = n.xpath('a/span[@class="refinementLink"]/../@href').to_s
          end
          
          puts "URL: #{url}"
          asin = url.split("&bbn")[0].split("Cn%3A").pop
          count = n.xpath('a/span[@class="narrowValue"]').text.to_s
          count = count[2, count.length - 3].to_s.delete(",").to_i
          puts "Count: #{count}"

          if name.strip != ""
            item = Category.new(:parent_id => node.id)  
              item.name = "#{name}"
              item.url = "#{url}"
              item.asin = "#{asin}"
              item.count = count
              if count > 40000
                if @automation_data.watch_iteration == 5
                  @automation_data.curr_watch += 1
                  @automation_data.watch_iteration = 0
                end

                item.watch = @automation_data.curr_watch

                @automation_data.watch_iteration += 1
                @automation_data.save
              end
              item.category_group_id = node.category_group_id
            item.save

            # Set watches for the parent categories.
            unless item.ancestry.include? "/"
              #######################################
              # if root, ignore.
              # reset to 100 for first node.
              #######################################
              if item.name == "Kindle eBooks"
                @automation_data.parent_watch = 99
                @automation_data.save
              else
                item.watch = @automation_data.parent_watch.to_i + 1
                @automation_data.parent_watch += 1
                @automation_data.save
              end
            end

            @automation_data.prev_id = item.id
            @automation_data.save
          end
        end
      end
    end
  end

  def new
    parse_categories(@category_id)
  end

  def run    
    parse_categories(@automation_data.prev_id)
  end
  #################################################
  ## End Category methods                        ##
  #################################################


  #################################################
  ## Rollup methods                              ##
  #################################################
  def generate(bestseller_id)
    unless bestseller_id.nil?
      bestseller = Bestseller.find(bestseller_id)
      category = Category.find(bestseller.category_id)
      est = estimates(bestseller_id)
      rollup = Rollup.new
      rollup.bestseller_id = bestseller_id
      rollup.category_child_count = category.children.count
      rollup.category_book_count = category.count
      rollup.estimated_mean_sales = est["estimated_mean_sales"]
      rollup.estimated_mean_income = est["estimated_mean_earnings"]
      rollup.estimated_ceiling_sales = est["estimated_ceiling_sales"]
      rollup.estimated_ceiling_income = est["estimated_ceiling_earnings"]
      rollup.estimated_floor_sales = est["estimated_floor_sales"]
      rollup.estimated_floor_income = est["estimated_floor_earnings"]
      rollup.save
    end
  end

  def estimates(bestseller_id)
    unless bestseller_id.nil?
      bestseller = Bestseller.find(bestseller_id)
      cat_root = Category.where(category_group_id: bestseller.category_group_id).first

      estimates_hash = Hash.new

      # Ranks and price arrays.
      ranks_arr = Array.new
      price_arr = Array.new
      reviews_arr = Array.new
      pages_arr = Array.new
      filesizes_arr = Array.new

      bestseller.ebooks.each do |e|
        e.ranks.where(category_id: cat_root.id).each do |r|
          ranks_arr << r.rank
        end
      end

      bestseller.ebooks.each do |p|
        price_arr << p.price.blank? ? "2.99".to_f : p.price
        reviews_arr << p.reviews if p.reviews
        pages_arr << p.pages if p.pages
        filesizes_arr << p.filesize[0, p.filesize.length - 3].to_i if p.filesize
      end

      # Calculate means.
      mean_rank = (ranks_arr.inject{|sum,x| sum + x }/ranks_arr.size).to_i unless ranks_arr.empty?
      mean_price = (price_arr.inject{|sum,x| sum + x }/price_arr.size).to_f unless price_arr.empty?
      mean_reviews = (reviews_arr.inject{|sum,x| sum + x }/reviews_arr.size).to_i unless reviews_arr.empty?
      mean_pages = (pages_arr.inject{|sum,x| sum + x }/pages_arr.size).to_i unless pages_arr.empty?
      mean_filesize = (filesizes_arr.inject{|sum,x| sum + x }/filesizes_arr.size).to_i unless filesizes_arr.empty?

      # Calculate floors.
      # Floor rank is actually the HIGHTEST rank.
      floor_rank = ranks_arr.sort.last.to_i
      floor_price = price_arr.sort.first.to_f
      floor_reviews = reviews_arr.sort.first.to_i
      floor_pages = pages_arr.sort.first.to_i
      floor_filesize = filesizes_arr.sort.first.to_i

      # Calculate ceilings.
      # Ceiling rank is actually the LOWEST rank.
      ceiling_rank = ranks_arr.sort.first.to_i
      ceiling_price = price_arr.sort.last.to_f
      ceiling_reviews = reviews_arr.sort.last.to_i
      ceiling_pages = pages_arr.sort.last.to_i
      ceiling_filesize = filesizes_arr.sort.last.to_i

      mean_est_earnings = earningsEstimator(mean_rank, mean_price, mean_filesize, "mean")
      floor_est_earnings = earningsEstimator(floor_rank, floor_price, floor_filesize, "floor")
      ceiling_est_earnings = earningsEstimator(ceiling_rank, ceiling_price, ceiling_filesize, "ceiling")

      estimates_hash["estimated_mean_earnings"] = mean_est_earnings
      estimates_hash["estimated_ceiling_earnings"] = ceiling_est_earnings
      estimates_hash["estimated_floor_earnings"] = floor_est_earnings

      mean_est_sales = salesEstimator(mean_rank, "mean")
      floor_est_sales = salesEstimator(floor_rank, "floor")
      ceiling_est_sales = salesEstimator(ceiling_rank, "ceiling")

      estimates_hash["estimated_mean_sales"] = mean_est_sales
      estimates_hash["estimated_ceiling_sales"] = ceiling_est_sales
      estimates_hash["estimated_floor_sales"] = floor_est_sales

      return estimates_hash
    end
  end

  def earningsEstimator(rank, price=2.99, filesize=1, operation)
    daily_sales = salesEstimator(rank, operation)

    # Client.average("orders_count")
    # Client.minimum("age")
    # Client.maximum("age")

    # Based on price, determine commissions.
    # If royalty is 70%, delivery rate for Amazon.com US is $0.15/MB (no charge for 35% royalty)
    #
    # USD List Price Requirements for Amazon.com          Minimum     Maximum 
    # 35 % Royalty Option                     List Price    List Price
      # • Less than or equal to 3 megabytes             $ 0.99      $ 200.00
      # • Greater than 3 megabytes and less than 10 megabytes   $ 1.99      $ 200.00
      # • 10 megabytes or greater                 $ 2.99      $ 200.00
    # 70 % Royalty Option                     $ 2.99      $ 9.99

    if price > 2.99 && price < 9.99
      royalty = 0.7
    else
      # Royalty will be 35%
      royalty = 0.35
    end

    delivery = 0
    delivery = 0.15 * filesize/1024 if royalty == 0.7

    return ((daily_sales * 30) * (royalty * price)) - (delivery * daily_sales)
  end

  def salesEstimator(rank, operation)
    # Estimate the number of sales.
    # From http://www.theresaragan.com/p/sale-ranking-chart.html:::
    #
    # Amazon Best Seller Rank 50,000 to 100,000 - selling close to 1 book a day.
    # Amazon Best Seller Rank 10,000 to 50,000 - selling 5 to 15 books a day.
    # Amazon Best Seller Rank 5,500 to 10,000 - selling 15 to 25 books a day.
    # Amazon Best Seller Rank 3,000 to 5,500 - selling 25 to 70 books a day.
    # Amazon Best Seller Rank 1,500 to 3,000 - selling 70 to 100 books a day.
    # Amazon Best Seller Rank 750 to 1,500 - selling 100 to 120 books a day.
    # Amazon Best Seller Rank 500 to 750 - selling 120 to 175 books a day.
    # Amazon Best Seller Rank 350 to 500 - selling 175 to 250 books a day.
    # Amazon Best Seller Rank 200 to 350 - selling 250 to 500 books a day.
    # Amazon Best Seller Rank 35 to 200 - selling 500 to 2,000 books a day.
    # Amazon Best Seller Rank 20 to 35 - selling 2,000 to 3,000 books a day.
    # Amazon Best Seller Rank of 5 to 20 - selling 3,000 to 4,000 books a day.
    # Amazon Best Seller Rank of 1 to 5 - selling 4,000+ books a day.
    unless rank.nil?
      if rank <= 200000
        case rank
        when 100000...200000
          daily_sales = 0.034
        when 50000...100000
          daily_sales = 1
        when 10000...50000
          case operation
            when "mean"
              daily_sales = 12
            when "median"
              daily_sales = 10
            when "ceiling"
              daily_sales = 15
            when "floor"
              daily_sales = 5
          end
        when 5500...10000
          case operation
            when "mean"
              daily_sales = 22
            when "median"
              daily_sales = 20
            when "ceiling"
              daily_sales = 25
            when "floor"
              daily_sales = 15
          end
        when 3000...5500
          case operation
            when "mean"
              daily_sales = 62
            when "median"
              daily_sales = 57
            when "ceiling"
              daily_sales = 70
            when "floor"
              daily_sales = 25
          end
        when 1500...3000
          case operation
            when "mean"
              daily_sales = 88
            when "median"
              daily_sales = 85
            when "ceiling"
              daily_sales = 100
            when "floor"
              daily_sales = 70
          end
        when 750...1500
          case operation
            when "mean"
              daily_sales = 113
            when "median"
              daily_sales = 110
            when "ceiling"
              daily_sales = 120
            when "floor"
              daily_sales = 100
          end
        when 500...750
          case operation
            when "mean"
              daily_sales = 150
            when "median"
              daily_sales = 145
            when "ceiling"
              daily_sales = 175
            when "floor"
              daily_sales = 120
          end
        when 350...500
          case operation
            when "mean"
              daily_sales = 210
            when "median"
              daily_sales = 200
            when "ceiling"
              daily_sales = 250
            when "floor"
              daily_sales = 175
          end
        when 200...350
          case operation
            when "mean"
              daily_sales = 400
            when "median"
              daily_sales = 375
            when "ceiling"
              daily_sales = 500
            when "floor"
              daily_sales = 250
          end
        when 35...200
          case operation
            when "mean"
              daily_sales = 1300
            when "median"
              daily_sales = 1250
            when "ceiling"
              daily_sales = 2000
            when "floor"
              daily_sales = 500
          end
        when 20...35
          case operation
            when "mean"
              daily_sales = 2600
            when "median"
              daily_sales = 2500
            when "ceiling"
              daily_sales = 3000
            when "floor"
              daily_sales = 2000
          end
        when 5...20
          case operation
            when "mean"
              daily_sales = 3650
            when "median"
              daily_sales = 3500
            when "ceiling"
              daily_sales = 4000
            when "floor"
              daily_sales = 3000
          end
        when 1...5
          case operation
            when "mean"
              daily_sales = 8000
            when "median"
              daily_sales = 7500
            when "ceiling"
              daily_sales = 10000
            when "floor"
              daily_sales = 4000
          end
        end
      else
        daily_sales = 0
      end

      return daily_sales
    else
      return 0
    end
  end

  def median(num_arr)
    if num_arr.length > 0
      num_arr.reject!(&:nil?)
      sorted = num_arr.sort
      len = sorted.length
      return (sorted[(len - 1) / 2] + sorted[len / 2]) / 2.0
    else
      return 0
    end
  end
  #################################################
  ## End Rollup methods                          ##
  #################################################
end