module CategoriesHelper
	def nested_descendants(categories)
		content_tag(:ul) do
			categories.map do |category|
				cat = Category.find(category.id)
				niches = Bestseller.distinct.where(:category_group_id => cat.category_group_id).pluck(:category_id)
				if cat.has_children?
					sub_categories = cat.children
					if niches.include?(category.id)
						content_tag :li, :style => "padding: 5px 0 5px 0" do
							content_tag(:span, "#{cat.count.to_s(:delimited)}", :class => "badge pull-right") + link_to(category.name, "categories/#{category.id}/analytics") + nested_descendants(sub_categories)							
						end
					else
						content_tag :li, :style => "padding: 5px 0 5px 0" do
							content_tag(:span, "#{cat.count.to_s(:delimited)}", :class => "badge pull-right") + category.name.html_safe + nested_descendants(sub_categories)
						end
					end
				else
					if niches.include?(category.id)
						content_tag :li, :style => "padding: 5px 0 5px 0" do
							content_tag(:span, "#{cat.count.to_s(:delimited)}", :class => "badge pull-right") + link_to(category.name, "categories/#{category.id}/analytics")
						end
					else
						content_tag :li, :style => "padding: 5px 0 5px 0" do
							content_tag(:span, "#{cat.count.to_s(:delimited)}", :class => "badge pull-right") + category.name.html_safe
						end
					end
				end
			end.reduce(:<<)
		end
	end

	def child_categories(categories)
		content_tag(:li) do
			categories.map do |category|
				cat = Category.find(category.id)

				if category.name
					if cat.has_children?
						sub_categories = cat.children
						content_tag(:span, " / #{link_to(category.name, "#{cat.url}", :target => "BLANK")}".html_safe + child_categories(sub_categories))
					else
						content_tag(:span, " / #{link_to(category.name, "#{cat.url}", :target => "BLANK")}".html_safe)
					end
				end
			end.reduce(:<<)
		end
	end
end