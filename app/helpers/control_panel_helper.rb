module ControlPanelHelper
	def nested_descendants_form(categories)
		content_tag(:ul) do
			categories.map do |category|
				cat = Category.find(category.id)
				niches = Bestseller.distinct.where(:category_group_id => cat.category_group_id).pluck(:category_id)
				if cat.has_children?
					sub_categories = cat.children
					if niches.include?(category.id)
						content_tag :li, :style => "padding: 5px 0 5px 0;" do
							content_tag :div, :class => "form-inline" do
								text_field_tag(category.id, "#{category.watch}", :class => "form-control", :style => "width: 45px; margin: 0 10px 0 10px;") + 
								content_tag(:span, "#{cat.count.to_s(:delimited)}", :class => "badge pull-right") + link_to(category.name, "categories/#{category.id}/analytics") + nested_descendants_form(sub_categories)							
							end
						end
					else
						content_tag :li, :style => "padding: 5px 0 5px 0;" do
							content_tag :div, :class => "form-inline" do
								text_field_tag(category.id, "#{category.watch}", :class => "form-control", :style => "width: 45px; margin: 0 10px 0 10px;") + 
								content_tag(:span, "#{cat.count.to_s(:delimited)}", :class => "badge pull-right") + category.name.html_safe + nested_descendants_form(sub_categories)
							end
						end
					end
				else
					if niches.include?(category.id)
						content_tag :li, :style => "padding: 5px 0 5px 0;" do
							content_tag :div, :class => "form-inline" do
								text_field_tag(category.id, "#{category.watch}", :class => "form-control", :style => "width: 45px; margin: 0 10px 0 10px;") + 
								content_tag(:span, "#{cat.count.to_s(:delimited)}", :class => "badge pull-right") + link_to(category.name, "categories/#{category.id}/analytics")
							end
						end
					else
						content_tag :li, :style => "padding: 5px 0 5px 0;" do
							content_tag :div, :class => "form-inline" do
								text_field_tag(category.id, "#{category.watch}", :class => "form-control", :style => "width: 45px; margin: 0 10px 0 10px;") + 
								content_tag(:span, "#{cat.count.to_s(:delimited)}", :class => "badge pull-right") + category.name.html_safe
							end
						end
					end
				end
			end.reduce(:<<)
		end
	end
end