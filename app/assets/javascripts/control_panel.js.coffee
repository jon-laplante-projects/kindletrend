# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
	$("#settings_bttn").click ->
		if $("#settings_form").css("display") == "none"
			$("#settings_form").slideDown("fast")
		else
			$("#settings_form").slideUp("fast")
		return
	
$ ->
	$("[name='current_month_toggle']").change ->
		if $("#date_container").css("display") == "none"
			$("#date_container").slideDown("fast")
		else
			$("#date_container").slideUp("fast")
		return

`var link_iteration = 1;

$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
	numFiles = input.get(0).files ? input.get(0).files.length : 1,
	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});

$(function(){
	$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
    });

	$("#more_links_bttn").click(function(){
		$("#more_links_div").append("<div class='input-group' style='margin: 5px 0 5px 0;'><input type='url' name='more_links_" + link_iteration + "' id='more_links_" + link_iteration + "' class='form-control' placeholder='http://www.example.com' /><span class='input-group-btn'><button class='btn btn-danger remove_link' type='button' id='less_links_bttn'><span class='glyphicon glyphicon-minus'></span></button></span></div>");
		link_iteration++;
	});

	$("body").on("click", ".remove_link", function(){
		//alert("link_iteration: " + link_iteration);
		$(this).parent().parent().remove();
	});

	$('.popover-btn').popover({
		trigger: 'hover'
	});

	var clip = new ZeroClipboard($(".copy_to_clipboard_bttn"));

	$("#white_label_admin_form").bootstrapValidator({
		live: 'enabled',
		submitButtons: '#save_bttn',
		message: 'This value is not valid',
    	feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
    	},

    	fields: {
            company_name: {
                message: 'This is not a valid company name.',
                validators: {
                    notEmpty: {
                        message: 'Company name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        max: 100,
                        message: 'Your company name must be more than 2 and less than 100 characters long'
                    }
                }
            },

            product_name: {
                message: 'This is not a valid product name.',
                validators: {
                    notEmpty: {
                        message: 'Product name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        max: 100,
                        message: 'Your product name must be more than 2 and less than 100 characters long'
                    }
                }
            },

            support_email: {
                validators: {
                    notEmpty: {
                        message: 'Support email is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
		}
	});

	$("#kindletrend_styles").change(function(){
		var selectors = ["link[href='/assets/bootstrap.css?body=1']", "link[href='/assets/bootstrap.cerulean.min.css?body=1']", "link[href='/assets/bootstrap.flatly.min.css?body=1']", "link[href='/assets/bootstrap.journal.min.css?body=1']", "link[href='/assets/bootstrap.lumen.min.css?body=1']", "link[href='/assets/bootstrap.paper.min.css?body=1']", "link[href='/assets/bootstrap.readable.min.css?body=1']", "link[href='/assets/bootstrap.sandstone.min.css?body=1']", "link[href='/assets/bootstrap.simplex.min.css?body=1']", "link[href='/assets/bootstrap.slate.min.css?body=1']", "link[href='/assets/bootstrap.spacelab.min.css?body=1']", "link[href='/assets/bootstrap.superhero.min.css?body=1']"].join(", ");

		if ( $("#kindletrend_styles").val() == "default" ) {
			$(selectors).attr("href", "/assets/bootstrap.css?body=1");
		} else {
			var stylesheet = $("#kindletrend_styles").val();
			$(selectors).attr("href", "/assets/bootstrap." + stylesheet + ".min.css?body=1");
		};
	});
});`