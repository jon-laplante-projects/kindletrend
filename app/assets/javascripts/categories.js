$(function(){
	if (navigator.userAgent.search("Firefox") >= 0) {
		//$(".container").first().prepend("<div class='alert alert-danger lead' role='alert'><h2>Warning!</h2>A bug has been discovered for users of Firefox, and Kindle Trend will not work as expected.<p/>While we are urgently addressing the issue, we suggest using Chrome for the time being.</div>");
	}

	$(".footable").footable();

	$('.footable a').filter('[data-page="0"]').trigger('click');

	$("#remove_filter").click(function(){
		$("#filter").val("");
		$(".footable").trigger("footable_clear_filter");
	});

	$("#loading").hide();
	$("#records").show();

	$('#intro_tabs a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	});

	$('#category_tree_tabs a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	});

	$(".category-list").click(function(e){
		//alert("0");
		if (e && e.preventDefault) {
			e.preventDefault(); // DOM style
		}
		//return false;
		//alert("1");
		if ( $(this).hasClass("disabled") ) {
			return false;
		};
		//alert("2");
		$(".category_instructions").slideUp('slow');
		//alert("3");
		var url = $(this).attr("href");
		var id = url.split("a=");
		id = id[1];
		//alert("4");
		//get the footable object
	    var footable = $('.footable').data('footable');
	    //alert("5");
	    // Create a boolean to track whether we are on targets or rollups.
	    var isTarget = false;
	    if ( $(this).hasClass("targets") == true ) {
	    	isTarget = true;
	    };

		if ( $(this).hasClass('active') == true ) {
			$(this).removeClass('active');

			$(".item_" + id).remove();

			$('.footable').trigger('footable_redraw');
		} else {
			$("#loading").show();
			$(".list-group-item").addClass("disabled");
			$(this).addClass('active');

			$.ajax({
				url: url,
				dataType: 'json',
				success: function(data){
					$.each(data, function(idx, item){
						//build up the row we are wanting to add
						if ( isTarget == true ) {
							var newRow = '<tr class="item_' + id + '"><td>' + item.ancestry + '<span style="color: red; font-weight: bolder;">' + item.name + '</span></td><td><span class="badge pull-right">' + item.count + '</span></td></tr>';
						} else {
							var ancestry = "";
							if (item.ancestry != null) {
									ancestry = item.ancestry + " >";
							};

							var newRow = '<tr class="item_' + id + '"><td><div style="font-size: .65em;">' + ancestry + '</div>' + unescapeHtml(item.category) +'</td><td>' + item.child_count + '</td><td>' + item.book_count + '</td><td>' + item.est_mean_sales + '</td><td>' + item.est_mean_income + '</td><td>' + item.est_ceiling_sales + '</td><td>' + item.est_ceiling_income + '</td><td>' + item.est_floor_sales + '</td><td>' + item.est_floor_income + '</td><td>' + unescapeHtml(item.analytics_link) + '</td><td>' + unescapeHtml(item.takeoffs_link) + '</td></tr>';
						};
					    
						//add it
						footable.appendRow(newRow);
					});
				},
				error: function(){
					$("#cannotLoadModal").modal('show');
				},
				complete: function(){
					$(this).addClass('active');
					$("#loading").slideUp('slow');
					$(".list-group-item").removeClass("disabled");
				}
			});
		};
		//return false;
	});

	// Functions.
	function unescapeHtml(unsafe) {
    	return unsafe
        .replace(/&amp;/g, "&")
        .replace(/&lt;/g, "<")
        .replace(/&gt;/g, ">")
        .replace(/&quot;/g, "\"")
        .replace(/&#039;/g, "'");
	}
});
