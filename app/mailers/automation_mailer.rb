class AutomationMailer < ActionMailer::Base
  default from: "noreply@kindletrend.com"

  def category_crawl_fail(category_id)
  	@category_id = category_id
  	mail(to: "jon.laplante@kindletrend.com", subject: "Category crawl failed")
  end
end
