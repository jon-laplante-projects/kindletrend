class AccountMailer < ActionMailer::Base
  default from: "noreply@kindletrend.com"

  def jv_join_welcome(email, password)
  	@email = email
  	@password = password
  	mail(to: @email, subject: "Welcome to Kindle Trend! Contains your account details")
  end

  def partner_join_welcome(email, password, partner_friendly_product)
  	@email = email
  	@password = password
  	@partner_friendly_product = partner_friendly_product
  	mail(to: @email, subject: "Welcome to #{@partner_friendly_product}! Contains your account details")
  end
end
