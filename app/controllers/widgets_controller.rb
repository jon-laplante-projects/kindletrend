class WidgetsController < ApplicationController
	def partner_sign_up
		@partner = Partner.find_by(token: params[:pid])
		unless @partner.nil?
			respond_to do |format|
				format.js { render }
			end
		else
			render :nothing
		end
	end

	def partner_iframe
		@partner = Partner.find_by(token: params[:pid])
		unless @partner.nil?
			respond_to do |format|
				format.js { render }
			end
		else
			render :nothing
		end
	end

	def partner_resize_injection
		@partner = Partner.find_by(token: params[:pid])
		unless @partner.nil?
			respond_to do |format|
				format.js { render }
			end
		else
			render :nothing
		end
	end
end
