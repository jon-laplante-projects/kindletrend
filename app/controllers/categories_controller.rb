class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  layout :set_layout

  # GET /categories
  # GET /categories.json
  def set_layout
    "layouts/application"

    if current_user.account.partner.present? && current_user.account.partner.partner_type_id == 2
      "layouts/blank"
    end
  end

  def index
    @top = []
    json_categories = []
    cat_group = CategoryGroup.where(complete: true).last
    first_cat = cat_group.categories.first
    top_categories = Category.where(:ancestry => "#{first_cat.id}")
    top_categories.each do |t|
      json_category = {}

      json_category["name"] = t.name
      json_category["url"] = "http://www.amazon.com/Best-Sellers-Kindle-Store/zgbs/digital-text/#{t.asin}?_encoding=UTF8&pg=1"
      json_categories << json_category

      categories = {}

      children = cat_group.categories.find(t.id).children
      categories["root"] = first_cat.id
      categories["id"] = t.id
      categories["name"] = t.name
      categories["category_count"] = t.count
      categories["child_count"] = t.child_ids.count
      @top << categories
    end
    #@niches = Bestseller.select(:category_id).distinct.pluck(:category_id)
    #####################################################
    @niches = cat_group.categories.where("watch IS NOT NULL").pluck(:id)
    #####################################################
    #@watched = Category.where(:watch => true).pluck(:id)
    if params[:a]
      @category = Category.find(params[:a])
    end

    @children = Category.where(:ancestry => "#{params[:r]}/#{params[:a]}")

    respond_to do |format|
      format.html {  }
      format.json { render json: json_categories }
    end
  end

  def analytics
    @category = Category.find(params[:category])
    cat_root = Category.find_by(category_group_id: @category.category_group_id)
    @ancestors = @category.ancestors
    #@descendants = @category.descendants
    @children = @category.children

    bestseller = Bestseller.where(category_id: @category.id).last
    #@ebooks = bestseller.ebooks(:include => :authors)
    @ebooks = bestseller.ebooks.includes(:authors).includes(:ranks).where("name IS NOT NULL")#.where("ranks.category_id = 1")
    #@authors = @ebooks.authors

    ebook_ids = Array.new

    # Get title keywords.
    titles = keywords(bestseller.id, "title")
    @titlewords = titles.sort_by {|key, value| -value}
    # End title keywords.

    # Get desc keywords.
    descs = keywords(bestseller.id, "desc")
    @descwords = descs.sort_by {|key, value| -value}
    # End desc keywords.

    # Get related categories.
    @related_categories = Category.find_by_sql("SELECT COUNT(rank_counts.category_id) AS category_count, categories.name, category_id, ancestry FROM ranks AS rank_counts LEFT OUTER JOIN categories ON category_id = categories.id WHERE rank_counts.ebook_id IN (SELECT id FROM ebooks WHERE bestseller_id = #{bestseller.id}) GROUP BY category_id, categories.id, ancestry ORDER BY category_id")
    # End related categories.

    @ebooks.each do |e|
      ebook_ids << e.id
    end

    id_string = ebook_ids.join(",")

    # Get rating.
    rating = 0
    @ebooks.each do |e|
      rating = rating + e.rating.to_f
    end

    @rating = (rating/@ebooks.count).to_f.round(2).to_s
    # End get rating.

    # Get estimated gender.
    @author_genders = Author.find_by_sql("SELECT COUNT(CASE WHEN gender = 'male' THEN 1 END) AS 'male', COUNT(CASE WHEN gender = 'female' THEN 1 END) AS 'female', COUNT(id) AS 'count' FROM authors WHERE ebook_id IN (#{id_string});")
    # End estimated gender.

    # Get competitiveness.
    @ceiling_count = Category.order("count DESC").first
    @cat_count = @category.count
    @floor_count = Category.order("count ASC").first
    # End competitiveness.

    # Ranks and price arrays.
    ranks_arr = Array.new
    price_arr = Array.new
    reviews_arr = Array.new
    pages_arr = Array.new
    filesizes_arr = Array.new

    @ebooks.each do |e|
      e.ranks.where(category_id: cat_root.id).each do |r|
        ranks_arr << r.rank
      end
    end

    @ebooks.each do |p|
      price_arr << p.price unless p.price.blank?
      reviews_arr << p.reviews unless p.reviews.blank?
      pages_arr << p.pages unless p.pages.blank?
      if p.filesize
        filesizes_arr << p.filesize[0, p.filesize.length - 3].to_i
      end
    end

    # Calculate means.
    @mean_rank = (ranks_arr.inject{|sum,x| sum + x }/ranks_arr.size).to_i
    @mean_price = (price_arr.inject{|sum,x| sum + x }/price_arr.size).to_f
    @mean_reviews = (reviews_arr.inject{|sum,x| sum + x }/reviews_arr.size).to_i.to_s(:delimited)
    @mean_pages = (pages_arr.inject{|sum,x| sum.to_i + x.to_i }/pages_arr.size).to_i.to_s(:delimited)
    @mean_filesize = (filesizes_arr.inject{|sum,x| sum + x }/filesizes_arr.size).to_i

    # Calculate medians.
    @median_rank = median(ranks_arr).to_i
    @median_price = median(price_arr).to_f
    @median_reviews = median(reviews_arr).to_i.to_s(:delimited)
    @median_pages = median(pages_arr).to_i.to_s(:delimited)
    @median_filesize = median(filesizes_arr).to_i

    # Calculate floors.
    # Floor rank is actually the HIGHTEST rank.
    @floor_rank = ranks_arr.sort.last.to_i
    @floor_price = price_arr.sort.first.to_f
    @floor_reviews = reviews_arr.sort.first.to_i.to_s(:delimited)
    @floor_pages = pages_arr.sort.first.to_i.to_s(:delimited)
    @floor_filesize = filesizes_arr.sort.first.to_i

    # Calculate ceilings.
    # Ceiling rank is actually the LOWEST rank.
    @ceiling_rank = ranks_arr.sort.first.to_i
    @ceiling_price = price_arr.sort.last.to_f
    @ceiling_reviews = reviews_arr.sort.last.to_i.to_s(:delimited)
    @ceiling_pages = pages_arr.sort.last.to_i.to_s(:delimited)
    @ceiling_filesize = filesizes_arr.sort.last.to_i

    #@mean_est_earnings = earningsEstimator(@mean_rank, @mean_price, @mean_filesize, "mean")
    #@median_est_earnings = earningsEstimator(@median_rank, @median_price, @median_filesize, "median")
    #@floor_est_earnings = earningsEstimator(@floor_rank, @floor_price, @floor_filesize, "floor")
    #@ceiling_est_earnings = earningsEstimator(@ceiling_rank, @ceiling_price, @ceiling_filesize, "ceiling")
    @mean_est_earnings = earningsEstimator(@mean_rank, 2.99, @mean_filesize, "mean")
    @median_est_earnings = earningsEstimator(@median_rank, 2.99, @median_filesize, "median")
    @floor_est_earnings = earningsEstimator(@floor_rank, 2.99, @floor_filesize, "floor")
    @ceiling_est_earnings = earningsEstimator(@ceiling_rank, 2.99, @ceiling_filesize, "ceiling")

    @mean_est_sales = salesEstimator(@mean_rank, "mean").to_s(:delimited)
    @median_est_sales = salesEstimator(median(ranks_arr), "median").to_s(:delimited)
    @floor_est_sales = salesEstimator(@floor_rank, "floor").to_s(:delimited)
    @ceiling_est_sales = salesEstimator(@ceiling_rank, "ceiling").to_s(:delimited)

    # Age data on ebooks.
    @ebook_age = Ebook.find_by_sql("SELECT COUNT(CASE WHEN publication_date > '#{(Time.now - 1.month).to_s(:db)}' THEN 1 END) AS 'one_month', COUNT(CASE WHEN publication_date <= '#{(Time.now - 1.month).to_s(:db)}' AND publication_date > '#{(Time.now - 3.months).to_s(:db)}' THEN 1 END) AS 'three_months', COUNT(CASE WHEN publication_date <= '#{(Time.now - 3.months).to_s(:db)}' AND publication_date > '#{(Time.now - 6.months).to_s(:db)}' THEN 1 END) AS 'six_months', COUNT(CASE WHEN publication_date <= '#{(Time.now - 6.months).to_s(:db)}' AND publication_date > '#{(Time.now - 1.year).to_s(:db)}' THEN 1 END) AS 'one_year', COUNT(CASE WHEN publication_date < '#{(Time.now - 1.year).to_s(:db)}' THEN 1 END) AS 'older' FROM ebooks WHERE bestseller_id = #{bestseller.id};")

    # Day of week published.
    @sunday = 0
    @monday = 0 
    @tuesday = 0
    @wednesday = 0
    @thursday = 0
    @friday = 0
    @saturday = 0

    @ebooks.each do |e|
      unless e.publication_date.blank?
        @sunday += 1 if e.publication_date.wday == 0
        @monday += 1 if e.publication_date.wday == 1
        @tuesday += 1 if e.publication_date.wday == 2
        @wednesday += 1 if e.publication_date.wday == 3
        @thursday += 1 if e.publication_date.wday == 4
        @friday += 1 if e.publication_date.wday == 5
        @saturday += 1 if e.publication_date.wday == 6
      end
    end

    # Percentage of indie published.
    @publishers = bestseller.ebooks.find_by_sql("SELECT COUNT(CASE WHEN publisher = '' THEN 1 END) AS 'self', COUNT(CASE WHEN publisher <> '' THEN 1 END) AS 'firm', COUNT(id) AS 'count' FROM ebooks WHERE bestseller_id = #{bestseller.id};")

    @ranks = bestseller.ebooks.pluck(:rank).sort

    if @ranks.last >= 100
      # 81 - 100 data...
      hundred_ebook = bestseller.ebooks.find_by(:rank => 100)
      unless hundred_ebook.blank? || hundred_ebook.ranks.blank?
        hundred_ranks = hundred_ebook.ranks.find_by(category_id: cat_root.id)
        @hundred_sales = salesEstimator(hundred_ranks.rank, "mean") > 0 ? salesEstimator(hundred_ranks.rank, "mean") : 1
        @hundred_income = earningsEstimator(hundred_ranks.rank, 2.99, 1500, "mean")
      else
        @hundred_sales = 0
        @hundred_income = 0
      end
    end

    if @ranks.last >= 80
      # 61 - 80 data...
      eighty_ebook = bestseller.ebooks.find_by(:rank => 80)
      unless eighty_ebook.blank? || eighty_ebook.ranks.blank?
        eighty_ranks = eighty_ebook.ranks.find_by(category_id: cat_root.id)
        @eighty_sales = salesEstimator(eighty_ranks.rank, "mean") > 0 ? salesEstimator(eighty_ranks.rank, "mean") : 1
        @eighty_income = earningsEstimator(eighty_ranks.rank, 2.99, 1500, "mean")

        @eighty_sales = @hundred_sales + 5 if @eighty_sales <= @hundred_sales.to_i && @hundred_sales
        @eighty_income = @hundred_income + 1345.5 if @eighty_income <= @hundred_income.to_f && @hundred_income
      else
        @eighty_sales = 0
        @eighty_income = 0
      end
    end

    if @ranks.last >= 60
      # 41 - 60 data...
        sixty_ebook = bestseller.ebooks.find_by(bestseller_id: bestseller.id, :rank => 60)
        unless sixty_ebook.blank? || sixty_ebook.ranks.blank?
          sixty_ranks = sixty_ebook.ranks.find_by(category_id: cat_root.id)
          @sixty_sales = salesEstimator(sixty_ranks.rank, "mean") > 0 ? salesEstimator(sixty_ranks.rank, "mean") : 1
          @sixty_income = earningsEstimator(sixty_ranks.rank, 2.99, 1500, "mean")

          @sixty_sales = @eighty_sales + 5 if @sixty_sales <= @eighty_sales.to_i && @eighty_sales
          @sixty_income = @eighty_income + 1345.5 if @sixty_income <= @eighty_income.to_f && @eighty_income
        else
          @sixty_sales = 0
          @sixty_income = 0
        end
    end

    if @ranks.last >= 40
      # 21 - 40 data...
      forty_ebook = bestseller.ebooks.find_by("name IS NOT NULL AND bestseller_id = #{bestseller.id} AND rank = 40")
      unless forty_ebook.blank? || forty_ebook.ranks.blank?
        forty_ranks = forty_ebook.ranks.find_by(category_id: cat_root.id) unless forty_ebook.blank?
        @forty_sales = salesEstimator(forty_ranks.rank, "mean") > 0 ? salesEstimator(forty_ranks.rank, "mean") : 1
        @forty_income = earningsEstimator(forty_ranks.rank, 2.99, 1500, "mean")

        @forty_sales = @sixty_sales + 5 if @forty_sales <= @sixty_sales.to_i && @sixty_sales
        @forty_income = @sixty_income + 1345.5 if @forty_income <= @sixty_income.to_f && @sixty_income
      else
        @forty_sales = 0
        @forty_income = 0
      end
    end

    if @ranks.last >= 20
      # Top 20 data...
      twenty_ebook = bestseller.ebooks.find_by("name IS NOT NULL AND bestseller_id = #{bestseller.id} AND rank = 20")#,bestseller_id: bestseller.id, :rank => 20)
      unless twenty_ebook.blank? || twenty_ebook.ranks.blank?
        twenty_ranks = twenty_ebook.ranks.find_by(category_id: cat_root.id) unless twenty_ebook.blank?
        one_ebook = bestseller.ebooks.find_by(bestseller_id: bestseller.id, :rank => 1)
        one_ranks = one_ebook.ranks.find_by(category_id: cat_root.id)
        @twenty_sales = salesEstimator(twenty_ranks.rank, "mean") > 0 ? salesEstimator(twenty_ranks.rank, "mean") : 1
        @twenty_income = earningsEstimator(twenty_ranks.rank, 2.99, 1500, "mean")
      else
        @twenty_sales = 0
        @twenty_income = 0
      end
      unless one_ranks.blank?
        @one_sales = salesEstimator(one_ranks.rank, "mean") > 0 ? salesEstimator(one_ranks.rank, "mean") : 1
        @one_income = earningsEstimator(one_ranks.rank, 2.99, 1500, "mean")
      else
        @one_sales = 0
        @one_income = 0
      end


      @twenty_sales = @forty_sales + 5 if @twenty_sales <= @forty_sales.to_i && @forty_sales
      @twenty_income = @forty_income + 1345.5 if @twenty_income <= @forty_income.to_f && @forty_income
      @one_sales = @twenty_sales + 5 if @one_sales <= @twenty_sales.to_i && @twenty_sales
      @one_income = @twenty_income + 1345.5 if @one_income <= @twenty_income.to_f && @twenty_income
    end
  end

  def targets
    #@gimmes = Category.where("count < 100").paginate(:page => params[:page], :per_page => 50)
    @gimmes = Array.new
    @active = ""
    cat_group = CategoryGroup.where(complete: true).last

    if params[:a].blank? == false
      @cat_name = Category.select("name").find(params[:a]).name
      first_cat = cat_group.categories.first
      @top_categories = cat_group.categories.where(:ancestry => "#{first_cat.id}")
      @active = params[:a]
    else
      @cat_name = "All Easy Targets"
      first_cat = cat_group.categories.first
      @top_categories = cat_group.categories.where(:ancestry => "#{first_cat.id}")
    end

    respond_to do |format|
      format.html {  }
      format.json do
        if params[:a].blank? == false
          first_cat = cat_group.categories.first
          gimmes = cat_group.categories.where("count < 100 AND ancestry LIKE '#{first_cat.id}/#{params[:a]}%'")
          @active = params[:a]
        else
          gimmes = cat_group.categories.where("count < 100")
          first_cat = cat_group.categories.first
        end

        gimmes.each do |g|
          item = Hash.new

          item["id"] = g.id
          item["name"] = g.name
          item["ancestry"] = ""
          item["count"] = g.count
          item["created_at"] = g.created_at

          ancestors = g.ancestors

          ancestors.each do |a|
            item["ancestry"] = item["ancestry"].concat("#{a.name} / ")
          end

          @gimmes << item
        end

        render json: @gimmes
      end
    end
  end

  def low_competition
    @low_competition = cat_group.categories.where("count BETWEEN 101 AND 250")
  end

  def takeoff
    bestseller = Bestseller.where(category_id: params[:category_id], complete: true).last
    cat_root = Category.where(category_group_id: bestseller.category_group_id).first
    @category = Category.find(params[:category_id])

    @ranks = bestseller.ebooks.pluck(:rank).sort

    if @ranks.last >= 100
      # 81 - 100 data...
      hundred_ebook = bestseller.ebooks.find_by(rank: 100)
      unless hundred_ebook.blank? || hundred_ebook.ranks.blank?
        hundred_ranks = hundred_ebook.ranks.find_by(category_id: cat_root.id)
        @hundred_sales = salesEstimator(hundred_ranks.rank, "mean") > 0 ? salesEstimator(hundred_ranks.rank, "mean") : 1
        @hundred_income = earningsEstimator(hundred_ranks.rank, 2.99, 1500, "mean")
      else
        @hundred_sales = "N/A"
        @hundred_income = "N/A"
      end
    end

    if @ranks.last >= 80
      # 61 - 80 data...
      eighty_ebook = bestseller.ebooks.find_by(rank: 80)
      unless eighty_ebook.blank? || eighty_ebook.ranks.blank?
        eighty_ranks = eighty_ebook.ranks.find_by(category_id: cat_root.id)
        @eighty_sales = salesEstimator(eighty_ranks.rank, "mean") > 0 ? salesEstimator(eighty_ranks.rank, "mean") : 1
        @eighty_income = earningsEstimator(eighty_ranks.rank, 2.99, 1500, "mean")

        @eighty_sales = @hundred_sales + 5 if @eighty_sales <= @hundred_sales.to_i && @hundred_sales
        @eighty_income = @hundred_income + 1345.5 if @eighty_income <= @hundred_income.to_f && @hundred_income
      else
        @eighty_sales = "N/A"
        @eighty_income = "N/A"
      end
    end

    if @ranks.last >= 60
      # 41 - 60 data...
      unless bestseller.ebooks.find_by(bestseller_id: bestseller.id, :rank => 60).blank? || bestseller.ebooks.find_by(bestseller_id: bestseller.id, :rank => 60).ranks.blank?
        sixty_ebook = bestseller.ebooks.find_by(bestseller_id: bestseller.id, :rank => 60)
        sixty_ranks = sixty_ebook.ranks.find_by(category_id: cat_root.id)
        @sixty_sales = salesEstimator(sixty_ranks.rank, "mean") > 0 ? salesEstimator(sixty_ranks.rank, "mean") : 1
        @sixty_income = earningsEstimator(sixty_ranks.rank, 2.99, 1500, "mean")

        @sixty_sales = @eighty_sales + 5 if @sixty_sales <= @eighty_sales.to_i && @eighty_sales
        @sixty_income = @eighty_income + 1345.5 if @sixty_income <= @eighty_income.to_f && @eighty_income
      else
        @sixty_sales = "N/A"
        @sixty_income = "N/A"
      end
    end

    if @ranks.last >= 40
      # 21 - 40 data...
      forty_ebook = bestseller.ebooks.find_by(bestseller_id: bestseller.id, :rank => 40)
      unless forty_ebook.blank? || forty_ebook.ranks.blank?
        forty_ranks = forty_ebook.ranks.find_by(category_id: cat_root.id)
        @forty_sales = salesEstimator(forty_ranks.rank, "mean") > 0 ? salesEstimator(forty_ranks.rank, "mean") : 1
        @forty_income = earningsEstimator(forty_ranks.rank, 2.99, 1500, "mean")

        @forty_sales = @sixty_sales + 5 if @forty_sales <= @sixty_sales.to_i && @sixty_sales
        @forty_income = @sixty_income + 1345.5 if @forty_income <= @sixty_income.to_f && @sixty_income
      else
        @forty_sales = "N/A"
        @forty_income = "N/A"
      end
    end

    if @ranks.last >= 20
      # Top 20 data...
      twenty_ebook = bestseller.ebooks.find_by(bestseller_id: bestseller.id, :rank => 20)
      unless twenty_ebook.blank? || twenty_ebook.ranks.blank?
        twenty_ranks = twenty_ebook.ranks.find_by(category_id: cat_root.id)
        @twenty_sales = salesEstimator(twenty_ranks.rank, "mean") > 0 ? salesEstimator(twenty_ranks.rank, "mean") : 1
        @twenty_income = earningsEstimator(twenty_ranks.rank, 2.99, 1500, "mean")

        @twenty_sales = @forty_sales + 5 if @twenty_sales <= @forty_sales.to_i && @forty_sales
        @twenty_income = @forty_income + 1345.5 if @twenty_income <= @forty_income.to_f && @forty_income
      else
        @twenty_sales = "N/A"
        @twenty_income = "N/A"
      end
    end

    if @ranks.last >= 10
      ten_ebook = bestseller.ebooks.find_by(bestseller_id: bestseller.id, rank: 10)
      ten_ranks = ten_ebook.ranks.find_by(category_id: cat_root.id)
      one_ebook = bestseller.ebooks.find_by(bestseller_id: bestseller.id, rank: 1)
      one_ranks = one_ebook.ranks.find_by(category_id: cat_root.id)
      
      unless ten_ranks.blank?
        @ten_sales = salesEstimator(ten_ranks.rank, "mean") > 0 ? salesEstimator(ten_ranks.rank, "mean") : 1
        @ten_income = earningsEstimator(ten_ranks.rank, 2.99, 1500, "mean")
      else
        @ten_sales = 0
        @ten_income = 0
      end
      
      unless one_ranks.blank?
        @one_sales = salesEstimator(one_ranks.rank, "mean") > 0 ? salesEstimator(one_ranks.rank, "mean") : 1
        @one_income = earningsEstimator(one_ranks.rank, 2.99, 1500, "mean")
      else
        @one_sales = 0
        @one_income = 0
      end

      @ten_sales = @twenty_sales + 5 if @ten_sales <= @twenty_sales.to_i && @twenty_sales
      @ten_income = @twenty_income + 1345.5 if @ten_income <= @twenty_income.to_f && @twenty_income      
      @one_sales = @ten_sales + 5 if @one_sales <= @ten_sales.to_i && @ten_sales
      @one_income = @ten_income + 1345.5 if @one_income <= @ten_income.to_f && @ten_income
    end
  end

  def amazon_suggest
    a = Mechanize.new { |agent| agent.user_agent_alias = "Mac Safari" }
    #suggestions = a.get("http://completion.amazon.com/search/complete?method=completion&q=#{params[:keywords]}&search-alias=digital-text&client=amazon-search-ui&mkt=1&fb=1&xcat=0&x=updateISSCompletion&sc=1&noCacheIE=1394791137430")
    suggestions = Nokogiri::HTML(a.get("http://completion.amazon.com/search/complete?method=completion&q=#{params[:keywords]}&search-alias=digital-text&client=amazon-search-ui&mkt=1&fb=1&xcat=0&x=updateISSCompletion&sc=1&noCacheIE=1394791137430").content)
    suggestions = suggestions.xpath("//p").text.split(";")[0]
    suggestions = suggestions[13,suggestions.length]
    suggestions = JSON.parse(suggestions)
    s = suggestions[1]
    respond_to do |format|
      format.json { render json: s }
    end
  end

  # GET /categories/1
  # GET /categories/1.json
#  def show
#  end

  # GET /categories/new
#  def new
#    @category = Category.new
#  end

  # GET /categories/1/edit
#  def edit
#  end

  # POST /categories
  # POST /categories.json
#  def create
#    @category = Category.new(category_params)
#
#    respond_to do |format|
#      if @category.save
#        format.html { redirect_to @category, notice: 'Category was successfully created.' }
#        format.json { render action: 'show', status: :created, location: @category }
#      else
#        format.html { render action: 'new' }
#        format.json { render json: @category.errors, status: :unprocessable_entity }
#      end
#    end
#  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
#  def update
#    respond_to do |format|
#      if @category.update(category_params)
#        format.html { redirect_to @category, notice: 'Category was successfully updated.' }
#        format.json { head :no_content }
#      else
#        format.html { render action: 'edit' }
#        format.json { render json: @category.errors, status: :unprocessable_entity }
#      end
#    end
#  end

  # DELETE /categories/1
  # DELETE /categories/1.json
#  def destroy
#    @category.destroy
#    respond_to do |format|
#      format.html { redirect_to categories_url }
#      format.json { head :no_content }
#    end
#  end

  def rollup
    # Amazon autocomplete URL = http://completion.amazon.com/search/complete?method=completion&q=domain&search-alias=digital-text&client=amazon-search-ui&mkt=1&fb=1&xcat=0&x=updateISSCompletion&sc=1&noCacheIE=1394181424869
    cat_group = CategoryGroup.where(complete: true).last
    @active = ""

    if params[:a].blank? == false
      @cat_name = Category.select("name").find(params[:a])
      @cat_name = @cat_name.name
      first_cat = cat_group.categories.first
      #@gimmes = cat_group.categories.where("count < 100 AND ancestry LIKE '#{first_cat.id}/#{params[:a]}%'")
      @top_categories = cat_group.categories.where(:ancestry => "#{first_cat.id}")

      category_name_ids = Category.select(:id).where(name: @cat_name)
      selected_categories = category_name_ids.map {|n| n.id }

      category_name_ids.each do |i|
        temp = Category.select(:id).where("ancestry LIKE '%/?/%'", i)

        unless temp.blank?
          temp.each do |t|
            selected_categories << t.id
          end
        end

        #select_categories = selected_categories.reject! { |c| c.blank? }

        # Set the active category so styling works correctly in the view.
        @active = params[:a]

        pool = Bestseller.pluck(:category_group_id)
        bestsellers = Bestseller.find_by_sql("SELECT id, category_id FROM bestsellers b1 WHERE NOT EXISTS (SELECT id, category_id FROM bestsellers b2 WHERE b1.id < b2.id AND b2.category_id = b1.category_id) AND complete = true AND category_id IN (#{selected_categories.join(",")});")
        bestsellers_arr = bestsellers.map { |b| b.id }
        rollups = Rollup.find_by_sql("SELECT id AS 'g_id', MAX(id) AS 'id', bestseller_id FROM rollups GROUP BY bestseller_id;")
        rollups_arr = rollups.map { |r| r.id }
        @rollups = Rollup.where("id IN (?) AND bestseller_id IN (?)", rollups_arr, bestsellers_arr)
      end
    else
      @cat_name = "All Categories"
      #@gimmes = cat_group.categories.where("count < 100")
      first_cat = cat_group.categories.first
      @top_categories = cat_group.categories.where(:ancestry => "#{first_cat.id}")
    end

    respond_to do |format|
      format.html {  }
      format.json do
        rollups_arr = Array.new
        rollup_table = Array.new
        #rollup_hash = Hash.new
        
        @rollups.each do |rollup|
        bestseller = Bestseller.find(rollup.bestseller_id)
        cat = Category.find(bestseller.category_id)
        ancestors = cat.ancestors
          rollups_arr << { row_id: rollup.id, ancestors: ancestors, category: "#{cat.name}", cat_group: cat.category_group_id, cat_id: cat.id, category_child_count: rollup.category_child_count.to_s(:delimited), category_book_count: rollup.category_book_count.to_s(:delimited), estimated_mean_sales: rollup.estimated_mean_sales.to_s(:delimited), estimated_mean_income: rollup.estimated_mean_income, estimated_ceiling_sales: rollup.estimated_ceiling_sales.to_s(:delimited), estimated_ceiling_income: rollup.estimated_ceiling_income, estimated_floor_sales: rollup.estimated_floor_sales.to_s(:delimited), estimated_floor_income: rollup.estimated_floor_income }
        end

        arr = rollups_arr.group_by { |rollup| rollup[:category] }
        arr.each do |a|
          row = Hash.new
          groups = a[1].sort_by { |rollup| rollup[:cat_group] }
          groups = groups.reverse

          groups.first[:ancestors].each do |a|
            unless a.name == "Kindle eBooks"
              if row["ancestry"].blank?
                row["ancestry"] = row["ancestry"].to_s.concat(a.name)
              else
                row["ancestry"] = row["ancestry"].to_s.concat(" > " + a.name)
              end
            end

          end
          row["category"] = view_context.link_to groups.first[:category], :controller => "categories", :action => "analytics", :category => groups.first[:cat_id]
          row["child_count"] = groups.first[:category_child_count]
          row["book_count"] = groups.first[:category_book_count]
          row["est_mean_sales"] = groups.first[:estimated_mean_sales]
          row["est_mean_income"] = view_context.number_to_currency(groups.first[:estimated_mean_income])
          row["est_ceiling_sales"] = groups.first[:estimated_ceiling_sales]
          row["est_ceiling_income"] = view_context.number_to_currency(groups.first[:estimated_ceiling_income])
          row["est_floor_sales"] = groups.first[:estimated_floor_sales]
          row["est_floor_income"] = view_context.number_to_currency(groups.first[:estimated_floor_income])
          row["analytics_link"] = view_context.link_to :controller => "categories", :action => "analytics", :category => groups.first[:cat_id] { '<span class="glyphicon glyphicon-stats" title="Category metrics"></span>' }
          row["takeoffs_link"] = view_context.link_to :controller => "categories", :action => "takeoff", :category_id => groups.first[:cat_id] { '<span class="glyphicon glyphicon-plane" title="Taking off in this category"></span>' }

            rollup_table << row
        end

        render json: rollup_table
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params[:category]
    end
end
