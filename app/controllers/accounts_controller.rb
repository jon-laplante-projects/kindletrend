class AccountsController < ApplicationController
	skip_before_action :verify_authenticity_token

	layout :set_layout

	def set_layout
		"layouts/application"

		partner = Partner.find_by(token: params[:bpid]) if params[:bpid]

		if (current_user.present? && current_user.account.partner.present? && current_user.account.partner.partner_type_id == 2) || (partner.present? && partner.partner_type_id == 2)
		  "layouts/blank"
		end
	end

	def whitelabel_login

	end

	def whitelabel_create_user_session
		pid = params[:pid]
		user = User.find_by(email: params[:user][:email])

		if user.present? && user.valid_password?(params[:user][:password]) == true
			flash[:info] = "Welcome back!"
			sign_in_and_redirect(user, params)
		else
			flash[:error] = "Uh-oh! You entered an invalid email or password."
			redirect_to "/app/partner/login?bpid=#{pid}"
		end

	end

	def whitelabel_update_account
		if current_user.valid_password?(params[:user][:current_password])
			if current_user.update_with_password({
					current_password: "#{params[:user][:current_password]}",
					password: "#{params[:user][:password]}",
					password_confirmation: "#{params[:user][:password_confirmation]}"
				})
				sign_in(current_user, :bypass => true)
				flash[:info] = "Your account changes have been saved."
				redirect_to controller: :welcome, action: :index
			else
				flash[:error] = "Hmmmm. We weren't able to update your password. Please try again."
				redirect_to "whitelabel_edit_account"
			end
		else
			flash[:error] = "Uh-oh! You entered an invalid email or password"
			redirect_to "whitelabel_edit_account"
		end
	end

	def whitelabel_logout
		pid = current_user.account.partner.token
		sign_out(current_user)

		redirect_to "/app/partner/login?bpid=#{pid}"
	end

	def notification_processor(processor, trans, email, name, pwd, user_id, processor_id, subscription_id, partner_id)
		trans = trans.upcase
		case trans
			when "SALE"
				acct_status = "approved"
			when "JV_SALE"
				acct_status = "approved"
			when "TEST_SALE"
				acct_status = "approved"
			when "TEST_JV_SALE"
				acct_status = "approved"
			when "RFND"
				acct_status = "frozen"
			when "TEST_RFND"
				acct_status = "frozen"
			when "CANCEL-REBILL"
				acct_status = "cancelled"
			when "UNCANCEL-REBILL"
				acct_status = "cancelled"
			when "CGBK"
				acct_status = "frozen"
		end

		if trans == "SALE" || trans == "JV_SALE" || trans == "TEST_SALE" || trans == "TEST_JV_SALE"


			unless name.blank?
				acct = Account.where(user_id: user_id).first
				acct.customer_name = name
				acct.payment_processor = processor
				acct.status = acct_status
				unless processor_id.blank?
					acct.processor_id = processor_id
				end
				unless subscription_id.blank?
					acct.subscription_id = subscription_id
				end
				unless partner_id.blank?
					acct.partner_id = partner_id
				end
				acct.save
			end

			#File.open("#{Rails.root}/log/credentials.log", "a") do |f|
			#	f.write("Name: #{name}\n")
			#	f.write("email: #{email}\n")
			#	f.write("password: #{pwd}\n")
			#	f.write("\n")
			#end

			#AccountMailer.jv_join_welcome(email, pwd).deliver
		else
			usr = User.find_by(email: email)
			acct = Account.find_by(user_id: usr.id)
			acct.status = acct_status
			acct.save
		end
		#render :nothing => true
	end

	def jvzoo_instant_notification
		#render :text => "Kindle Trend echo.", :status => 200, :content_type => 'text/html'
		
		logger.info "Received POST from JVZoo."
		ccustname = params[:ccustname]
		logger.info "ccustname = #{ccustname}."
		#ccuststate = params[:ccuststate]
		#ccustcc = params[:ccustcc]
		ccustemail = params[:ccustemail]
		logger.info "ccustemail = #{ccustemail}."
		#cproditem = params[:cproditem]
		#cprodtitle = params[:cprodtitle]
		#cprodtype = params[:cprodtype]
		ctransaction = params[:ctransaction]
		logger.info "ctransaction = #{ctransaction}."
		#ctransaffiliate = params[:ctransaffiliate]
		#ctransamount = params[:ctransamount]
		#ctranspaymentmethod = params[:ctranspaymentmethod]
		#ctransvendor = params[:ctransvendor]
		#ctransreceipt = params[:ctransreceipt]
		#cupsellreceipt = params[:cupsellreceipt]
		#caffitid = params[:caffitid]
		#cvendthru = params[:cvendthru]
		#cverify = params[:cverify]
		#ctranstime = params[:ctranstime]

		# Trans types: SALE, BILL, RFND, CGBK, INSF, CANCEL-REBILL, UNCANCEL-REBILL
		pwd = SecureRandom.hex(8)

		# Spawn a new User object...
		usr = User.new
		usr.password = pwd
		usr.password_confirmation = pwd
		usr.email = ccustemail
		usr.save

		# processor, trans, email, name, pwd, user_id, processor_id, subscription_id, partner_id
		#User.create!({:email => "#{ccustemail}", :password => "#{pwd}", :password_confirmation => "#{pwd}" })
		notification_processor("jvzoo", "#{ctransaction}", "#{ccustemail}", "#{ccustname}", "#{pwd}", usr.id, nil, nil, nil)

		@email = ccustemail
		@password = pwd

		AccountMailer.jv_join_welcome(ccustemail, pwd).deliver

		# Build a response string.
		params_str = ""
		params.each do |p|
			params_str.concat("#{p} | ")
		end

		params_str.concat("6Pa0wh3qjq3_f_4")

		respond_to do |format|
			#format.html {render :jvzoo_instant_notification, :layout => false}
			format.html do
				logger.info "Returning #{params_str}"
				render text: params_str
			end
		end
		#head 200, :content_type => 'text/html'
		#render :html => layout: false, :status => 200
	end

	def clickbank_instant_notification
		#caccountamount = params[:caccountamount]
		#ccurrency = params[:ccurrency]
		#ccustaddr1 = params[:ccustaddr1]
		#ccustaddr2 = params[:ccustaddr2]
		#ccustcc = params[:ccustcc]
		#ccustcity = params[:ccustcity]
		#ccustcounty = params[:ccustcounty]
		ccustemail = params[:ccustemail]
		#ccustfirstname = params[:ccustfirstname]
		ccustfullname = params[:ccustfullname]
		#ccustlastname = params[:ccustlastname]
		#ccustshippingcountry = params[:ccustshippingcountry]
		#ccustshippingzip = params[:ccustshippingzip]
		#ccuststate = params[:ccuststate]
		#ccustzip = params[:ccustzip]
		#cfuturepayments = params[:cfuturepayments]
		#cnextpaymentdate = params[:cnextpaymentdate]
		#corderamount = params[:corderamount]
		#cprocessedpayments = params[:cprocessedpayments]
		#cproditem = params[:cproditem]
		#cprodtitle = params[:cprodtitle]
		#cprodtype = params[:cprodtype]
		#crebillamnt = params[:crebillamnt]
		#crebillstatus = params[:crebillstatus]
		#ctid = params[:ctid]
		ctransaction = params[:ctransaction]
		#ctransaffiliate = params[:ctransaffiliate]
		#ctranspaymentmethod = params[:ctranspaymentmethod]
		#ctranspublisher = params[:ctranspublisher]
		#ctransreceipt = params[:ctransreceipt]
		#ctransrole = params[:ctransrole]
		#ctranstime = params[:ctranstime]
		#cupsellreceipt = params[:cupsellreceipt]
		#cvendthru = params[:cvendthru]
		#cverify = params[:cverify]
		#ccustshippingstate = params[:ccustshippingstate]
		#cnoticeversion = params[:cnoticeversion]
		#ctransvendor = params[:ctransvendor]
		#crebillfrequency = params[:crebillfrequency]
		#cbfid = params[:cbfid]
		#cbf = params[:cbf]
		#cbfpath = params[:cbfpath]
		#corderlanguage = params[:corderlanguage]
		#ctaxamount = params[:ctaxamount]
		#cshippingamount = params[:cshippingamount]

		# Trans types = SALE, JV_SALE, INSF, RFND, CGBK, BILL, JV_BILL, TEST_SALE, TEST_JV_SALE, TEST_BILL, TEST_JV_BILL, TEST_RFND

		# Spawn a new User object...
		usr = User.new
		usr.password = pwd
		usr.password_confirmation = pwd
		usr.email = ccustemail
		usr.save

		# processor, trans, email, name, pwd
		#User.create!({:email => "#{ccustemail}", :password => "#{pwd}", :password_confirmation => "#{pwd}" })
		notification_processor("clickbank", "#{ctransaction}", "#{ccustemail}", "#{ccustname}", "#{pwd}", usr.id, nil, nil, nil)

		@email = ccustemail
		@password = pwd

		AccountMailer.jv_join_welcome(email, pwd).deliver

		render :nothing => true
	end

	def paypal_instant_notification

		render :nothing => true
	end

	def click2sell_instant_notification
		ccustname = params[:buyer_name] + " " + params[:buyer_surname]
		#ccuststate = params[:ccuststate]
		#ccustcc = params[:ccustcc]
		ccustemail = params[:buyer_email]
		#cproditem = params[:cproditem]
		#cprodtitle = params[:cprodtitle]
		#cprodtype = params[:cprodtype]
		ctransaction = params[:transaction_type]
		#ctransaffiliate = params[:ctransaffiliate]
		#ctransamount = params[:ctransamount]
		#ctranspaymentmethod = params[:ctranspaymentmethod]
		#ctransvendor = params[:ctransvendor]
		#ctransreceipt = params[:ctransreceipt]
		#cupsellreceipt = params[:cupsellreceipt]
		#caffitid = params[:caffitid]
		#cvendthru = params[:cvendthru]
		#cverify = params[:cverify]
		#ctranstime = params[:ctranstime]

		# Trans types: Sale, Refund, Subscription, Chargeback
		pwd = SecureRandom.hex(8)

		# Spawn a new User object...
		usr = User.new
		usr.password = pwd
		usr.password_confirmation = pwd
		usr.email = ccustemail
		usr.save

		# processor, trans, email, name, pwd
		#User.create!({:email => "#{ccustemail}", :password => "#{pwd}", :password_confirmation => "#{pwd}" })
		notification_processor("click2sell", "#{ctransaction}", "#{ccustemail}", "#{ccustname}", "#{pwd}", usr.id, nil, nil, nil)
		@email = ccustemail
		@password = pwd

		AccountMailer.jv_join_welcome(email, pwd).deliver

		respond_to do |format|
			format.html {render :jvzoo_instant_notification, :layout => false}
		end
		#render :nothing => true
		#render :html => layout: false
	end

	def spawn_user
	usr_token = Hash.new

	unless params[:email].blank?
		unless User.find_by(email: "#{params[:email]}").nil?
			pwd = SecureRandom.hex(8)

			usr = User.new
			usr.password = pwd
			usr.password_confirmation = pwd
			usr.email = params[:email]
			usr.save

			usr_token["token"] = usr.account.token
		else
			usr_token["token"] = "existing_user"
		end
	else
		usr_token["token"] = "params_failure"
	end

	render json: usr_token
	end

	def partner_integration
		# params.each do |p|
		# 	puts "Param: #{p[0]} : #{p[1]}"
		# end
		#logger.info "PID: #{params[:pid]}"
		#logger.info "Email: #{params[:email]}"
		partner = Partner.find_by(token: "#{params[:pid]}")
		#logger.info "Partner: #{partner}"
		existing = User.find_by(email: "#{params[:email]}")

		if partner.partner_type_id == 2
			friendly_product = partner.product_name
		else
			friendly_product = "Kindle Trend"
		end

		# If password has not been provided but all else has, we will simply generate one.
		#unless params[:password].blank? || params[:confirm_password].blank?
		#	pwd = params[:password]
		#else
		#	pwd = SecureRandom.hex(8)
		#end

		success_hash = Hash.new

		unless (partner.nil?) || (params[:password].blank? || params[:confirm_password].blank? || params[:email].blank? || params[:first_name].blank? || params[:last_name].blank?)
			unless existing.nil?
				exists_hash = Hash.new
				exists_hash["message"] = "That email address is already in use."

				render json: exists_hash
			else
				# begin
					unless params[:password].blank? || params[:email].blank? || params[:confirm_password].blank? || params[:first_name].blank? || params[:last_name].blank?
						usr = User.new
						usr.password = params[:password]
						usr.password_confirmation = params[:confirm_password]
						usr.email = params[:email]
						usr.save

						notification_processor("#{partner.name_as_processor}", "SALE", "#{params[:email]}", "#{params[:first_name]} #{params[:last_name]}", "#{params[:password]}", usr.id, nil, nil, partner.id)

						unless partner.partner_type_id == 2
							#AccountMailer.partner_join_welcome(usr.email, params[:password], friendly_product).deliver
							send_partner_welcome_email(usr.email.to_s, usr.password.to_s, friendly_product.to_s)

							# # If this is Chandler and James, send them a confirmation email, too.
							# if partner.token == "hj356hglkj345"
							# 	begin
							# 		sendgrid = SendGrid::Client.new do |c|
							# 			c.api_key = "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"
							# 		end

							# 		email = SendGrid::Mail.new do |m|
							# 			m.to      = "#{email}"
							# 			m.from    = "noreply@kindletrend.com"
							# 			m.subject = "#{subject}"
							# 			m.html    = "#{content}"
							# 		end

							# 			# ...and send that mail!
							# 			sendgrid.send(email)
							# 	    # mandrill = Mandrill::API.new '5U-aZ4Fp5LoaknZJUQP_ZA'
							# 	    # template_name = "welcome-educational-partners"
							# 	    # template_content = [{}]
							# 	    # message = {"from_name"=>"The Kindle Trend Team",
							# 	    #  "to"=>
							# 	    #     [{"email"=>"uberjon@gmail.com",
							# 	    #     # [{"email"=>"bookbrosinc@gmail.com",
							# 	    #         "name"=>"#{usr.email}",
							# 	    #         "type"=>"to"}],
							# 	    #  "merge_vars"=>
							# 	    #     [{ "vars" => [{ "name" => "uid", "content" => "#{usr.email}"}, {"name" => "pwd", "content" => "#{usr.password}" }, {"name" => "product_name", "content" => "#{product_name}" }],
							# 	    #         "rcpt"=>"bookbrosinc@gmail.com"}],
							# 	    #  "merge_language"=>"mailchimp",
							# 	    #  "headers"=>{"Reply-To"=>"support@kindletrend.com"},
							# 	    #  "from_email"=>"noreply@kindletrend.com"}
							# 	    # async = true
							# 	    # ip_pool = "Main Pool"
							# 	    # send_at = {}
							# 	    # result = mandrill.messages.send_template template_name, template_content, message, async, ip_pool, send_at

							# 	rescue => e
							# 	# rescue Mandrill::Error => e
							# 	    # Mandrill errors are thrown as exceptions
							# 	    puts "A mandrill error occurred: #{e.class} - #{e.message}"
							# 	    # A mandrill error occurred: Mandrill::UnknownSubaccountError - No subaccount exists with the id 'customer-123'    
							# 	    raise
							# 	end								
							# end
						end

						success_hash["status"] = "success"
						success_hash["message"] = "Done! Your account has been created."
					else
						success_hash["status"] = "failure"
						success_hash["message"] = "There's a problem with the parameters you sent."
					end
				#rescue => err
				#	success_hash["status"] = "failure"
				#	success_hash["message"] = "FAILED: #{err.message.to_s}"
				#end

				render json: success_hash
			end
		else
			success_hash["status"] = "failure"
			success_hash["message"] = "There is a problem with the parameters you sent. "
			
			if partner.nil?
				success_hash["message"].concat("There was a problem with the partner account. ")
			end

			if params[:password].blank?
			# if pwd.blank?
				success_hash["message"].concat("You didn't provide a password, and one couldn't be generated. ")
			end

			if params[:email].blank?
				success_hash["message"].concat("You didn't provide an email. ")
			end

			if params[:first_name].blank?
				success_hash["message"].concat("You need to provide a first name. ")
			end

			if params[:last_name].blank?
				success_hash["message"].concat("You need to provide a last name. ")
			end


			render json: success_hash
		end
	end

	def partner_gen_password
		#params.each do |p|
		#	puts "Param: #{p[0]} : #{p[1]}"
		#end
		partner = Partner.find_by(token: "#{params[:pid]}")
		existing = User.find_by(email: "#{params[:email]}")

		if partner.partner_type_id == 2
			friendly_product = partner.product_name
		else
			friendly_product = "Kindle Trend"
		end

		pwd = SecureRandom.hex(8)

		success_hash = Hash.new

		unless (partner.nil?) || (params[:email].blank? || params[:first_name].blank? || params[:last_name].blank?)
			unless existing.nil?
				exists_hash = Hash.new
				exists_hash["message"] = "That email address is already in use."

				render json: exists_hash
			else
				begin
					unless params[:email].blank? || params[:first_name].blank? || params[:last_name].blank?
						usr = User.new
						usr.password = pwd
						usr.password_confirmation = pwd
						usr.email = params[:email]
						usr.save

						notification_processor("#{partner.name_as_processor}", "SALE", "#{params[:email]}", "#{params[:first_name]} #{params[:last_name]}", "#{params[:password]}", usr.id, nil, nil, partner.id)

						unless partner.partner_type_id == 2
							#AccountMailer.partner_join_welcome(usr.email, params[:password], friendly_product).deliver
							send_partner_welcome_email(usr.email, pwd, friendly_product)
						end

						success_hash["status"] = "success"
						success_hash["message"] = "Done! Your account has been created."
					else
						success_hash["status"] = "failure"
						success_hash["message"] = "There is a problem with the parameters you sent."
					end
				rescue => err
					success_hash["status"] = "failure"
					success_hash["message"] = "FAILED: #{err.message.to_s}"
				end

				render json: success_hash
			end
		else
			success_hash["status"] = "failure"
			success_hash["message"] = "There is a problem with the parameters you sent."

			render json: success_hash
		end
	end

	def zaxaa
		#API signature - FVZ2GCP5171TZY17
		respond_to do |format|
			#format.html {render :jvzoo_instant_notification, :layout => false}
			format.html do
				#logger.info "Returning #{params_str}"
				#render text: params_str
				render text: 'FVZ2GCP5171TZY17'
			end

			format.json do
				render json: {api_signature: 'FVZ2GCP5171TZY17'}
			end

			format.js do
				render json: {api_signature: 'FVZ2GCP5171TZY17'}
			end
		end
	end

	def zaxaa_zpn
		#render :text => "Kindle Trend echo.", :status => 200, :content_type => 'text/html'
		trans_type = params[:trans_type]
		logger.info "trans_type = #{trans_type}"
		hash_key = params[:hash_key]
		logger.info "hash_key = #{hash_key}"
		trans_gateway = params[:trans_gateway]
		logger.info "trans_gateway = #{trans_gateway}"
		trans_date = params[:trans_date]
		logger.info "trans_date = #{trans_date}"
		trans_testmode = params[:trans_testmode]
		logger.info "trans_testmode = #{trans_testmode}"
		seller_id = params[:seller_id]
		logger.info "seller_id = #{seller_id}"
		seller_email = params[:seller_email]
		logger.info "seller_email = #{seller_email}"
		cust_email = params[:cust_email]
		logger.info "cust_email = #{cust_email}"
		cust_firstname = params[:cust_firstname]
		logger.info "cust_firstname = #{cust_firstname}"
		cust_lastname = params[:cust_lastname]
		logger.info "cust_lastname = #{cust_lastname}"
		cust_address = params[:cust_address]
		logger.info "cust_address = #{cust_address}"
		cust_state = params[:cust_state]
		logger.info "cust_state = #{cust_state}"
		cust_city = params[:cust_city]
		logger.info "cust_city = #{cust_city}"
		cust_country = params[:cust_country]
		logger.info "cust_country = #{cust_country}"


		# Trans types: SALE, BILL, RFND, CGBK, INSF, CANCEL-REBILL, UNCANCEL-REBILL
		pwd = SecureRandom.hex(8)

		# Spawn a new User object...
		usr = User.new
		usr.password = pwd
		usr.password_confirmation = pwd
		usr.email = cust_email
		usr.save

		# processor, trans, email, name, pwd, user_id, processor_id, subscription_id, partner_id
		#User.create!({:email => "#{ccustemail}", :password => "#{pwd}", :password_confirmation => "#{pwd}" })
		notification_processor("zaxaa", "#{trans_type}", "#{cust_email}", "#{cust_firstname} #{cust_lastname}", "#{pwd}", usr.id, nil, nil, nil)

		@email = cust_email
		@password = pwd

		AccountMailer.jv_join_welcome(cust_email, pwd).deliver

		# Build a response string.
		params_str = ""
		params.each do |p|
			params_str.concat("#{p} | ")
		end

		params_str.concat("6Pa0wh3qjq3_f_4")

		respond_to do |format|
			#format.html {render :jvzoo_instant_notification, :layout => false}
			format.html do
				logger.info "Returning #{params_str}"
				#render text: params_str
				render text: 'FVZ2GCP5171TZY17'
			end
		end
		#head 200, :content_type => 'text/html'
		#render :html => layout: false, :status => 200
	end

	def subscription
		# Sandbox keys...
		# Braintree::Configuration.environment = :sandbox
		# Braintree::Configuration.merchant_id = "9b95yhrshvjtxgzq"
		# Braintree::Configuration.public_key = "94s9hpxd6r2pjpcc"
		# Braintree::Configuration.private_key = "32df896e8d0b39d5249fe11680210df0"

		# Production keys...
		Braintree::Configuration.environment = :production
		Braintree::Configuration.merchant_id = "73kf9bjb57mk8k7y"
		Braintree::Configuration.public_key = "gx774gp22cvwc7jh"
		Braintree::Configuration.private_key = "0db17a2ee448b501f37d00a97a1d2ac6"

		@client_token = Braintree::ClientToken.generate
	end

	def process_subscription
		unless params.blank?
			@email = params[:email]
			@first_name = params[:first_name]
			@last_name = params[:last_name]
			pwd = params[:password]
			coupon = params[:coupon]

			# Test to see if this email is already associated with an account.
			existing_email = User.where(email: "#{@email}").count

			unless existing_email > 0
				# Sandbox keys...
				# Braintree::Configuration.environment = :sandbox
				# Braintree::Configuration.merchant_id = "9b95yhrshvjtxgzq"
				# Braintree::Configuration.public_key = "94s9hpxd6r2pjpcc"
				# Braintree::Configuration.private_key = "32df896e8d0b39d5249fe11680210df0"

				# Production keys...
				Braintree::Configuration.environment = :production
				Braintree::Configuration.merchant_id = "73kf9bjb57mk8k7y"
				Braintree::Configuration.public_key = "gx774gp22cvwc7jh"
				Braintree::Configuration.private_key = "0db17a2ee448b501f37d00a97a1d2ac6"

				customer = Braintree::Customer.create(
			    :first_name => "#{@first_name}",
			    :last_name => "#{@last_name}",
					:email => "#{@email}",
					:credit_card => {
						:payment_method_nonce => params[:payment_method_nonce]
					}
			  )

			puts "Coupon code: #{coupon}"
			  if customer.success?
					payment_method_token = customer.customer.credit_cards[0].token

					if coupon == "hollis"
						puts "So, we're going to try and discount this baby."
						subscription = Braintree::Subscription.create(
				      		:payment_method_token => payment_method_token,
				      		:plan_id => "KT_monthly_subscription",
				      			:discounts => {
				      				:add => [ { :inherited_from_id => "hollis_partnerships" } ]
				      			}
				    	)
				    else
				    	puts "Booo! No discount for you! Boo!"
			    		subscription = Braintree::Subscription.create(
				      		:payment_method_token => payment_method_token,
				      		:plan_id => "KT_monthly_subscription"
				    	)
			    	end

					if subscription.success?
						# Spawn a new User object...
						usr = User.new
						usr.password = pwd
						usr.password_confirmation = pwd
						usr.email = @email
						usr.save

						# processor, trans, email, name, pwd, usr_id, processor_id, subscription_id
						notification_processor("braintree", "SALE", "#{@email}", "#{@first_name} #{@last_name}", "#{pwd}", usr.id, customer.customer.id, subscription.subscription.id, nil)
						#
						sign_in usr

						render "welcome/index"
					else
						flash[:error] = " Whoops! There was an error processing your credit card: #{customer.message}"
						redirect_to action: "subscription"#, error:  "<h3>Whoops!</h3><p>There was an error processing your credit card: #{customer.message}</p>."
					end
			  else
					flash[:error] = "Whoops! There was an error processing your credit card: #{customer.message}"
			    redirect_to action: "subscription"#, error: "<h3>Whoops!</h3><p>There was an error processing your credit card: #{customer.message}</p>."
			  end
			else
				flash[:error] = "There is already an account with that email address."
				redirect_to action: "subscription"
			end
			else
				flash[:error] = "Please make sure to provide the required data."
				redirect_to action: "subscription"#, error: "Please make sure to provide the required data."
			end
	end

	def cancel_subscription
		# Sandbox keys...
		# Braintree::Configuration.environment = :sandbox
		# Braintree::Configuration.merchant_id = "9b95yhrshvjtxgzq"
		# Braintree::Configuration.public_key = "94s9hpxd6r2pjpcc"
		# Braintree::Configuration.private_key = "32df896e8d0b39d5249fe11680210df0"

		# Production keys...
		Braintree::Configuration.environment = :production
		Braintree::Configuration.merchant_id = "73kf9bjb57mk8k7y"
		Braintree::Configuration.public_key = "gx774gp22cvwc7jh"
		Braintree::Configuration.private_key = "0db17a2ee448b501f37d00a97a1d2ac6"

		unless current_user.blank?
			usr = User.find(current_user.id)
			acct = usr.account

			# Change account status to 'cancelled, removing user access.'
			acct.status = "cancelled"
			acct.save

			# Delete the subscription at the payment processor.
			Braintree::Subscription.cancel(acct.subscription_id)

			# Log user out and redirect to root.
			sign_out(current_user)
			flash[:warning] = "Your account has been permanently cancelled. You will not receive further charges after the current billing cycle."
			redirect_to :root
		end
	end

	def verify_coupon
		# Hardcoding for now - eventually this will need to be more robust.
		#coupons = [{"code": "", "amount": 0}]
		valid = false

		if params[:coupon] == "hollis"
			valid = true
		end

		render json: { valid: valid }
	end

	def send_partner_welcome_email(email, password, product_name)
		# content = <<-CONTENT
			content = "<!DOCTYPE html><html><head><meta content=%Q(text/html; charset=UTF-8) http-equiv=%Q(Content-Type) /></head><body><p>Welcome to #{product_name}!</p><p>#{product_name} gives you the tools to not just see how your preferred niche is served, but to explore how many books are available in the many, many other underserved niches, equipping you to find profitable parallels.</p><p>To get started, visit <a href=%Q(https://www.kindletrend.com/app/) target=%Q(_BLANK)>https://www.kindletrend.com/app</a>, and login with the following credentials:<br/><strong>email:</strong> #{email}<br/><strong>password:</strong> #{password}<br/><br/>We suggest that you change your password after you login for the first time. You can do so by clicking the %Q(Edit Profile) link, located next to your email address on the right side of the #{product_name} menu bar.</p><p>Thanks for joining #{product_name}! If you have any questions or comments, please use our <a href=%Q(http://www.kindletrend.com/contact-us/) target=%Q(_BLANK)>contact form</a> and let us know!</p><p>You're awesome!</p><p>Sincerely,</p>The #{product_name} Team</body></html>"
		# CONTENT

		subject = "Welcome to #{product_name}!"

		# begin
			AccountMailer.partner_join_welcome(email, password, product_name).deliver
			# sg = SendGrid::API.new(api_key: "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")
			# response = sg.client.mail._('send').post(request_body: mail.to_json)
			# puts response.status_code
			# puts response.body
			# puts response.headers
		    
		# rescue => e
		#     # Mandrill errors are thrown as exceptions
		#     puts "A mailing error occurred: #{e.class} - #{e.message}"
		#     # A mandrill error occurred: Mandrill::UnknownSubaccountError - No subaccount exists with the id 'customer-123'
		#     raise
		# end
	end
end
