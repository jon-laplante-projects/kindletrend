class ApplicationController < ActionController::Base
  force_ssl unless Rails.env.development?

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  WillPaginate.per_page = 100

  before_action :scope_to_partner, :partner_login

  def scope_to_partner
  	unless current_user.blank? || current_user.account.partner.blank?
	  	partner = Partner.find(current_user.account.partner)

	  	if partner.partner_type_id == 2
	  		@friendly_product = "#{partner.product_name}"
	  	else
	  		@friendly_product = "Kindle Trend"
	  	end
	else
		@friendly_product = "Kindle Trend"
  	end
  end

  def partner_login
  	unless params[:bpid].blank?
  		partner = Partner.find_by(token: "#{params[:bpid]}")
  		if partner.partner_type_id == 2
	  		@friendly_product = "#{partner.product_name}"
	  	else
	  		@friendly_product = "Kindle Trend"
	  	end
  	end
  end

  def earningsEstimator(rank, price, filesize=1, operation)
		daily_sales = salesEstimator(rank, operation)

		# Client.average("orders_count")
		# Client.minimum("age")
		# Client.maximum("age")

		# Based on price, determine commissions.
		# If royalty is 70%, delivery rate for Amazon.com US is $0.15/MB (no charge for 35% royalty)
		#
		# USD List Price Requirements for Amazon.com  				Minimum 		Maximum 
		# 35 % Royalty Option 										List Price 		List Price
   		# • Less than or equal to 3 megabytes 						$ 0.99 			$ 200.00
   		# • Greater than 3 megabytes and less than 10 megabytes 	$ 1.99 			$ 200.00
   		# • 10 megabytes or greater 								$ 2.99 			$ 200.00
		# 70 % Royalty Option 										$ 2.99 			$ 9.99

		if price > 2.99 && price < 9.99
			royalty = 0.7
		else
			# Royalty will be 35%
			royalty = 0.35
		end

		delivery = 0
		delivery = 0.15 * filesize/1024 if royalty == 0.7

		return ((daily_sales * 30) * (royalty * price)) - (delivery * daily_sales)
	end

	def salesEstimator(rank, operation)
		# Estimate the number of sales.
		# From http://www.theresaragan.com/p/sale-ranking-chart.html:::
		#
		# Amazon Best Seller Rank 50,000 to 100,000 - selling close to 1 book a day.
		# Amazon Best Seller Rank 10,000 to 50,000 - selling 5 to 15 books a day.
		# Amazon Best Seller Rank 5,500 to 10,000 - selling 15 to 25 books a day.
		# Amazon Best Seller Rank 3,000 to 5,500 - selling 25 to 70 books a day.
		# Amazon Best Seller Rank 1,500 to 3,000 - selling 70 to 100 books a day.
		# Amazon Best Seller Rank 750 to 1,500 - selling 100 to 120 books a day.
		# Amazon Best Seller Rank 500 to 750 - selling 120 to 175 books a day.
		# Amazon Best Seller Rank 350 to 500 - selling 175 to 250 books a day.
		# Amazon Best Seller Rank 200 to 350 - selling 250 to 500 books a day.
		# Amazon Best Seller Rank 35 to 200 - selling 500 to 2,000 books a day.
		# Amazon Best Seller Rank 20 to 35 - selling 2,000 to 3,000 books a day.
		# Amazon Best Seller Rank of 5 to 20 - selling 3,000 to 4,000 books a day.
		# Amazon Best Seller Rank of 1 to 5 - selling 4,000+ books a day.
		unless rank.nil?
			if rank <= 200000
				case rank
				when 100000...200000
					daily_sales = 0.034
				when 50000...100000
					daily_sales = 1
				when 10000...50000
					case operation
						when "mean"
							daily_sales = 12
						when "median"
							daily_sales = 10
						when "ceiling"
							daily_sales = 15
						when "floor"
							daily_sales = 5
					end
				when 5500...10000
					case operation
						when "mean"
							daily_sales = 22
						when "median"
							daily_sales = 20
						when "ceiling"
							daily_sales = 25
						when "floor"
							daily_sales = 15
					end
				when 3000...5500
					case operation
						when "mean"
							daily_sales = 62
						when "median"
							daily_sales = 57
						when "ceiling"
							daily_sales = 70
						when "floor"
							daily_sales = 25
					end
				when 1500...3000
					case operation
						when "mean"
							daily_sales = 88
						when "median"
							daily_sales = 85
						when "ceiling"
							daily_sales = 100
						when "floor"
							daily_sales = 70
					end
				when 750...1500
					case operation
						when "mean"
							daily_sales = 113
						when "median"
							daily_sales = 110
						when "ceiling"
							daily_sales = 120
						when "floor"
							daily_sales = 100
					end
				when 500...750
					case operation
						when "mean"
							daily_sales = 150
						when "median"
							daily_sales = 145
						when "ceiling"
							daily_sales = 175
						when "floor"
							daily_sales = 120
					end
				when 350...500
					case operation
						when "mean"
							daily_sales = 210
						when "median"
							daily_sales = 200
						when "ceiling"
							daily_sales = 250
						when "floor"
							daily_sales = 175
					end
				when 200...350
					case operation
						when "mean"
							daily_sales = 400
						when "median"
							daily_sales = 375
						when "ceiling"
							daily_sales = 500
						when "floor"
							daily_sales = 250
					end
				when 35...200
					case operation
						when "mean"
							daily_sales = 1300
						when "median"
							daily_sales = 1250
						when "ceiling"
							daily_sales = 2000
						when "floor"
							daily_sales = 500
					end
				when 20...35
					case operation
						when "mean"
							daily_sales = 2600
						when "median"
							daily_sales = 2500
						when "ceiling"
							daily_sales = 3000
						when "floor"
							daily_sales = 2000
					end
				when 5...20
					case operation
						when "mean"
							daily_sales = 3650
						when "median"
							daily_sales = 3500
						when "ceiling"
							daily_sales = 4000
						when "floor"
							daily_sales = 3000
					end
				when 1...5
					case operation
						when "mean"
							daily_sales = 8000
						when "median"
							daily_sales = 7500
						when "ceiling"
							daily_sales = 10000
						when "floor"
							daily_sales = 4000
					end
				end
			else
				daily_sales = 0
			end

			return daily_sales
		else
			return 0
		end
	end

	def median(num_arr)
		if num_arr.length > 0
			num_arr.reject!(&:nil?)
			sorted = num_arr.sort
			len = sorted.length
			return (sorted[(len - 1) / 2] + sorted[len / 2]) / 2.0
		else
			return 0
		end
	end

	def keywords(id, search_type)
	    bestseller = Bestseller.find(id)
	    ebooks = bestseller.ebooks

	    keyword_str = Array.new

	    ebooks.each do |e|
	      if search_type == "title"
	        keyword_str << e.name
	      else
	        keyword_str << e.desc
	      end
	    end

	    keyword_str = keyword_str.join(" ")

	    keywords = "#{keyword_str}".downcase.gsub(/[^a-z\s]/, '').split(" ")
	    # Build stop word list...
	    stopwords = ["a","and","about","an","are","as","at","be","by","com","for","from","in","is","it","of","on","or","that","the","this","to","was","will","with","the","youll","youre","so","its","if"]
	    keywords.delete_if{ |k| stopwords.include?(k) }
	    counts = Hash.new(0)
	    keywords.each { |keyword| counts[keyword] += 1 }
	    
	    #@titlewords.flatten.each do |t|
	      #   sorted = t.sort_by {|key, value| -value}
	      #   sorted.each do |word|
	      #     word
	      #   end
	      #end

	    return counts
	end
end