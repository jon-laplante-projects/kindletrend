class OneclickAdminController < ApplicationController
	before_action :authenticate_user!

	layout :set_layout

	def set_layout
		"layouts/application"

		if current_user.account.partner.present? && current_user.account.partner.partner_type_id == 2
		  "layouts/blank"
		end
	end
	
	def mapper
		cat_group = CategoryGroup.where(complete: true).last
		@categories = cat_group.categories.where("ancestry NOT LIKE '%/%'")
	end

	def parse_form
		unless params[:form_url].blank?
			form_elements = Array.new
			@friendlies = Array.new
			el = Hash.new
			content = get_page("#{params[:form_url]}")
			
			page = Nokogiri::HTML(content.content)

			if content.iframes.empty?
				el["scraped_form"] = parse_page(content)

				# Get form page data (i.e. Page title, page url, submission url, submission type)
				el["page_data"] = page_data(content)
			else
				# There are times when the iframe has nothing to do with the form...
				# REVISIT - THIS IS NOT THE BEST WAY TO HANDLE THIS CASE!!!
				begin
					frame_page = content.iframes.first.click
					el["scraped_form"] = parse_page(frame_page.content)

					# Get form page data (i.e. Page title, page url, submission url, submission type)
					el["page_data"] = page_data(frame_page.content)
				rescue
					el["scraped_form"] = parse_page(content)

					# Get form page data (i.e. Page title, page url, submission url, submission type)
					el["page_data"] = page_data(content)
				end
			end

			# Get the friendly field names.
			# TODO: Match friendly field names to form element data type.

			friendlies = FriendlyName.all.select(:name)
			puts "#{FriendlyName.all.select(:name).to_sql}"
			if friendlies.count > 0
				@friendlies << friendlies
			end

			el["friendly_names"] = @friendlies

			form_elements << el

			# Fields, Friendly, Type, Values, Required, Ignore
			# name, type, value, class, id, aria-label, aria-required, title

			puts "RETURNING:\n#{form_elements.to_json}"
			render json: form_elements

		else
			render :nothing
		end

	end

	def get_page(url)
		# Randomize the browser displayed to the server.
		rand_browser = 1 + rand(7)
		case(rand_browser)
			when 1 then agent_alias = 'Mac Firefox'
			when 2 then agent_alias = 'Mac Mozilla'
			when 3 then agent_alias = 'Windows Chrome'
			when 4 then agent_alias = 'Windows IE 8'
			when 5 then agent_alias = 'Windows IE 9'
			when 6 then agent_alias = 'Windows Mozilla'
			when 7 then agent_alias = 'Linux Firefox'
		end

		# Create our Mechanize object, using the random browser.
		user_agent = Mechanize.new {|agent| agent.user_agent_alias = "#{agent_alias}"}

		# Get the page.
		user_agent.get(url)
	end

	def page_data(content)
		# Get form page data (i.e. Page title, page url, submission url, submission type)
		data = Array.new
		node = Hash.new

		page = Nokogiri::HTML(content.content.to_s)

		node["page_title"] = page.xpath("//title/text()").to_s.gsub("\n", "")
		node["page_url"] = params[:form_url]
		node["submission_url"] = page.xpath("//form/@action").to_s
		node["submission_method"] = page.xpath("//form/@method").to_s
		
		data << node

		return data
	end

	def parse_page(content)
		#@elements = Array.new
		@form_elements = Array.new

		# Create a Nokogiri object from the retrieved page called, appropriately, "page".
		page = Nokogiri::HTML(content.content.to_s)

		puts "HTML:\n==================\n#{page.to_s}"

		# Extract form elements, input elements, labels, paragraphs, divs, and spans from the retrieved HTML.
		form_elements = page.xpath('//form')
		#form_elements = content.form
		#mech_form_elements = content.form
		@inputs = page.xpath('//input')
		#@labels = form_elements.xpath('label')
		@ps = page.xpath('//p')
		@divs = page.xpath('//div')
		@spans = page.xpath('//span')

		# Parse form <input />
		########################################
		## Detect LIST attribute in <input /> ##
		## and parse related <datalist />	  ##
		## where it is present.				  ##
		########################################

		form_elements.each do |form|			
			inputs = form.xpath("//input")

			inputs.each do |input|
				# Fields, Friendly, Type, Values, Required, Ignore
				# name, type, value, class, id, aria-label, aria-required, title
				element = Hash.new
				friendly = Hash.new

				#element["name"] = input["name"].to_s.strip
				element["name"] = input["id"].to_s.strip
				element["type"] = input["type"].to_s.strip
				element["value"] = input["value"].to_s.strip
				element["class"] = input["class"]
				element["id"] = input["id"].to_s.strip
				element["aria-label"] = input["aria-label"].to_s.strip
				element["aria-required"] = input["aria-required"]
				element["title"] = input["title"].to_s.strip

				@form_elements << element

				# If there aren't aria-labels, check for <label /> tags.
				lbl = form.xpath("//label[@for='#{element["id"]}']/text()").to_s.strip

				if element["aria-label"].blank? && !lbl.blank?
					element["aria-label"] = lbl.to_s.strip
				elsif !element["aria-label"].blank? && !lbl.blank?
					friendly["name"] = lbl.to_s.strip
					#friendly["field_type"] = element["type"].to_s.strip

					@friendlies << friendly
				end
			end

			# Parse form <textarea />
			textareas = form.xpath("//textarea")
			textareas.each do |ta|

				element = Hash.new

				element["name"] = ta["name"].to_s.strip
				element["type"] = "textarea"
				element["value"] = ta["value"].to_s.strip
				element["class"] = ta["class"]
				element["id"] = ta["id"].to_s.strip
				element["aria-label"] = ta["aria-label"].to_s.strip
				element["aria-required"] = ta["aria-required"]
				element["title"] = ta["title"].to_s.strip

				@form_elements << element

				# If there aren't aria-labels, check for <label /> tags.
				lbl = form.xpath("//label[@for='#{element["id"]}']/text()").to_s.strip

				if element["aria-label"].blank? && !lbl.blank?
					element["aria-label"] = lbl.to_s.strip
				elsif !element["aria-label"].blank? && !lbl.blank?
					friendly["name"] = lbl.to_s.strip
					friendly["field_type"] = element["type"].to_s.strip

					@friendlies << friendly
				end
			end

			# Parse form <select />
			# NEED TO PARSE <OPTION /> AS WELL!!!
			selects = form.xpath("//select")
			selects.each do |sel|

				element = Hash.new
				element["options"] = Array.new

				element["name"] = sel["name"].to_s.strip
				element["type"] = "select"
				element["value"] = sel["value"].to_s.strip
				element["class"] = sel["class"]
				element["id"] = sel["id"].to_s.strip
				element["aria-label"] = sel["aria-label"].to_s.strip
				element["aria-required"] = sel["aria-required"]
				element["title"] = sel["title"].to_s.strip

				# Parse the <option /> tags associated with this <select />...
				options = sel.xpath("option")
				options.each do |opt|
					option = Hash.new

					option["text"] = opt.text().to_s.strip
					option["value"] = opt["value"]

					element["options"] << option
				end

				@form_elements << element

				# If there aren't aria-labels, check for <label /> tags.
				lbl = form.xpath("//label[@for='#{element["id"]}']/text()").to_s.strip

				if element["aria-label"].blank? && !lbl.blank?
					element["aria-label"] = lbl.to_s.strip
				elsif !element["aria-label"].blank? && !lbl.blank?
					friendly["name"] = lbl.to_s.strip
					friendly["field_type"] = element["type"].to_s.strip

					@friendlies << friendly
				end
			end
		end

		return @form_elements
	end

	def save_form_data
		@form = Form.new
		@form.name = params[:form_name]
		@form.form_url = params[:url]
		@form.submission_url = params[:submission_url]
		@form.submission_type = params[:submission_method]
		@form.captcha = params[:captcha]
		@form.free_ebooks = params[:free_ebooks]
		@form.save

		params[:categories].each do |c|
			categories = CategoryForm.new
			categories.form_id = @form.id
			categories.category_id = c.to_i
			categories.save
		end

		puts "Fields count: #{params[:fields].count}"
		params[:fields].each do |f|
			@friendly = FriendlyName.where("name LIKE '#{f["friendly_name"]}'").first

			unless f.blank?
				if @friendly.blank?
					@friendly = FriendlyName.new
					@friendly.name = f["friendly_name"]
					@friendly.field_type = f["field_type"]
					@friendly.field_name = f["friendly_name"].downcase.gsub(" ", "_")
					@friendly.save
				end

				@field = Field.new
				@field.name = f["id"]
				@field.form_id = @form.id
				puts "#{@friendly.to_s}"
				@field.friendly_name_id = @friendly.id.to_i
				@field.field_type = f["field_type"]
				@field.require_field = f["is_required"]
				@field.ignore_field = f["is_ignored"]
				@field.save

				unless f[:field_values].blank?
					f[:field_values].each do |v|
						field_vals = FieldValue.new
						field_vals.field_id = @field.id
						field_vals.preset_field_value = v["text"]
						field_vals.save
					end
				end
			end
		end

		render :nothing
	end
end