class ControlPanelController < ApplicationController
	skip_before_action :verify_authenticity_token
	before_action :authenticate_user!
	authorize_resource :class => false

	layout :set_layout

	def set_layout
		"layouts/application"

		if current_user.account.partner.present? && current_user.account.partner.partner_type_id == 2
		  "layouts/blank"
		end
	end

	def system_admin

	end

	def watches_admin
		#params[:category]
		@watches_arr = Array.new
		@parent_watches_arr = Array.new

		for w in 1..30 do
			cat_group = CategoryGroup.last
			watch = cat_group.categories.where(watch: w).count
			@watches_arr[w] = watch
		end

		for w in 100..130 do
			cat_group = CategoryGroup.last
			watch = cat_group.categories.where(watch: w).count
			@parent_watches_arr[w] = watch
		end

		@top = []
	    cat_group = CategoryGroup.where(complete: true).last
	    first_cat = cat_group.categories.first
	    top_categories = Category.where(:ancestry => "#{first_cat.id}")
	    top_categories.each do |t|
	      categories = {}
	      children = cat_group.categories.find(t.id).children
	      categories["root"] = first_cat.id
	      categories["id"] = t.id
	      categories["name"] = t.name
	      categories["category_count"] = t.count
	      categories["child_count"] = t.child_ids.count
	      categories["watch"] = t.watch
	      @top << categories
	    end
	    #@niches = Bestseller.select(:category_id).distinct.pluck(:category_id)
	    #####################################################
	    @niches = cat_group.categories.where("watch IS NOT NULL").pluck(:id)
	    #####################################################
	    #@watched = Category.where(:watch => true).pluck(:id)
	    if params[:a]
	      @category = Category.find(params[:a])
	    end

	    @children = Category.where(:ancestry => "#{params[:r]}/#{params[:a]}")
	end

	def watches_update
		params.each do |w|
			unless w[0] == "controller" || w[0] == "action"
				logger.info "Cat_id: #{w}"
				cat = Category.find(w[0])
				cat.watch = w[1]
				cat.save
			end
		end

		redirect_to action: "watches_admin"
	end

	def users_report
		if params[:start_month].blank? || params[:current_month_toggle] == "on"
			@month = Date::today.month
			@year = Date::today.year
			
			if current_user.role == "partner_admin"
				@users = User.includes(:account).where("MONTH(users.created_at) = ? AND YEAR(users.created_at) = ? AND accounts.exempt IS NULL AND accounts.admin IS NULL AND accounts.partner_id = #{current_user.account.partner_id}", @month, @year)
				@exemptions = User.includes(:account).where("MONTH(users.created_at) = ? AND YEAR(users.created_at) = ? AND accounts.exempt IS NOT NULL AND accounts.admin IS NULL AND accounts.partner_id = #{current_user.account.partner_id}", @month, @year)
				@admins = User.includes(:account).where("MONTH(users.created_at) = ? AND YEAR(users.created_at) = ? AND accounts.exempt IS NULL AND accounts.admin IS NOT NULL AND accounts.partner_id = #{current_user.account.partner_id}", @month, @year)
			else
				@users = User.includes(:account).where("MONTH(users.created_at) = ? AND YEAR(users.created_at) = ? AND accounts.exempt IS NULL AND accounts.admin IS NULL", @month, @year)
				@exemptions = User.includes(:account).where("MONTH(users.created_at) = ? AND YEAR(users.created_at) = ? AND accounts.exempt IS NOT NULL AND accounts.admin IS NULL", @month, @year)
				@admins = User.includes(:account).where("MONTH(users.created_at) = ? AND YEAR(users.created_at) = ? AND accounts.exempt IS NULL AND accounts.admin IS NOT NULL", @month, @year)
			end
		else
			@start_date = DateTime.parse("#{params[:start_year]}-#{params[:start_month]}-1")
			@end_date = Date.civil(params[:end_year].to_i, params[:end_month].to_i, -1)

			if current_user.role == "partner_admin"
				@users = User.includes(:account).where("users.created_at BETWEEN ? AND ? AND accounts.exempt IS NULL AND accounts.admin IS NULL AND accounts.partner_id = #{current_user.account.partner_id}", @start_date, @end_date)
				@exemptions = User.includes(:account).where("users.created_at BETWEEN ? AND ? AND accounts.exempt IS NOT NULL AND accounts.admin IS NULL AND accounts.partner_id = #{current_user.account.partner_id}", @start_date, @end_date)
				@admins = User.includes(:account).where("users.created_at BETWEEN ? AND ? AND accounts.exempt IS NULL AND accounts.admin IS NOT NULL AND accounts.partner_id = #{current_user.account.partner_id}", @start_date, @end_date)
			else
				@users = User.includes(:account).where("users.created_at BETWEEN ? AND ? AND accounts.exempt IS NULL AND accounts.admin IS NULL", @start_date, @end_date)
				@exemptions = User.includes(:account).where("users.created_at BETWEEN ? AND ? AND accounts.exempt IS NOT NULL AND accounts.admin IS NULL", @start_date, @end_date)
				@admins = User.includes(:account).where("users.created_at BETWEEN ? AND ? AND accounts.exempt IS NULL AND accounts.admin IS NOT NULL", @start_date, @end_date)
			end
		end
	end

	def user_update
		user = User.find(params[:user_id])

		user.email = params[:email]
		user.save

		user.account.status = "#{params[:user_status]}"
		user.account.save

		case "#{params[:user_entitlements]}"
			when "none"
				user.account.admin = nil
				user.account.exempt = nil
			when "admin"
				user.account.admin = true
				user.account.exempt = nil
			when "exempt"
				user.account.admin = nil
				user.account.exempt = true
		end

		user.account.save

		redirect_to action: "users_report"
	end

	def white_label_admin
		@partner = current_user.account.partner
	end

	def white_label_edit_account
		@partner = current_user.account.partner
	end

	def white_label_logout
		@partner = current_user.account.partner
	end

	def white_label_update
		partner = Partner.find(params[:partner_id])

		partner.name = "#{params[:company_name]}"
		partner.product_name = "#{params[:product_name]}"
		partner.support_email = "#{params[:support_email]}"
		partner.app_style = "#{params[:kindletrend_styles]}"
		case params[:kt_supported]
			when "email"
				partner.kt_supported = true
			when "form"
				partner.kt_supported = false
		end
		partner.save

		flash[:success] = "Changes to your white label account have been saved."

		redirect_to action: "white_label_admin"
	end
end