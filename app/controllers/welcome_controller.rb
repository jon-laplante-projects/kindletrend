class WelcomeController < ApplicationController
	before_action :authenticate_user!, :except => [:calcs, :mission]

	layout :set_layout

	def set_layout
		"layouts/application"

		if current_user.account.partner.present? && current_user.account.partner.partner_type_id == 2
		  "layouts/blank"
		end
	end

	def index
		
	end

	def calcs

	end

	def mission

	end
end