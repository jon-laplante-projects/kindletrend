require 'open-uri'

class ParserController < ApplicationController
  skip_before_action :verify_authenticity_token

  def get_ebook_stubs(bestseller_id) #Internal
    if bestseller_id.blank?
      bestseller_list = Bestseller.where(complete: false).first
    else
      bestseller_list = Bestseller.find(bestseller_id)
    end

    cat = Category.find(bestseller_list.category_id)

    url_id = cat.asin

    link_arr = Array.new
    for x in (5).downto(1)
      rand_browser = 1 + rand(7)
      case(rand_browser)
        when 1 then agent_alias = 'Mac Firefox'
        when 2 then agent_alias = 'Mac Mozilla'
        when 3 then agent_alias = 'Windows Chrome'
        when 4 then agent_alias = 'Windows IE 8'
        when 5 then agent_alias = 'Windows IE 9'
        when 6 then agent_alias = 'Windows Mozilla'
        when 7 then agent_alias = 'Linux Firefox'
      end

      logger.info "get_ebook_stubs agent_alias: #{agent_alias}"
      logger.info "get_ebook_stubs url_id: #{url_id}"
      user_agent = Mechanize.new {|agent| agent.user_agent_alias = "#{agent_alias}"}

      list_link = "http://www.amazon.com/Best-Sellers-Kindle-Store/zgbs/digital-text/#{url_id}?_encoding=UTF8&pg=#{x}"
      page_data = ""
      user_agent.get(list_link) { |d| page_data = d.content }

      page = Nokogiri::HTML(page_data)

      @bestseller_count = if page.xpath("//a[@page='5']") == ""
        if page.xpath("//a[@page='4']") == ""
          if page.xpath("//a[@page='3']") == ""
            if page.xpath("//a[@page='2']") == ""
              page.xpath("//a[@page='1']").text.to_s.split("-")[1]
            else
              page.xpath("//a[@page='2']").text.to_s.split("-")[1]
            end
          else
            page.xpath("//a[@page='3']").text.to_s.split("-")[1]
          end
        else
          page.xpath("//a[@page='4']").text.to_s.split("-")[1]
        end
      else
        page.xpath("//a[@page='5']").text.to_s.split("-")[1]
      end

      puts "bestseller_count: #{@bestseller_count}"

      #############################################
      ## Revisit and add support for categories  ##
      ## with an odd number of bestsellers (i.e. ##
      ## 84, for instance.)                      ##
      #############################################
      case x
        when 5
          item_num = 100
        when 4
          item_num = 80
        when 3
          item_num = 60
        when 2
          item_num = 40
        when 1
          item_num = 20
      end

      unless x == 1
        begin
        link_hash = Hash.new
        item = page.xpath("//span[@class='zg_rankNumber'][contains(text(),'#{item_num}.')]/../../div/div[2]/a")
        i = item.xpath('@href').text.strip!
        i = i.gsub("\n", "")
        link_hash["url"] = "#{i}"
        link_hash["rank"] = "#{item_num}"

        link_arr << link_hash

        rescue Exception => e
          #log_err(e)
          link_hash["url"] = nil
          link_hash["rank"] = "#{item_num}"
          logger.info "#{e.message}\n"
          logger.info "page.to_s"
        end
      else
        for y in 1..20
          link_hash = Hash.new
          item = page.xpath("//span[@class='zg_rankNumber'][.='#{y}.']/../../div/div[2]/a")
          i = item.xpath('@href').text.strip!
          unless i.blank?
            i = i.gsub("\n", "")
            link_hash["url"] = "#{i}"
            link_hash["rank"] = "#{y}"

            link_arr << link_hash
          end
        end

        logger.info "link_arr = #{link_arr.to_s}"
        logger.info "bestseller_id: #{bestseller_list.id}"

        link_arr.each do |a|
          ebook_test = bestseller_list.ebooks.find_by(bestseller_id: bestseller_list.id, rank: a["rank"])

          if ebook_test.blank?
            ebook = bestseller_list.ebooks.new
            ebook.bestseller_id = bestseller_list.id
            ebook.url = "#{a['url']}"
            ebook.rank = a["rank"]
            ebook.save
          end
        end
      end
    end
  end

  def assign_category
    # If category is completely processed, COMPLETE the bestseller rec and set up the next category.
    ebook_count = Ebook.where(name: nil, assigned: nil).count
    if ebook_count == 0
      # There's not an ebook stub, let's look for a bestseller that's incomplete.
      bestsellers = Bestseller.where(complete: false).order(:id)

      unless bestsellers.count < 1
        #watch_num = bestsellers.categories.find_by(bestsellers[0].category_id).pluck(:watch)
        get_ebook_stubs(nil)
      end
    end

    ebook = Ebook.select(:url, :bestseller_id, :id).where(name: nil, assigned: nil).first

    unless ebook.blank?
      render json: ebook

      ebook.assigned = true
      ebook.save
    else
      return_empty = Hash.new
      return_empty["id"] = nil
      #render :nothing => true
      render json: return_empty
    end
  end

  def parse_page
    # If category is completely processed, COMPLETE the bestseller rec and set up the next category.
    
    page_html = URI::decode(params[:page_html])
    @page = page("#{page_html}")
    @bestseller_id = params[:bestseller_id]
    save_ebook(params[:ebook_id])
    logger.info "parse_page book_id: #{params[:ebook_id]}"

    response_hash = Hash.new

    unparsed = Ebook.where("bestseller_id = #{@bestseller_id} AND name IS NULL").count

    if unparsed == 0
      bestseller = Bestseller.find(@bestseller_id)
      bestseller.complete = true
      bestseller.save
      #response_hash["directive"] = "continue"
    #else
      #response_hash["directive"] = "halt"
    end

    render json: response_hash
  end

  def page(page_html)
    Nokogiri::HTML(page_html.to_s)
  end

  def title
    begin
      @page.xpath("//span[@id='btAsinTitle']/text()").to_s.strip.encode("ISO-8859-1").force_encoding("UTF-8").scrub
    rescue
      @page.xpath("//span[@id='btAsinTitle']/text()").to_s.strip
    ensure
      unless @page.xpath("//span[@id='btAsinTitle']/text()").blank?
        @page.xpath("//span[@id='btAsinTitle']/text()").to_s.strip
      else
        puts "................................."
        puts "................................."
        puts "................................."
        puts @page.to_s
        puts "................................."
        puts "................................."
        puts "................................."
        nil
      end
    end
  end

  def snippet
    @page.xpath("li[@id='SalesRank']")
  end

  def code
    Nokogiri::HTML("#{snippet}")
  end

  def ranks
    ranks_arr = Array.new
    bestseller = Bestseller.find(@bestseller_id)

    primary_rank = @page.xpath('//li[@id="SalesRank"]/text()').to_s.strip

    unless primary_rank.blank?
      primary_rank = primary_rank[0, primary_rank.length - 24]
      primary_rank = primary_rank[1, primary_rank.length - 1]
      primary_rank = primary_rank.delete(",").to_i

      primary_hash = Hash.new
      primary_hash["rank"] = "#{primary_rank}"
      # Get the root of the current category group.
      cat_root = Category.where(category_group_id: bestseller.category_group_id).first
      primary_hash["category"] = cat_root.id
      ranks_arr << primary_hash
    end

    ranks = @page.xpath('//li[@id="SalesRank"]/ul/li')

    ranks.each do |r|
      other_ranks = Hash.new

      rank = r.xpath("span")
      rank_ladder = rank[1].text.to_s.split(" > ")
      if "#{rank_ladder[0]}" == "in Kindle Store"
        rank_str = rank[0].text
        other_ranks["rank"] = rank_str[1, rank_str.length - 1]

        rank_ladder.shift

        cat_group = CategoryGroup.where(complete: true).last
        current_root = Category.where(category_group_id: cat_group.id).first
        ancestry = current_root.id.to_s
        for x in 0..rank_ladder.length - 1
          if rank_ladder[x] != "Kindle eBooks"
            rung = Category.where(:ancestry => "#{ancestry}", :name => "#{rank_ladder[x]}").first
            unless rung.blank?
              if x != rank_ladder.length - 1
                ancestry << "/#{rung.id.to_s}"
              else
                if rung
                  other_ranks["category"] = rung.id
                else
                  c = Category.new
                  c.parent = ancestry.split("/").pop
                  c.name = "#{rank_ladder[x]}"
                  other_ranks["category"] = rung.id
                end
              end
            end
          end
        end

        ranks_arr << other_ranks
      end
    end

    return ranks_arr
  end

  def rating
    rating = @page.xpath("//*[contains(text(),'out of')]/text()").first.to_s

    unless rating.blank?
      rating[0, rating.length - 15].to_f
    else
      nil
    end
  end

  def reviews
    review_count = @page.xpath("//*[contains(text(),'customer review')]/text()").first.to_s
    unless review_count.blank?
      review_count = review_count.to_s
      review_count = review_count[0, review_count.length - 17]
      review_count = review_count.delete(",")
    else
      review_count = 0
    end

    review_count.to_i
  end

  def authors
    writers = Array.new

    if @page.xpath("//span[@class='contributorNameTrigger']/a").count > 0
      author_list = @page.xpath("//span[@class='contributorNameTrigger']/a")

      author_list.each do |a|
        auth_hash = Hash.new
        unless a.text.blank?
          begin
            auth_hash["name"] = a.text.encode("ISO-8859-1").force_encoding("UTF-8").scrub
          rescue
            auth_hash["name"] = a.text
          end
          author = a.text.split(" ")
          auth_hash["gender"] = gender(author[0])
          auth_hash["authorcentral"] = authorcentral(auth_hash["name"]) #?  : nil
          writers << auth_hash
        end
      end
    else
      auth_arr = @page.xpath("//h1[contains(concat(' ',@class,' '), 'parseasinTitle')]/following-sibling::span/a")
      if auth_arr.count > 0
        auth_arr.each do |a|
          auth_hash = Hash.new
          #auth_hash["name"] = @page.xpath("//h1[contains(concat(' ',@class,' '), 'parseasinTitle')]/following-sibling::span/a").text
          auth_hash["name"] = "#{a.text}"
          author = auth_hash["name"].split(" ")
          titles = ["Dr.","Mr.","Mrs.","Miss","Ms.","Representative","Senator","Speaker","President","Councillor","Alderman","Delegate","Mayor","Governor","Prefect","Prelate","Premier","Burgess","Ambassador","Envoy","Secretary","Attaché","Provost","Prince","Princess","Archduke","Archduchess","Earl","Viscount","Lord","Emperor","Empress","Tsar","Tsarina","Tsaritsa","King","Pope","Bishop","Father","Pastor","Cardinal","Colonel","General","Commodore","Corporal","Mate","Sergeant","Admiral","Brigadier","Captain","Commander","Officer","Lieutenant","Major","Private","Doctor"]
          unless titles.include?(author[0])
            auth_hash["gender"] = gender(author[0])
          else
            auth_hash["gender"] = gender(author[1])
          end
          auth_hash["authorcentral"] = authorcentral(auth_hash["name"])
          writers << auth_hash
        end
      else
        writers = nil
      end
    end

    return writers
  end

  def price
    price = @page.xpath("//*[contains(concat(' ', @class, ' '), ' priceLarge ')]/text()").to_s.strip

    unless price.blank?
      price[1,price.length].to_f
    else
      price = 0.to_f
    end
  end

  def pages
    page_count = @page.xpath("//*[contains(text(),'Print Length:')]/..").text
    page_count.slice! "Print Length: "
    page_count.slice! " pages"

    unless page_count.blank?
      page_count.to_i
    else
      nil
    end
  end

  def publication_date
    unless @page.xpath("//input[@id='pubdate']/@value").to_s.to_date.blank?
      @page.xpath("//input[@id='pubdate']/@value").to_s.to_date
    else
      nil
    end
  end

  def publisher
    publisher = @page.xpath("//b[contains(text(), 'Publisher')]/..").text.strip

    unless publisher.blank?
      publisher[11, publisher.length - 1]
    else
      nil
    end
  end

  def filesize
    filesize = @page.xpath("//b[contains(text(), 'File Size:')]/..").text

    unless filesize.blank?
      filesize[11, filesize.length - 1]
    else
      nil
    end
  end

  def description
    #@page.xpath("//div/div[@class='content']/noscript").text
    unless @page.xpath("//div[@id='postBodyPS']").text.blank?
      @page.xpath("//div[@id='postBodyPS']").text
    else
      nil
    end
  end

  def gender(name)
    gender = JSON.parse(open("http://api.genderize.io?name=#{name.to_s}").read) unless name.blank?

    unless gender.blank?
      return "#{gender['gender']}"
    else
      nil
    end
  end

  def authorcentral(author)
    unless author.blank?
      @page.xpath(%Q[//a[contains(text(),"#{author} Page")]/@href/text()]).to_s.strip
    else
      nil
    end
  end

  def authorcentral_twitter(url)
    unless url.blank?
      begin
        ac_page = Nokogiri::HTML(open(url))

        return ac_page.xpath("//p[@class='tweetScreenName']/a.text()").to_s.strip
      rescue
        return nil
      end
    end
  end

  def save_ebook(ebook_id)
    unless title.blank?
      ebook = Ebook.find(ebook_id)
        ebook.name = "#{title}"
        ebook.save

      begin
        ebook.rating = rating #unless rating.blank?
        ebook.save
      rescue(e)
        ebook.rating = nil
        processing_failure(e, book.name)
      end

      begin
        ebook.reviews = reviews #unless reviews.blank?
        ebook.save
      rescue
        ebook.reviews = nil
        processing_failure(e, book.name)
      end

      begin
        ebook.price = price #unless price.blank?
        ebook.save
      rescue
        ebook.price = nil
        processing_failure(e, book.name)
      end

      begin
        ebook.pages = pages #unless pages.blank?
        ebook.save
      rescue
        ebook.pages = nil
        processing_failure(e, book.name)
      end

      begin
        ebook.desc = "#{description}" #unless description.blank?
        ebook.save
      rescue
        ebook.desc = nil
        processing_failure(e, book.name)
      end

      begin
        ebook.publisher = "#{publisher}" #unless publisher.blank?
        ebook.save
      rescue
        ebook.publisher = nil
        processing_failure(e, book.name)
      end

      begin
        ebook.publication_date = "#{publication_date}" #unless publication_date.blank?
        ebook.save
      rescue
        ebook.publication_date = nil
        processing_failure(e, book.name)
      end

      begin
        ebook.filesize = "#{filesize}" #unless filesize.blank?
        ebook.save
      rescue
        ebook.filesize = nil
        processing_failure(e, book.name)
      end

        authors().each do |a|
          author = ebook.authors.new
          author.name = "#{a['name']}" #unless a['name'].blank?
          author.save

          author.gender = "#{a['gender']}" #unless a['gender'].blank?
          author.save

          author.authorcentral = "#{a['authorcentral']}" #unless a['authorcentral'].blank?
          author.save

          author.twitter = "#{authorcentral_twitter(a['authorcentral'])}" #unless a['authorcentral'].blank?
          author.save
        end

        ranks().each do |r|
          rank = ebook.ranks.new
          rank.category_id = r["category"] unless r["category"].blank?
          rank.save

          rank.rank = r["rank"] unless r["rank"].blank?
          rank.save
        end

      generate_rollup(ebook.bestseller_id)
    else
      ebook.publication_date = nil
      processing_failure(e, "Title Failure")
    end
  end

  def processing_failure(e, title)
    File.open("../log/processing_failures/failures.log", "a") do |f|
      f.write("Datetime: #{Time.now}\n\n")
      f.write("Title: #{title}")
      f.write("#{e.message}\n")
      f.write("====================================================================\n\n")
    end

    File.open("../log/processing_failures/#{Time.now} - #{title}.html", "a") do |html|
      html.write("#{@page.to_s}")
    end
  end

  def custom_crawl
    unless params[:url].blank?
      url = params[:url]

      asin = url.split("/")[6]  

      logger.info "ASIN: #{asin}"

      cat_group = CategoryGroup.where(complete: true).last

      # Get the current category...
      cat = cat_group.categories.where(asin: asin).first

      bestseller = Bestseller.new
      bestseller.category_id = cat.id
      bestseller.category_group_id = cat_group.id
      bestseller.save

      get_ebook_stubs(bestseller.id)

      #ebooks = bestseller.ebooks
      ebooks = Ebook.find_by_sql("SELECT *, #{bestseller.category_id} AS category_id FROM ebooks WHERE bestseller_id = #{bestseller.id};")

      render json: ebooks
    end
  end

  def generate_rollup(bestseller_id)
    unless bestseller_id.nil?
      bestseller = Bestseller.find(bestseller_id)
      category = Category.find(bestseller.category_id)
      est = rollup_estimates(bestseller_id)
      rollup = Rollup.new
      rollup.bestseller_id = bestseller_id
      rollup.category_child_count = category.children.count
      rollup.category_book_count = category.count
      rollup.estimated_mean_sales = est["estimated_mean_sales"]
      rollup.estimated_mean_income = est["estimated_mean_earnings"]
      rollup.estimated_ceiling_sales = est["estimated_ceiling_sales"]
      rollup.estimated_ceiling_income = est["estimated_ceiling_earnings"]
      rollup.estimated_floor_sales = est["estimated_floor_sales"]
      rollup.estimated_floor_income = est["estimated_floor_earnings"]
      rollup.save
    end
  end

  def rollup_estimates(bestseller_id)
    unless bestseller_id.nil?
      bestseller = Bestseller.find(bestseller_id)
      cat_root = Category.where(category_group_id: bestseller.category_group_id).first

      estimates_hash = Hash.new

      # Ranks and price arrays.
      ranks_arr = Array.new
      price_arr = Array.new
      reviews_arr = Array.new
      pages_arr = Array.new
      filesizes_arr = Array.new

      bestseller.ebooks.each do |e|
        e.ranks.where(category_id: cat_root.id).each do |r|
          ranks_arr << r.rank
        end
      end

      bestseller.ebooks.each do |p|
        price_arr << p.price if p.price
        reviews_arr << p.reviews if p.reviews
        pages_arr << p.pages if p.pages
        filesizes_arr << p.filesize[0, p.filesize.length - 3].to_i if p.filesize
      end

      # Calculate means.
      mean_rank = (ranks_arr.inject{|sum,x| sum + x }/ranks_arr.size).to_i unless ranks_arr.empty?
      mean_price = (price_arr.inject{|sum,x| sum + x }/price_arr.size).to_f unless price_arr.empty?
      mean_reviews = (reviews_arr.inject{|sum,x| sum + x }/reviews_arr.size).to_i unless reviews_arr.empty?
      mean_pages = (pages_arr.inject{|sum,x| sum + x }/pages_arr.size).to_i unless pages_arr.empty?
      mean_filesize = (filesizes_arr.inject{|sum,x| sum + x }/filesizes_arr.size).to_i unless filesizes_arr.empty?

      # Calculate floors.
      # Floor rank is actually the HIGHTEST rank.
      floor_rank = ranks_arr.sort.last.to_i
      floor_price = price_arr.sort.first.to_f
      floor_reviews = reviews_arr.sort.first.to_i
      floor_pages = pages_arr.sort.first.to_i
      floor_filesize = filesizes_arr.sort.first.to_i

      # Calculate ceilings.
      # Ceiling rank is actually the LOWEST rank.
      ceiling_rank = ranks_arr.sort.first.to_i
      ceiling_price = price_arr.sort.last.to_f
      ceiling_reviews = reviews_arr.sort.last.to_i
      ceiling_pages = pages_arr.sort.last.to_i
      ceiling_filesize = filesizes_arr.sort.last.to_i

      mean_est_earnings = earningsEstimator(mean_rank, mean_price, mean_filesize, "mean")
      floor_est_earnings = earningsEstimator(floor_rank, floor_price, floor_filesize, "floor")
      ceiling_est_earnings = earningsEstimator(ceiling_rank, ceiling_price, ceiling_filesize, "ceiling")

      estimates_hash["estimated_mean_earnings"] = mean_est_earnings
      estimates_hash["estimated_ceiling_earnings"] = ceiling_est_earnings
      estimates_hash["estimated_floor_earnings"] = floor_est_earnings

      mean_est_sales = salesEstimator(mean_rank, "mean")
      floor_est_sales = salesEstimator(floor_rank, "floor")
      ceiling_est_sales = salesEstimator(ceiling_rank, "ceiling")

      estimates_hash["estimated_mean_sales"] = mean_est_sales
      estimates_hash["estimated_ceiling_sales"] = ceiling_est_sales
      estimates_hash["estimated_floor_sales"] = floor_est_sales

      return estimates_hash
    end
  end
end
