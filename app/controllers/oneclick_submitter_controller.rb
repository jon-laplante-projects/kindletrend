class OneclickSubmitterController < ApplicationController
	before_action :authenticate_user!

	layout :set_layout

	def set_layout
		"layouts/application"

		if current_user.account.partner.present? && current_user.account.partner.partner_type_id == 2
		  "layouts/blank"
		end
	end
	
	def pulse_form
		required_friendlies = '"title", "author_first_name", "author_last_name", "author", "genre", "free_start_month", "free_start_date", "free_start_year", "free_start_hour", "free_start_minute", "free_end_hour", "free_end_minute", "free_end_month", "free_end_date", "free_end_year","free_from", "free_to", "number_of_reviews", "days_free", "any_2_digits", "three_plus_one_is", "asin"'

		category_root_id = CategoryGroup.where("complete IS true").last

		@categories = Category.where("ancestry LIKE '#{category_root_id.group_root_id}'")

		@friendlies = FriendlyName.where("field_type <> 'hidden' AND field_type <> 'submit' AND field_name NOT IN (#{required_friendlies})")
		@forms = Array.new

		cat_ids = CategoryForm.select("category_id").distinct

		cat_ids.each do |cat_id|
			form = Hash.new

			category = Category.find(cat_id.category_id)
			form["cat_id"] = cat_id.category_id
			form["category"] = category.name

			form_arr = Array.new
			cat_forms = CategoryForm.where(category_id: cat_id.category_id)#.select("form_id").distinct
			cat_forms.each do |cf|
				frms = Hash.new

				frm = Form.find(cf.form_id)
				frms["id"] = frm.id
				frms["form_name"] = frm.name

				form_arr << frms
			end
			form["forms"] = form_arr

			@forms << form
		end

		puts @forms
	end

	def pulse_submitter
		results = Array.new

		# Map the categories to forms...
		catForms = CategoryForm.where("category_id IN (#{params[:categories].join(",")})").select("form_id").distinct()

		# Get the form...
		form_ids = Array.new
		catForms.each do |f|
			form_ids << f.form_id
		end

		forms = Form.where("id IN (#{form_ids.join(",")})")

		# Build an array of hashes of the field_params
		field_params = params[:fields].map

		field_map = Array.new
		field_params.each do |fp|
			result = Hash.new
			fp.each do |p|
				result["key"] = p[0]
				result["val"] = p[1]	
			end

			field_map << result
		end

		form = Hash.new
		# MECHANIZE into the form and push the variables into the appropriate fields...
		forms.each do |f|
			res = Hash.new

			res["form_name"] = "#{f.name}"
			res["form_id"] = f.id
			res["form_url"] = f.form_url

			# Get the fields for that form...
			fields = f.fields.where("ignore_field IS false AND field_type <> 'submit'")

			# Randomize the browser displayed to the server.
			rand_browser = 1 + rand(7)
			case(rand_browser)
				when 1 then agent_alias = 'Mac Firefox'
				when 2 then agent_alias = 'Mac Mozilla'
				when 3 then agent_alias = 'Windows Chrome'
				when 4 then agent_alias = 'Windows IE 8'
				when 5 then agent_alias = 'Windows IE 9'
				when 6 then agent_alias = 'Windows Mozilla'
				when 7 then agent_alias = 'Linux Firefox'
			end

			# Create our Mechanize object, using the random browser.
			user_agent = Mechanize.new {|agent| agent.user_agent_alias = "#{agent_alias}"}

			# Get the page.
			ebook_forum = user_agent.get(f.form_url)
			ebook_form = ebook_forum.form

			fields.each do |fld|
				friendly = FriendlyName.find(fld.friendly_name_id)

				a = field_map.select {|result| result["key"] == "#{friendly.field_name}"}
				
				val_str = a[0]["val"].to_s
				#unless fld.field_type == "select"
					eval("ebook_form['#{fld.name}'] = #{%Q[val_str]}")
				#else
				#	#ebook_form['#{fld.name}'].
				#	ebook_form['#{fld.name}'].options.find{ |o| o.text == %Q[val_str] }.select
				#	#eval("ebook_form['#{fld.name}'].options.find{|o| o.text == #{%Q[val_str]} }.select")
				#end
			end

			submission = ebook_form.submit

			base_url = f.submission_url
			submission = Nokogiri::HTML(submission.content)
			
			# Remove anything pulling assets from or linking to somewhere away from Kindle Trend.
			submission.xpath("//a").remove
			submission.xpath("//link").remove
			submission.xpath("//script").remove
			submission.xpath("//img").remove
			submission.xpath("//style").remove
			submission.xpath("//@*[starts-with(name(),'on')]").remove

			# If there is an error and a form is shown, on submit pop a new tab instead of redirecting the iframe.
			submission.xpath("//form").each do |f|
				f["target"] = "_BLANK"
			end

			res["response"] = CGI::unescapeHTML(submission.xpath("//body").to_s)
			results << res
		end

		render json: results
	end
end