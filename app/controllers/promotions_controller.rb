class PromotionsController < ApplicationController
	before_action :authenticate_user!

	layout :set_layout

	def set_layout
		"layouts/application"

		if current_user.account.partner.present? && current_user.account.partner.partner_type_id == 2
		  "layouts/blank"
		end
	end
	
	def resources

	end
end