class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_one :account
  
  attr_reader :active

  after_create :create_account

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  ROLES = %i[sys_admin jv_admin partner_admin subscriber banned]

  def active_for_authentication?
  	my_acct = Account.find_by(user_id: self.id)

  	if my_acct.status == "approved"
  		super and true
  	else
  		super and false
  	end
  end

  def create_account
    token = SecureRandom.hex(16)
    token = SecureRandom.hex(16) until Account.find_by(token: "#{token}").nil?

  	my_acct = Account.new
  	my_acct.user_id = self.id
  	my_acct.status = "unverified"
    my_acct.token = token
  	my_acct.save
  end
end
