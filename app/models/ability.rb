class Ability
  include CanCan::Ability

  def initialize(user)
    if user.role == "sys_admin"
        can :manage, :all
        can :white_label_admin, :control_panel
    end
    # Read-only sysAdmin showing account details.
    if user.role == "jv_admin"
        can :index, Category
        can :easy_targets, Category
        can :analytics, Category
        can :takeoffs, Category
        can :rollups, Category
        can :users_report, :control_panel
        can :user_update, :control_panel
        cannot :white_label_admin, :control_panel
    end
    
    # Read-only, scoped to partner company.
    # Can add accounts, but are charged for each.
    # Comes with 2 free admin accounts.
    if user.role == "partner_admin"
        can :index, Category
        can :easy_targets, Category
        can :analytics, Category
        can :takeoffs, Category
        can :rollups, Category
        can :users_report, :control_panel
        can :white_label_admin, :control_panel
        can :white_label_update, :control_panel
        can :user_update, :control_panel
    end

    if user.role == "subscriber"
        can :index, Category
        can :easy_targets, Category
        can :analytics, Category
        can :takeoffs, Category
        can :rollups, Category
        cannot :joint_venture, :control_panel
        cannot :partners, :control_panel
        cannot :system_admin, :control_panel
        cannot :users_report, :control_panel
        cannot :white_label_admin, :control_panel
        cannot :user_update, :control_panel
    end
    
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
