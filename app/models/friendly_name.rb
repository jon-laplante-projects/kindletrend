class FriendlyName < ActiveRecord::Base
	has_many :friendly_vals
	has_many :friendly_radios
end