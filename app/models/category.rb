class Category < ActiveRecord::Base
	has_ancestry
	belongs_to :category_group
	has_many :bestsellers

	def self.init
		cat_group = CategoryGroup.new
	    cat_group.save

		automation_data = CategoryAutomation.last

	    category = cat_group.categories.new
	      category.name = "Kindle eBooks"
	      #category.url = "http://www.amazon.com/Best-Sellers-Kindle-Store-eBooks/zgbs/digital-text/154606011/ref=zg_bs_unv_kstore_2_154692011_2"
	      category.url = "http://www.amazon.com/s/ref=lp_154607011_ex_n_1?rh=n%3A133140011%2Cn%3A!133141011%2Cn%3A154606011&bbn=154606011&ie=UTF8&qid=1392049593"
	      category.asin = "154606011"
	      category.count = 2482522
	    category.save

	    cat_group.group_root_id = category.id
	    cat_group.save

	    Category.parse_single_category(category.id)

	    automation_data.parent_watch = 100
	    automation_data.curr_watch = 1
	    automation_data.save

	    #    watch_id = 100
		#    watch_count = 1
		#
		#    watches = Category.where(category_group_id: cat_group.id)
		#    watches.each do |w|
		#      if watch_count == 3
		#      #if w.id != watches[0].id
		#        watch_id += 1
		#        watch_count = 1
		#      end
		#
		#      w.watch = watch_id
		#      w.save
		#
		#      watch_count += 1
		#    end

	    # Populate automation data for the new category_group.
	    last_top_id = Category.where(category_group_id: cat_group.id).last
	    automation_data.prev_top_id = category.id
	    automation_data.last_top_id = last_top_id.id
	    automation_data.prev_id = category.id
	    automation_data.prev_top_complete = true
	    automation_data.save
	end

	protected
	def self.parse_single_category(id)
	    automation_data = CategoryAutomation.last

	    node = Category.find(id)
	    puts "Current node: #{node.id}"

	    # Get the root page.
	    rand_browser = 1 + rand(7)
	    case(rand_browser)
	      when 1 then agent_alias = 'Mac Firefox'
	      when 2 then agent_alias = 'Mac Mozilla'
	      when 3 then agent_alias = 'Windows Chrome'
	      when 4 then agent_alias = 'Windows IE 8'
	      when 5 then agent_alias = 'Windows IE 9'
	      when 6 then agent_alias = 'Windows Mozilla'
	      when 7 then agent_alias = 'Linux Firefox'
	    end

	    user_agent = Mechanize.new {|agent| agent.user_agent_alias = "#{agent_alias}"}

	    user_agent.get(node.url) { |d| @page = d.content }

	    @page = Nokogiri::HTML(@page)

	    ## Get the categories navigation section of the page.
	    #if node.name == "Kindle eBooks"
	    # #nav = @page.xpath('//ul[@id="ref_154606011"]')
	      nav = @page.xpath('//ul[@data-typeid="n"]')
	    #else
	    # @xpath_var = "//ul[@id='ref_#{node.asin}']"
	    # @xpath_var = "//ul[@id='ref_#{node.asin}']/li/strong"
	    # 
	    # puts "Nav. xpath: #{@xpath_var}"
	    # nav = @page.xpath(@xpath_var).children
	    #end

	    #nav = nav.children
	    puts "Nav. count: #{@page.xpath(@xpath_var).children.count}"

	    # Get the category names and links for this page.
	    if nav.children.count > 0
	      puts "Node has children..."
	      nav.children.each do |n|
	        if n.xpath('a/span[@class="refinementLink"]').to_s.strip != ""
	          name = n.xpath('a/span[@class="refinementLink"]').text
	          puts "Name: #{name}"
	          # 3 lines to distill this is really ugly!
	          puts "Nav link = " << n.xpath('a/span[@class="refinementLink"]/../@href').to_s[0,21]
	          unless n.xpath('a/span[@class="refinementLink"]/../@href').to_s[0,21] == "http://www.amazon.com"
	            url = "http://www.amazon.com" + n.xpath('a/span[@class="refinementLink"]/../@href').to_s
	          else
	            # Old URL structure: http://www.amazon.com/s?rh=n%3A157305011
	            # New structure as of 15-May-2014: http://www.amazon.com/s/ref=lp_154606011_nr_n_23/186-8214925-0066221?rh=n%3A133140011%2Cn%3A%21133141011%2Cn%3A154606011%2Cn%3A668010011&bbn=154606011&ie=UTF8&qid=1400164622&rnid=154606011
	            url = n.xpath('a/span[@class="refinementLink"]/../@href').to_s
	          end
	          
	          puts "URL: #{url}"
	          asin = url.split("&bbn")[0].split("Cn%3A").pop
	          count = n.xpath('a/span[@class="narrowValue"]').text.to_s
	          count = count[2, count.length - 3].to_s.delete(",").to_i
	          puts "Count: #{count}"

	          if name.strip != ""
	            item = Category.new(:parent_id => node.id)  
	              item.name = "#{name}"
	              item.url = "#{url}"
	              item.asin = "#{asin}"
	              item.count = count
	              if count > 40000
	                if automation_data.watch_iteration == 5
	                  automation_data.curr_watch += 1
	                  automation_data.watch_iteration = 0
	                end

	                item.watch = automation_data.curr_watch

	                automation_data.watch_iteration += 1
	                automation_data.save
	              end
	              item.category_group_id = node.category_group_id
	            item.save

	            # Set watches for the parent categories.
	            unless item.ancestry.include? "/"
	              #######################################
	              # if root, ignore.
	              # reset to 100 for first node.
	              #######################################
	              if item.name == "Kindle eBooks"
	                automation_data.parent_watch = 99
	                automation_data.save
	              else
	                item.watch = automation_data.parent_watch.to_i + 1
	                automation_data.parent_watch += 1
	                automation_data.save
	              end
	            end

	            automation_data.prev_id = item.id
	            automation_data.save
	          end
	        end
	      end
	    end
	end
end