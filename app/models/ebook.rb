class Ebook < ActiveRecord::Base
	belongs_to :bestseller
	has_many :authors
	has_many :ranks
	has_many :reviewers
end