class Bestseller < ActiveRecord::Base
	belongs_to :category
	has_many :ebooks
	has_one :rollup
end
