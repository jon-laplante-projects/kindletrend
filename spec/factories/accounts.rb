# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :account do
    user_id 1
    customer_name "MyString"
    status "MyString"
    payment_processor "MyString"
  end
end
