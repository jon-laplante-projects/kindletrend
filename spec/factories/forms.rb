# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :form do
    name "MyString"
    form_url "MyText"
    submission_url "MyText"
    submission_type "MyString"
    captcha false
    free_ebooks false
  end
end
