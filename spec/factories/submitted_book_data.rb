# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :submitted_book_datum, :class => 'SubmittedBookData' do
    submitted_book_id 1
    friendly_field_id 1
    field_value "MyText"
  end
end
