# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :field, :class => 'Fields' do
    name "MyString"
    form_id 1
    friendly_name_id 1
    field_type "MyString"
    require_field false
    ignore_field false
  end
end
