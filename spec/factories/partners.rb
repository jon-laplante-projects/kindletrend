# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :partner do
    name "MyString"
    name_as_processor "MyString"
    token "MyString"
  end
end
