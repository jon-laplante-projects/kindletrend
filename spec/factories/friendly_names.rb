# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :friendly_name do
    name "MyString"
    field_type "MyString"
  end
end
