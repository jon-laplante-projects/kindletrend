# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :submission_log do
    submitted_book_id 1
    form_id 1
  end
end
