# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :company_contact do
    first_name "MyString"
    last_name "MyString"
    email "MyString"
    phone "MyString"
    poc false
  end
end
