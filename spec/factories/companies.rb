# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :company do
    name "MyString"
    sys_name "MyString"
    company_type_id 1
    company_contact_id 1
  end
end
