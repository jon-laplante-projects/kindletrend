# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :field_value do
    field_id 1
    preset_field_value "MyString"
  end
end
