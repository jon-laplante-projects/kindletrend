#require "sidekiq/web"
#require 'sidetiq/web'

Kindletrend::Application.routes.draw do
  #namespace :app do
    #devise_for :users
    #############################################################################
    # DON'T allow visitors to register, but DO allow users to edit their profile.
    #############################################################################
    devise_for :users, :skip => [:registrations]
    as :user do
      get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
      put 'users' => 'devise/registrations#update', :as => 'user_registration'
    end
  #end

  resources :categories

  resources :ebooks

  get 'rollup', to: 'categories#rollup'
  get 'categories/:category/analytics/', to: 'categories#analytics'
  get 'targets', to: 'categories#targets'
  get 'estimations', to: 'welcome#calcs'
  get 'mission', to: 'welcome#mission'
  get 'categories/:category_id/takeoff', to: "categories#takeoff"
  get 'categories/:keywords/amazon_suggest', to: "categories#amazon_suggest"
  
  # Routes for account CRUD.
  get 'accounts/subscription', to: "accounts#subscription", as: "subscribe"
  post 'accounts/process_subscription', to: "accounts#process_subscription"
  match 'jvzoo_instant_notification', to: "accounts#jvzoo_instant_notification", via: [:options]
  post 'jvzoo_instant_notification', to: "accounts#jvzoo_instant_notification"
  #post 'clickbank_instant_notification' => "accounts#clickbank_instant_notification"
  #post 'paypal_instant_notification' => "accounts#paypal_instant_notification"
  post 'click2sell_instant_notification', to: "accounts#click2sell_instant_notification"
  post 'spawn_user', to: "accounts#spawn_user"
  delete 'accounts/cancel_subscription', to: "accounts#cancel_subscription", as: "cancel_subscription"
  match 'accounts/partner_integration', to: "accounts#partner_integration", via: [:options]
  post 'accounts/partner_integration', to: "accounts#partner_integration"
  match 'accounts/partner_gen_integration' => "accounts#partner_gen_password", via: [:options]
  post 'accounts/partner_gen_integration' => "accounts#partner_gen_password"  

  match 'accounts/zaxaa_integration', to: "accounts#zaxaa_zpn", via: [:options]
  post 'accounts/zaxaa_integration', to: "accounts#zaxaa_zpn"

  match 'accounts/zaxaa', to: "accounts#zaxaa", via: [:options]
  post 'accounts/zaxaa', to: "accounts#zaxaa"
  get 'accounts/zaxaa', to: "accounts#zaxaa"
  match 'accounts/zaxaa', to: 'accounts#zaxaa', :constraints => { :protocol => 'http' }, :via => :get
  post 'verify/coupon', to: "accounts#verify_coupon"

  get 'partner/login', to: "accounts#whitelabel_login"
  post 'partner/user/login', to: "accounts#whitelabel_create_user_session"
  delete 'partner/logout', to: "accounts#whitelabel_logout"
  get 'partner/edit', to: "accounts#whitelabel_edit_account"
  post 'partner/user/update', to: "accounts#whitelabel_update_account"
  # End account routes.

  # Routes for data parsing.
  get 'retrieval_assignment', to: "parser#assign_category"
  get 'category_assignment', to: "parser#assign_cat_id"
  match 'data_consumer', to: "parser#parse_page", via: [:options]
  post 'data_consumer', to: "parser#parse_page"
  match 'custom_crawl', to: "parser#custom_crawl", via: [:options]
  post 'custom_crawl', to: "parser#custom_crawl"
  # End parsing routes.

  # Routes for the "OneClick" feature...
  get 'oneclick_admin', to: "oneclick_admin#mapper"
  post 'parse_form', to: "oneclick_admin#parse_form"
  post 'save_form_data', to: "oneclick_admin#save_form_data"

  ######################################################
  ## Turn this back on when Forum Blaster is fixed!!!
  ######################################################
  #get 'pulse_form', to: "oneclick_submitter#pulse_form"
  #post 'pulse_submitter', to: "oneclick_submitter#pulse_submitter"
  # End "OneClick" routes...

  # Promotions routes...
  get "promotions/resources", to: "promotions#resources"
  # End promotions routes...

  # Remote widgets...
  get "widgets/partner_sign_up", to: "widgets#partner_sign_up"
  get "widgets/partner_iframe", to: "widgets#partner_iframe"
  get "widgets/partner_resize_injection", to: "widgets#partner_resize_injection"
  # End remote widgets...

  # Control panel routes...
  get 'control_panel/system_admin', to: "control_panel#system_admin"
  #get 'control_panel/joint_venture', to: "control_panel#joint_venture"
  get 'control_panel/users_report', to: "control_panel#users_report"
  get 'control_panel/watches_admin', to: "control_panel#watches_admin", as: "watches_admin"
  post 'control_panel/watches_update', to: "control_panel#watches_update"
  get 'whitelabel/admin', to: "control_panel#white_label_admin"
  post 'whitelabel/update', to: "control_panel#white_label_update"
  post 'control_panel/user_update', to: "control_panel#user_update"
  #get 'control_panel/user_reports', to: "control_panel#user_reports"
  # End control panel routes...
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root to: 'welcome#index'

#  sidekiq_constraint = lambda do |request|
#    request.env["warden"].user(:user).role == 'sys_admin'
#  end
#
#  constraints sidekiq_constraint do
#    mount Sidekiq::Web, at: "automation_queue"
#  end
end
