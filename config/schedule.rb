# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
job_type :rvm_rake, "cd #{path} && RAILS_ENV=:environment bundle exec rake :task --silent :output"

set :output, "#{path}/log/cron_log.log"

#every 3.minutes do
#	#rake "automation:test_me"
#	rvm_rake "automation:test_me"
#end

every 1.day, :at => "12:03 am" do
	rvm_rake "automation:bestsellers"
end

every 1.day, :at => "12:30 am" do
	rvm_rake "automation:generate_rollups"
end

every 1.day, :at => "12:10 am" do
	rvm_rake "automation:parent_bestsellers"
end

every 1.day, :at => "9:00 am" do
	rvm_rake "automation:parent_categories"
end

every 1.day, :at => "3:00 am" do
	rvm_rake "automation:subcategories"
end

every 1.day, :at => "3:00 pm" do
	rvm_rake "automation:subcategories"
end

every 1.day, :at => "6:00 pm" do
	rvm_rake "automation:parent_bestsellers_cleanup"
end

#every 1.day, :at => "6:00 am" do
#	rvm_rake "automation:categories"
#end

every 1.day, :at => "2:00 am" do
	rvm_rake "automation:reset_aged_assigned_ebooks"
end

every 1.day, :at => "2:00 pm" do
	rvm_rake "automation:reset_aged_assigned_ebooks"
end

every 1.day, :at => "4:00 pm" do
	command "rvmsudo bundle exec ruby /webapps/app.git/lib/mysql-s3-backup.rb"
end
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
