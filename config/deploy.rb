# config valid only for Capistrano 3.1
lock '3.1.0'

set :application, 'KindleTrend'
#set :repo_url, 'deploy@kindletrend.com:/webapps/app.git'
set :repo_url, 'https://memphisJon@bitbucket.org/memphisJon/kindletrend.git'
#set :repo_url, 'https://memphisJon:j3tstr3@m@bitbucket.org/memphisJon/kindletrend.git'
#set :git_https_username, 'memphisJon'
set :git_https_password, 'j3tstr3@m'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }
set :branch, "master"

# Default deploy_to directory is /var/www/my_app
#set :deploy_to, '/webapps/app.git'

# Default value for :scm is :git
set :scm, :git

set :user, "deploy"
# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

set :rails_env, "production"

#set :deploy_via, :remote_cache
set :deploy_via, :copy

server "kindletrend.com", roles: [:app, :web, :db]#, :primary => true
set :deploy_to, "/webapps/app.git"

set :ssh_options, {
   verbose: :debug
}

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
